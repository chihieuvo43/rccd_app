﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace RCCD_WebApp.Components
{
    [ViewComponent(Name = "FlagKhoaHoc")]
    public class FlagKhoaHocViewComponent:ViewComponent
    {
        RCCD_API rccd_api = new RCCD_API();
        public FlagKhoaHocViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<KhoaHoc> khoaHoc = new List<KhoaHoc>();
            HttpClient client = rccd_api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/GetKhoaHocByFlag");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<List<KhoaHoc>>(result);

                //Lấy thông tin chi tiết
                List<ChiTietGiangDay> listChiTietGiangDay = new List<ChiTietGiangDay>();
                HttpResponseMessage ChiTietGiangDay = await client.GetAsync("api/ChiTietGiangDay");
                var resultChiTietGiangDay = ChiTietGiangDay.Content.ReadAsStringAsync().Result;
                listChiTietGiangDay = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(resultChiTietGiangDay);

                //List giảng viên
                List<GiangVien> listGiangVien = new List<GiangVien>();
                HttpResponseMessage giangVien = await client.GetAsync("api/GiangVien");
                var resultGiangVien = giangVien.Content.ReadAsStringAsync().Result;
                listGiangVien = JsonConvert.DeserializeObject<List<GiangVien>>(resultGiangVien);

                ViewBag.ListChiTietGiangDay = listChiTietGiangDay;
                ViewBag.ListGiangVien = listGiangVien;

                return View(khoaHoc);
            }
            return View();
        }
    }
}
