﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace RCCD_WebApp.Components
{
    [ViewComponent(Name = "ListLoaiChuongTrinhHocInHome")]
    public class ListLoaiChuongTrinhHocInHomeViewComponent:ViewComponent
    {
        RCCD_API rccd_api = new RCCD_API();
        public ListLoaiChuongTrinhHocInHomeViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<LoaiChuongTrinhHoc> loaiChuongTrinhHoc = new List<LoaiChuongTrinhHoc>();
            HttpClient client = rccd_api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/LoaiChuongTrinhHoc");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                loaiChuongTrinhHoc = JsonConvert.DeserializeObject<List<LoaiChuongTrinhHoc>>(result);
                return View(loaiChuongTrinhHoc);
            }
            return View();
        }
    }
}
