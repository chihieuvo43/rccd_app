#pragma checksum "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "982ce5364e5eeb5682d38ff7e79ce9e4e107af85"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_NgayNghi__ModalNgayNghiAddChildrenPartialView), @"mvc.1.0.view", @"/Areas/Admin/Views/NgayNghi/_ModalNgayNghiAddChildrenPartialView.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Admin/Views/NgayNghi/_ModalNgayNghiAddChildrenPartialView.cshtml", typeof(AspNetCore.Areas_Admin_Views_NgayNghi__ModalNgayNghiAddChildrenPartialView))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\_ViewImports.cshtml"
using RCCD_WebApp;

#line default
#line hidden
#line 2 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\_ViewImports.cshtml"
using RCCD_WebApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"982ce5364e5eeb5682d38ff7e79ce9e4e107af85", @"/Areas/Admin/Views/NgayNghi/_ModalNgayNghiAddChildrenPartialView.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0de4ce4158f45426283ca5e3d408354158fb0a3b", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_NgayNghi__ModalNgayNghiAddChildrenPartialView : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Update", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Phong", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("frmEditPhong"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-horizontal form-label-left"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 629, true);
            WriteLiteral(@"<div class=""modal fade"" id=""ModalNgayNghiAddChildrenPartialView"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLabel"" aria-hidden=""true"">
    <div class=""modal-dialog"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">Thêm ngày nghỉ năm </h5><h4></h4>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                ");
            EndContext();
            BeginContext(629, 4639, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af856089", async() => {
                BeginContext(772, 442, true);
                WriteLiteral(@"
                    <div class=""form-group"">
                        <label class=""control-label col-md-4 col-sm-4 col-xs-12"" for=""first-name"">
                            Bắt đầu ngày/tháng <span class=""required"">*</span>
                        </label>
                        <div class=""col-md-4 col-sm-4 col-xs-6"">
                            <select id=""txtNgayBatDauNghi"" class=""form-control"">
                                ");
                EndContext();
                BeginContext(1214, 38, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af856926", async() => {
                    BeginContext(1222, 21, true);
                    WriteLiteral("--Chọn ngày bắt đầu--");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1252, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 19 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                  
                                    for (int i = 1; i <= 31; i++)
                                    {

#line default
#line hidden
                BeginContext(1396, 40, true);
                WriteLiteral("                                        ");
                EndContext();
                BeginContext(1436, 30, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af858656", async() => {
                    BeginContext(1456, 1, false);
#line 22 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                                      Write(i);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#line 22 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                           WriteLiteral(i);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1466, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 23 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                    }
                                

#line default
#line hidden
                BeginContext(1542, 39, true);
                WriteLiteral("                            </select>\r\n");
                EndContext();
                BeginContext(1699, 489, true);
                WriteLiteral(@"                            <input type=""hidden"" data-val=""true"" id=""txtNamId"" class=""form-control col-md-7 col-xs-12"">
                            <input type=""hidden"" data-val=""true"" id=""txtNamInModelAdd"" class=""form-control col-md-7 col-xs-12"">
                        </div>
                        
                        <div class=""col-md-4 col-sm-4 col-xs-6"">
                            <select id=""txtThangBatDauNghi"" class=""form-control"">
                                ");
                EndContext();
                BeginContext(2188, 39, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af8511796", async() => {
                    BeginContext(2196, 22, true);
                    WriteLiteral("--Chọn tháng bắt đầu--");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2227, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 34 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                  
                                    for (int i = 1; i <= 12; i++)
                                    {

#line default
#line hidden
                BeginContext(2371, 40, true);
                WriteLiteral("                                        ");
                EndContext();
                BeginContext(2411, 30, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af8513528", async() => {
                    BeginContext(2431, 1, false);
#line 37 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                                      Write(i);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#line 37 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                           WriteLiteral(i);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2441, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 38 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                    }
                                

#line default
#line hidden
                BeginContext(2517, 39, true);
                WriteLiteral("                            </select>\r\n");
                EndContext();
                BeginContext(2675, 505, true);
                WriteLiteral(@"                        </div>
                        </div>
                    <div class=""form-group"">
                        <label class=""control-label col-md-4 col-sm-4 col-xs-12"" for=""first-name"">
                            đến hết ngày/tháng <span class=""required"">*</span>
                        </label>
                        <div class=""col-md-4 col-sm-4 col-xs-6"">
                            <select id=""txtNgayKetThucNghi"" class=""form-control"">
                                ");
                EndContext();
                BeginContext(3180, 39, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af8516677", async() => {
                    BeginContext(3188, 22, true);
                    WriteLiteral("--Chọn ngày kết thúc--");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3219, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 51 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                  
                                    for (int i = 1; i <= 31; i++)
                                    {

#line default
#line hidden
                BeginContext(3363, 40, true);
                WriteLiteral("                                        ");
                EndContext();
                BeginContext(3403, 30, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af8518409", async() => {
                    BeginContext(3423, 1, false);
#line 54 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                                      Write(i);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#line 54 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                           WriteLiteral(i);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3433, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 55 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                    }
                                

#line default
#line hidden
                BeginContext(3509, 39, true);
                WriteLiteral("                            </select>\r\n");
                EndContext();
                BeginContext(3784, 214, true);
                WriteLiteral("                        </div>\r\n                        <div class=\"col-md-4 col-sm-4 col-xs-6\">\r\n                            <select id=\"txtThangKetThucNghi\" class=\"form-control\">\r\n                                ");
                EndContext();
                BeginContext(3998, 40, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af8521264", async() => {
                    BeginContext(4006, 23, true);
                    WriteLiteral("--Chọn tháng kết thúc--");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4038, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 64 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                  
                                    for (int i = 1; i <= 12; i++)
                                    {

#line default
#line hidden
                BeginContext(4182, 40, true);
                WriteLiteral("                                        ");
                EndContext();
                BeginContext(4222, 30, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "982ce5364e5eeb5682d38ff7e79ce9e4e107af8522997", async() => {
                    BeginContext(4242, 1, false);
#line 67 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                                      Write(i);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#line 67 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                           WriteLiteral(i);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4252, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 68 "D:\KhoaLuanTotNghiep\Demo\RCCD_WebApp\RCCD_WebApp\Areas\Admin\Views\NgayNghi\_ModalNgayNghiAddChildrenPartialView.cshtml"
                                    }
                                

#line default
#line hidden
                BeginContext(4328, 933, true);
                WriteLiteral(@"                            </select>
                        </div>
                        </div>
                        <div class=""form-group"">
                            <label class=""control-label col-md-3 col-sm-3 col-xs-12"" for=""first-name"">
                                Nội dung<span class=""required"">*</span>
                            </label>
                            <div class=""col-md-9 col-sm-9 col-xs-12"">
                                <input type=""text"" id=""txtNoiDungNghi"" class=""form-control col-md-7 col-xs-12"">
                            </div>
                        </div>
                        <div class=""modal-footer"">
                            <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Đóng</button>
                            <button type=""button"" id=""btnSaveNgayNghiChildren"" class=""btn btn-primary"">Lưu</button>
                        </div>
");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("data-parsley-validate", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5268, 58, true);
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
