﻿var chiTietLichTrongController = function () {
    this.initialize = function () {
        ListGiangVien();
        ListCa();
        LoadTableByGiangVienIdEvent();
        AddThoiGianHocEvents();
        DelEvents();
        ListChiTietLichTrong();
    }
    //Xóa lịch trống
    var DelEvents = function () {
        $('body').on('click', '#btnDelLichTrong', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            Delete(id);
        });
    }
    function Delete(id) {
        common.confirm('Bạn muốn xóa? ', function () {
            $.ajax({
                type: "POST",
                url: "/Admin/ChiTietLichTrong/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    var giangVienId = $("#txtGiangVienId").val();
                    LoadTableByGiangVienId(giangVienId);
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
   // Thêm chi tiết lịch trống
    var listChiTietLichTrong;
    function  ListChiTietLichTrong () {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietLichTrong/GetAll",
            data: {
                id: giangVienId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listChiTietLichTrong = data;
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }

    var AddThoiGianHocEvents = function () {
        $('body').on('click', '#btnAddLichTrong', function (e) {
            e.preventDefault();
            var test = true;
            var caId = $("#CaId").val();
            var giangVienId = $("#txtGiangVienId").val();
            if (caId == 0) {
                common.notify('Chọn ca', 'error');
            }
            else {
                AddChiTietChuyenMonGiangDay(giangVienId, caId);
            }
            
        });
    }
    function AddChiTietChuyenMonGiangDay(giangVienId, caId) {
        $.ajax({
            type: "POST",
            data: {
                GiangVienId: giangVienId,
                CaId: caId
            },
            url: "/Admin/ChiTietLichTrong/AddChiTietLichTrong",
            success: function (response) {
                var data = response;
                if (data.Response == "false") {
                    common.notify(data.Message, data.Type);
                }
                else {
                    common.notify(data.Message, data.Type);
                    LoadTableByGiangVienId(giangVienId);
                }
                
            },
            error: function () {
                common.notify('Lỗi khi thêm lịch trống của giảng viên', 'error');
            }
        });
    }

    //------------------------
    //Load danh sách giảng viên
    var listGiangVien;
    var ListGiangVien=function() {
        $.ajax({
            type: "GET",
            url: "/Admin/GiangVien/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listGiangVien = data;
                LoadTableByGiangVienId(giangVienId);
            }
        });
    }

    //Load danh sách ca
    var listCa;
    var ListCa = function () {
        $.ajax({
            type: "GET",
            url: "/Admin/Ca/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listCa = data;
            }
        });
    }

    var giangVienId;
    var LoadTableByGiangVienIdEvent = function () {
        $('body').on('click', '#profile-tab3', function (e) {
            e.preventDefault();
            ListGiangVien();
            var giangVienId = $("#txtGiangVienId").val();
            LoadTableByGiangVienId(giangVienId);
        });
    }

    //Load theo giảng viên Id
    function LoadTableByGiangVienId(giangVienId) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietLichTrong/GetByGiangVienId",
            data: {
                id: giangVienId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-ChiTietLichTrong').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenGiangVien;
                    $.each(listGiangVien, function (j, itemj) {

                        if (item.giangVienId == itemj.id) {
                            tenGiangVien = itemj.hoVaTen;
                        }

                    });
                    var tenCa;
                    $.each(listCa, function (j, itemj) {

                        if (item.caId == itemj.id) {
                            tenCa = itemj.tenCa;
                        }

                    });

                    //var ngayTrongTuan = (item.ngayTrongTuan).toString();
                    //var n = ngayTrongTuan.length;
                    //var thu = "";
                    //for (var itemj = 0; itemj <= n; itemj++) {
                    //    if (ngayTrongTuan[itemj] === "7") {
                    //        thu = thu + '<span class="badge badge-light">Chủ nhật</span>';
                    //    }
                    //    else if (ngayTrongTuan[itemj] === "1") {
                    //        thu = thu + '<span class="label label-default">Thứ 2</span>';
                    //    }
                    //    else if (ngayTrongTuan[itemj] === "2") {
                    //        thu = thu + '<span class="label label-primary">Thứ 3</span>';
                    //    }
                    //    else if (ngayTrongTuan[itemj] === "3") {
                    //        thu = thu + '<span class="label label-danger">Thứ 4</span>';
                    //    }
                    //    else if (ngayTrongTuan[itemj] === "4") {
                    //        thu = thu + '<span class="label label-warning">Thứ 5</span>';
                    //    }
                    //    else if (ngayTrongTuan[itemj] === "5") {
                    //        thu = thu + '<span class="label label-info">Thứ 6</span>';
                    //    }
                    //    else if (ngayTrongTuan[itemj] === "6") {
                    //        thu = thu + '<span class="label label-success">Thứ 7</span>';
                    //    }
                    //};

                    render += Mustache.render(template, {
                        STT: Stt++,
                        HoVaTen: tenGiangVien,
                        Ca: tenCa,
                        Id: item.id
                    });

                });
                if (render != '') {
                    $('#tbl-content-ChiTietLichTrong').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}