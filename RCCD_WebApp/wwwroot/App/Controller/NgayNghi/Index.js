﻿var ngayNghiController = function () {
    this.initialize = function () {
        LoadTableNgayNghi();
        btnAddNgayNghiChildrenEvent();
        btnCallFormAddNamEvent();
        btnSaveNgayNghiEvent();
        DelEvents();
        EditEvent();
    }
    //Sửa ngày nghỉ
    var EditEvent = function () {
        $('body').on('click', '#btnEditNgayNghi', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            Edit(id);
        });
    }
    var Edit = function (id) {
        $.ajax({
            type: 'GET',
            data:
            {
                Id: id
            },
            url: '/Admin/NgayNghi/GetById',
            success: function (res) {
                var data = res;
                var batDau = new Date(data.tuNgay);
                var ketThuc = new Date(data.denNgay);
                if (data.denNgay != null) {
                    var batDauDay = batDau.getDate();
                    $('#txtNgayNghi').val(batDauDay);

                    var batDauMonth = batDau.getMonth() + 1;
                    $('#txtThangNghi').val(batDauMonth);

                    var batDauYear = batDau.getFullYear();
                    $('#txtNamNghi').val(batDauYear);

                    var ketThucDay = ketThuc.getDate();
                    $('#txtNgayKetThuc').val(ketThucDay);

                    var ketThucMonth = ketThuc.getMonth() + 1;
                    $('#txtThangKetThuc').val(ketThucMonth);

                    var ketThucYear = ketThuc.getFullYear();
                    $('#txtNamKetThuc').val(ketThucYear);
                }
                else {
                    var batDauDay = batDau.getDate();
                    $('#txtNgayNghi').val(batDauDay);

                    var batDauMonth = batDau.getMonth() + 1;
                    $('#txtThangNghi').val(batDauMonth);

                    var batDauYear = batDau.getFullYear();
                    $('#txtNamNghi').val(batDauYear);

                   
                    $('#txtNgayKetThuc').val('');

                    $('#txtThangKetThuc').val('');

                    $('#txtNamKetThuc').val('');
                }

                $('#txtNoiDung').val(data.noiDung);

                $('#txtNgayNghiId').val(data.id);

                $('#ModalNgayNghiPartialView').modal('show');
            },
            error: function (res) {
                common.notify('Lỗi - Hãy nhập đầy đủ thông tin', 'error');
            },
        })
    }
    //Thêm năm
    var btnCallFormAddNamEvent = function () {
        $('body').on('click', '#btnCallFormAddNam', function (e) {
            e.preventDefault();
            $('#txtNoiDung').val('');
            $('#txtNgayNghi').val(0);
            $('#txtThangNghi').val(0);
            var d = new Date();
            var year = d.getFullYear();
            $('#txtNamNghi').val(year);
            $('#txtNgayKetThuc').val(0);
            $('#txtThangKetThuc').val(0);
            $('#txtNamKettThuc').val(year);
            $('#txtNgayNghiId').val(0);
            $('#ModalNgayNghiPartialView').modal('show');
        });
    }
    var btnSaveNgayNghiEvent = function () {
        $('body').on('click', '#btnSaveNgayNghi', function (e) {
            e.preventDefault();
            var noiDung = $('#txtNoiDung').val();
            var ngayNghi = $('#txtNgayNghi').val();
            var thangNghi = $('#txtThangNghi').val();
            var namNghi = $('#txtNamNghi').val();
            var ngayKetThuc = $('#txtNgayKetThuc').val();
            var thangKetThuc = $('#txtThangKetThuc').val();
            var namKetThuc = $('#txtNamKetThuc').val();
            var id = $('#txtNgayNghiId').val();

            if (noiDung == "" || ngayNghi == 0 || thangNghi == 0 || namNghi == 0) {
                common.notify('Nhập đầy đủ thông tin', 'error')
                return false;
            }

            var ngayNghi = ngayNghi + '/' + thangNghi + '/' + namNghi;
            var ngayKetThuc = ngayKetThuc + '/' + thangKetThuc + '/' + namKetThuc;

            
            
            btnSaveNgayNghiChildren(id,noiDung, ngayNghi, ngayKetThuc)
        });
    }
    //Thêm ngày nghỉ
    var btnAddNgayNghiChildrenEvent = function () {
        $('body').on('click', '#btnAddNgayNghiChildren', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $('#txtNamId').val(id);
            var nam = $('#txtNamInGetChildren').val();
            $('#txtNamInModelAdd').val(nam);
            $('#ModalNgayNghiAddChildrenPartialView').modal('show');
        });
    }
    
    var btnSaveNgayNghiChildren = function (id,noiDung, ngayNghi, ngayKetThuc) {
        $.ajax({
            type: 'POST',
            data:
            { 
                TuNgay: ngayNghi,
                DenNgay: ngayKetThuc,
                NoiDung: noiDung,
                Id:id
            },
            url: '/Admin/NgayNghi/Add',
            success: function (res) {
                $('#ModalNgayNghiPartialView').modal('hide');
                LoadTableNgayNghi();
                common.notify('Thành công', 'success')

            },
            error: function (res) {
                common.notify('Lỗi - Hãy nhập đầy đủ thông tin', 'error');
            },
        })
    }
    
    function Delete(id) {
        common.confirm('Bạn muốn xóa ngày nghỉ? ', function () {
            $.ajax({
                type: "POST",
                url: "/Admin/NgayNghi/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableNgayNghi();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa', 'error');
                },
            });
        });
    }
    
    var DelEvents = function () {
        $('body').on('click', '#btnDelNgayNghiChildren', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            Delete(id);
        });
    }

   
    function LoadTableNgayNghi() {
        $.ajax({
            type: "GET",
            url: "/Admin/NgayNghi/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-NgayNghi').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    if (item.denNgay != null) {
                        var ngayNghi = common.dateFormatJson(item.tuNgay) + ' - ' + common.dateFormatJson(item.denNgay);
                    }
                    else {
                        var ngayNghi = common.dateFormatJson(item.tuNgay);
                    }
                    render += Mustache.render(template, {
                        Id:item.id,
                        STT: Stt++,
                        TuNgay: item.tuNgay,
                        DenNgay: item.denNgay,
                        NoiDung: item.noiDung,
                        NgayNghi: ngayNghi
                    });
                });
                if (render != '') {
                    $('#tbl-content-NgayNghi').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}