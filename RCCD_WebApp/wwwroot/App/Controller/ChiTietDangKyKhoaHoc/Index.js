﻿var chiTietDangKyKhoaHocController = function ()
{
    this.initialize = function ()
    {
        LoadDanhSachKhachHang();
        BtnShowDanhSachHocVienInGetByIdEvent();
        CallAddFileHocVienModalEvent();
        AddFileHocVienEvent();
        LoadHocVienByKhoaHocIdEvent();
    }
    //nhấn nút btnShowDanhSachHocVienInGetById show danh sách học viên trong modal
    var BtnShowDanhSachHocVienInGetByIdEvent = function () {
        $('body').on('click', '#btnShowDanhSachHocVienInGetById', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            BtnShowDanhSachHocVienInGetById(id);
        });
    }
    function BtnShowDanhSachHocVienInGetById(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietDangKyKhoaHoc/GetByKhoaHocId",
            data: {
                id: id
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-ModalDanhSachHocVienInGetById').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenHocVien = '';
                    var soDienThoai = '';
                    var email = '';
                    $.each(listKhachHang, function (j, itemj) {
                        if (item.khachHangId == itemj.id) {
                            tenHocVien = itemj.hoVaTen;
                            soDienThoai = itemj.soDienThoai;
                            email = itemj.email;
                            return false;
                        }
                    });
                    html = '<input type="checkbox" />';
                  
                    render += Mustache.render(template, {
                        STT: Stt++,
                        HoVaTen: tenHocVien,
                        SoDienThoai: soDienThoai,
                        Email: email,
                        Duyet: html
                    });
                });
                if (render != '') {
                    $('#tbl-content-ModalDanhSachHocVienInGetById').html(render);
                }
                $('#ModalDanhSachHocVienInGetById').modal('show');
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //-----------------------------------------------------------------------------
    //lấy danh sách khách hàng để hiển thị tên khách hàng trong bảng danh sách học viên
    var listKhachHang = '';
    function LoadDanhSachKhachHang() {
        $.ajax({
            type: "GET",
            url: "/Admin/KhachHang/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listKhachHang = data;
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //--NHập file học viên
    var CallAddFileHocVienModalEvent = function () {
        $('body').on('click', '#btnCallAddFileHocVienModal', function (e) {
            e.preventDefault();
            $('#ImportHocVienModal').modal('show');
        });
    }
    var AddFileHocVienEvent = function () {
        $('body').on('click', '#btnAddFileHocVien', function (e) {
            e.preventDefault();

            var fileHocVien = $("#fileHocVien").get(0);
            var files = fileHocVien.files;
            var khoaHocId = $('#txtKhoaHocId').val();
           
            // Create FormData object  
            //fileData.set('khoaHocId', $('#txtKhoaHocId').val());
            var fileData = new FormData();
            fileData.append('khoaHocId', $('#txtKhoaHocId').val());
            // Looping over all files and add it to FormData object  
            for (var i = 0; i < files.length; i++) {
                fileData.append("files", files[i]);
            }
            $.ajax({
                url: '/Admin/ChiTietDangKyKhoaHoc/AddFileHocVien',
                type: 'POST',
                data: fileData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data) {
                    $('#ImportHocVienModal').modal('hide');
                    loadData();

                }
            });
            return false;
            
        });
    }
    function AddFileHocVien(fileHocVien, khoaHocId) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietDangKyKhoaHoc/AddFileHocVien",
            data: {
                files: fileHocVien,
                khoaHocId: khoaHocId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
               
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
       
    }
    //--Thêm học viên vào chi tiết đăng ký khóa học
    //var CallAddFileHocVienModalEvent = function () {
    //    $('body').on('click', '#btnCallAddFileHocVienModal', function (e) {
    //        e.preventDefault();
    //        $('#ImportHocVienModal').modal('show');
    //    });
    //}
    //--Show danh sách học viên của khóa học----
    var LoadHocVienByKhoaHocIdEvent = function () {
        $('body').on('click', '#profile-tab2', function (e) {
            e.preventDefault();
            var khoaHocId = $("#txtKhoaHocId").val();

            LoadHocVienByKhoaHocId(khoaHocId);
        });
    }

    function LoadHocVienByKhoaHocId(khoaHocId) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietDangKyKhoaHoc/GetByKhoaHocId",
            data: {
                id: khoaHocId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-ListHocVienCuaLopHoc').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        Id: item.id,
                        HoVaTen: item.hoVaTen,
                        SDT: item.soDienThoai,
                        Email: item.email,
                        TrangThai: 1
                    });
                });
                if (render != '') {
                    $('#tbl-content-ListHocVienCuaLopHoc').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
//--------------------------------------------------
}