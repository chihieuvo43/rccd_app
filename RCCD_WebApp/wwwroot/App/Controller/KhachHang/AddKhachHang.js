﻿var addKhachHangController = function () {
    
    this.initialize = function () {
        AddKhachHangEvent();
    }
    //Nhấn nút btnFromTongHocPhiToThanhToan chuyển số tiền từ ô tổng học phí sang thanh toán
    var AddKhachHangEvent = function () {
        $('body').on('click', '#btnAddKhachHangMoi', function (e) {
            e.preventDefault();
            var hoVaTen = $('#txtTenKhachHangMoi').val();
            var soDienThoai = $('#txtSDTKhachHangMoi').val();
            var email = $('#txtEmailKhachHangMoi').val();
            var facebook = $('#txtFacebookKhachHangMoi').val();
            var ngaySinh = $('#txtNgaySinhKhachHangMoi').val();
            if (hoVaTen != null && soDienThoai != null && email != null) {
                AddKhachHang(hoVaTen, email, soDienThoai, facebook, ngaySinh);
            }
            else {
                common.notify('Nhập đầy đủ thông tin', 'error');
            }
           
            
        });
    }
    //Khi khi tạo phiếu thu thì lưu vào bảng chi tiết đăng ký khóa học luôn, lưu học viên vào danh sách lớp.
    function AddKhachHang(hoVaTen, email, soDienThoai, facebook, ngaySinh) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhachHang/Add",
            data: {
                HoVaTen: hoVaTen,
                SoDienThoai: soDienThoai,
                Email: email,
                Facebook: facebook,
                NgaySinh: ngaySinh
            },
            beforeSend: function () {
               
            },
            success: function (response) {
                var data = response;
                var Message = data.Message;
                var Response = data.Response;
                var Type = data.Type;
                if (Response == "False") {
                    common.notify(Message, Type);
                }
                else {
                    common.notify(Message, Type);
                }

                common.startLoading();
            },
            error: function () {
                common.notify('Khi thêm học viên vào danh sách khóa học', 'error');
            },
        });
    }
}