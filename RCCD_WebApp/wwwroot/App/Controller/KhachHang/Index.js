﻿var khachHangController = function () {
    var MaKH = 0;
    
    this.initialize = function () {
        LoadTableKhachHang();
        AddKhachHangEvents();
        CallModalAddKhachHang();
        AddKhachHangMoiEvents();
        ShowDisplayAddPhieuThuEvents();
        ChangeSelectKhoaHoc();
        ChangeGiamGiaTM();
        ChangeGiamGiaPT();
        ChangeThanhToan();
        AddChiTietPhieuThuEvents();
        ChangeTableChiTietPhieuThu();
        TongTienPhaiDong();
        SavePhieuThu();
        FromTongHocPhiToThanhToanEvent();
    }
    //Nhấn nút btnFromTongHocPhiToThanhToan chuyển số tiền từ ô tổng học phí sang thanh toán
    var FromTongHocPhiToThanhToanEvent = function () {
        $('body').on('click', '#btnFromTongHocPhiToThanhToan', function (e) {
            e.preventDefault();
            var tongHocPhi = $('#txtTongHocPhi').val();
            var tongHocPhi = Number(tongHocPhi.replace(/[^0-9.-]+/g, ""));
            $('#txtThanhToan').val(tongHocPhi);
            //Tính lại số tiền phải đóng
            
        });
    }
    //Khi khi tạo phiếu thu thì lưu vào bảng chi tiết đăng ký khóa học luôn, lưu học viên vào danh sách lớp.
    function SaveChiTietDangKyKhoaHoc(khoaHocId, khachHangId) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietDangKyKhoaHoc/AddChiTietDangKyKhoaHoc",
            data: {
                Duyet: 1,
                KhoaHocId: khoaHocId,
                KhachHangId: khachHangId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {

            },
            error: function () {
                common.notify('Khi thêm học viên vào danh sách khóa học', 'error');
            },
        });
    }
    //Lưu chi tiết nhân viên phiếu thu
    function SaveChiTietNhanVienPhieuThuEvent(phieuThuId, nhanVienId) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietNhanVienPhieuThu/AddChiTietNhanVienPhieuThu",
            data: {
                PhieuThuId: phieuThuId,
                NhanVienId: nhanVienId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
            
            },
            error: function () {
                common.notify('Lỗi khi thêm chi tiết nhân viên phiếu thu', 'error');
            },
        });
    }
    //Lưu phiếu thu học phí
    var SavePhieuThu = function () {
        $('body').on('click', '#TaoPhieuThu', function (e) {
            $('#frmPhieuThuHocPhi').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    ThanhToan: { required: true },
                }
            });
            e.preventDefault();
            var tongTien = $('#txtTongTienPhaiDong').val();
            var tongTien = Number(tongTien.replace(/[^0-9.-]+/g, ""));
            var tienDaThanhToan = $('#txtThanhToan').val();
            var tienDaThanhToan = Number(tienDaThanhToan.replace(/[^0-9.-]+/g, ""));
            var hanDong = $('#HanDongTienHocPhi').val();
            var ghiChu = $('#GhiChuDongHocPhi').val();
            var khachHangId = $('#KhachHangId').val();
            var khoaHocId = $('#KhoaHocId').val();
            var tienConLai = $('#txtTienConLai').val();
            if (tienDaThanhToan == null) {
                common.notify('Nhập tiền thanh toán', 'error');
                return false;
            }
            if (khachHangId == null) {
                common.notify('Chọn khách hàng', 'error');
                return false;
            }
            if (khoaHocId == '') {
                common.notify('Chọn khóa học', 'error');
                return false;
            }
            if (tienConLai == '') {
                common.notify('Nhập tiền thanh toán', 'error');
                return false;
            }
            SavePhieuThuEvent(tongTien, tienDaThanhToan, hanDong, ghiChu, khachHangId, khoaHocId);
        });
    }
    function SavePhieuThuEvent(tongTien, tienDaThanhToan, hanDong, ghiChu, khachHangId, khoaHocId) {
        $.ajax({
            type: "POST",
            url: "/Admin/PhieuThu/AddPhieuThu",
            data: {
                TongTien: tongTien,
                TienDaThanhToan: tienDaThanhToan,
                HanDong: hanDong,
                GhiChu: ghiChu,
                KhachHangId: khachHangId,
                KhoaHocId: khoaHocId,
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
                var phieuThuId = '' + res.khachHangId + '' + res.khoaHocId;
                $('#tblChiTietPhieuThu > tbody  > tr').each(function () {
                    var noiDung = $('.NoiDung', this).text();
                    var donViTinh = $('.DonViTinh', this).text();
                    var donGia = $('.DonGia', this).text();
                    var soLuong = $('.SoLuong', this).text();
                    SaveCacKhoanThuKhac(noiDung, donViTinh, donGia, soLuong, phieuThuId);
                });
                var nhanVienId = 0;
                SaveChiTietNhanVienPhieuThuEvent(phieuThuId, nhanVienId);

                //var khoaHocId = khoaHocId;
                //var khachHangId = khachHangId;
                SaveChiTietDangKyKhoaHoc(khoaHocId, khachHangId);

                common.notify('Tạo phiếu thu học phí thành công', 'success');
            },
            error: function () {
                common.notify('Lỗi khi xóa khóa học', 'error');
            },
        });
    }
    function SaveCacKhoanThuKhac(noiDung,donViTinh,donGia,soLuong,phieuThuId) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietPhieuThu/AddChiTietPhieuThu",
            data: {
                NoiDung: noiDung,
                DonViTinh: donViTinh,
                DonGia: donGia,
                SoLuong: soLuong,
                PhieuThuId: phieuThuId,
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function () {


            },
            error: function () {
                common.notify('Lỗi khi thêm các khoản thu khác', 'error');
            },
        });
    }
    
    //--Tính tổng tiền học viên phải đóng
    var TongTienPhaiDong = function () {
        if (!$('#txtTongHocPhi').val('')) {
            var TongHocPhi = $('#txtTongHocPhi').val();
            var TongHocPhi = Number(TongHocPhi.replace(/[^0-9.-]+/g, ""));
        }
        if (!$('#txtTongCacKhoanThuKhac').val('')) {
            var TongCacKhoanThuKhac = $('#txtTongCacKhoanThuKhac').val();
            var TongCacKhoanThuKhac = Number(TongCacKhoanThuKhac.replace(/[^0-9.-]+/g, ""));
        }
        $('#txtTongTienPhaiDong').val(TongHocPhi + TongCacKhoanThuKhac).simpleMoneyFormat();
    }
    //------------------------------------
    //--Khi thêm hay xóa các hàng trong bảng các khoản thu khác thì cộng tổng các khoản
    //--phí trong bảng các khoản thu khác
    var ChangeTongTienChiTiet = function () {
        var txtTongCacKhoanThuKhac = $('#txtTongCacKhoanThuKhac').val();
        var txtTongCacKhoanThuKhac = Number(txtTongCacKhoanThuKhac.replace(/[^0-9.-]+/g, ""));
        var TienPhaiDong = $('#txtTongHocPhi').val();
        var TienPhaiDong = Number(TienPhaiDong.replace(/[^0-9.-]+/g, ""));
        //var TienPhaiDong = TienPhaiDong + txtTongCacKhoanThuKhac;
        //$('#txtTongHocPhi').val(TienPhaiDong).simpleMoneyFormat();
    }
    //---------------------------------------------------------------
    //--
    var AddChiTietPhieuThuEvents = function () {
        $('body').on('click', '#btnAddChiTietPhieuThu', function (e) {
            e.preventDefault();
            var NoiDung = $('#txtNoiDungPhieuThu').val();
            //var DonVi = $('#txtDonViTinhPhieuThu').val();
            var DonGia = $('#txtDonGiaPhieuThu').val();
            var SoLuong = $('#txtSoLuongPhieuThu').val();
            var ThanhTien = DonGia * SoLuong;
            var markup =
                "<tr> <td class='NoiDung'>" + NoiDung + "</td> <td class='DonGia'>" + DonGia + "</td><td class='SoLuong'>" + SoLuong + "</td><td class='ThanhTien'>" + ThanhTien + "</td><td><a class='btnDelTimeCTPT'><i class='fa fa-times-circle-o'></i></a></td></tr> ";
            $("table tbody").append(markup);
            $('.DonGia').simpleMoneyFormat();
            $('.ThanhTien').simpleMoneyFormat();
            
        });
        $('body').on('click', '.btnDelTimeCTPT', function (e) {
            //var ThanhTien = $('.ThanhTien', this).text();
            //var ThanhTien = Number(ThanhTien.replace(/[^0-9.-]+/g, ""));
            //var TienPhaiDong = TienPhaiDong - ThanhTien;
            //$('#txtTongHocPhi').val(TienPhaiDong).simpleMoneyFormat();
            $(this).closest('tr').remove();
        });
    }
    //Khi bảng các khoản thu khác thay đổi thì txtTongCacKhoanThuKhac cộng tổng các hàng thành tiền lại
    var ChangeTableChiTietPhieuThu = function () {
        $('body').on('click', '#btnAddChiTietPhieuThu', function (e) {
            var ThongThanhTienChiTiet = 0;
            $('#tblChiTietPhieuThu > tbody  > tr').each(function () {
                var ThanhTien = $('.ThanhTien', this).text();
                var ThanhTien = Number(ThanhTien.replace(/[^0-9.-]+/g, ""));
                ThongThanhTienChiTiet = ThongThanhTienChiTiet + ThanhTien;
            });
            ChangeTongTienChiTiet();
            $('#txtTongCacKhoanThuKhac').val(ThongThanhTienChiTiet).simpleMoneyFormat();
            TinhTongTienPhaiDong();
        });
        $('body').on('click', '.btnDelTimeCTPT', function (e) {
            var ThongThanhTienChiTiet = 0;
            $('#tblChiTietPhieuThu > tbody  > tr').each(function () {
                var ThanhTien = $('.ThanhTien', this).text();
                var ThanhTien = Number(ThanhTien.replace(/[^0-9.-]+/g, ""));
                ThongThanhTienChiTiet = ThongThanhTienChiTiet + ThanhTien;
            });
            $('#txtTongCacKhoanThuKhac').val(ThongThanhTienChiTiet).simpleMoneyFormat();
            TinhTongTienPhaiDong();
        });
    }
    //--Tính tổng tiền phải đóng TongHocPhi + TongCacKhoanThuKhac
    var TinhTongTienPhaiDong = function () {
        var TongHocPhi = $('#txtTongHocPhi').val();
        var TongHocPhi = Number(TongHocPhi.replace(/[^0-9.-]+/g, ""));
        var TongCacKhoanThuKhac = $('#txtTongCacKhoanThuKhac').val();
        var TongCacKhoanThuKhac = Number(TongCacKhoanThuKhac.replace(/[^0-9.-]+/g, ""));
        var TongTienPhaiDong = TongHocPhi + TongCacKhoanThuKhac;
        $('#txtTongTienPhaiDong').val(TongTienPhaiDong).simpleMoneyFormat();
    }
    //-----------------------------------------------------------
    var ChangeSelectKhoaHoc = function () {
        $('#KhoaHocId')
            .change(function () {
                var KhoaHocId = $('#KhoaHocId').val();
                $.ajax({
                    type: 'POST',
                    data:
                    {
                        Id: KhoaHocId
                    },
                    url: '/Admin/KhoaHoc/GetHocPhi',
                    success: function (res) {
                        var GiamGia = res.giamGia;
                        var HocPhiKhoaHoc = res.hocPhiKhoaHoc;
                        if (GiamGia == null) {
                            $('#txtHocPhiKhoaHoc').val(HocPhiKhoaHoc);
                            //$('#txtHocPhiKhoaHoc').formatCurrency();
                        }
                        else {
                            $('#txtHocPhiKhoaHoc').val(GiamGia);
                        }
                        var hocPhi = $('#txtHocPhiKhoaHoc').val();
                        $('#txtHocPhiKhoaHoc').simpleMoneyFormat();
                        $('#txtTongHocPhi').val(hocPhi).simpleMoneyFormat();
                        TinhTongTienPhaiDong();
                    },
                    error: function () {
                        common.notify('Lỗi khi thêm nhân viên', 'error');
                    },
                })
            })
            .change();
    }
    //--Giảm giá bằng tiền mặt
    function GiamGiaTM() {
        var HocPhi = $('#txtHocPhiKhoaHoc').val();
        var HocPhi = Number(HocPhi.replace(/[^0-9.-]+/g, ""));
        var GiamGiaTM = $('#txtGiamGiaTienMat').val();
        var GiamGiaTM = Number(GiamGiaTM.replace(/[^0-9.-]+/g, ""));
        var TienPhaiDong = HocPhi - GiamGiaTM;
        if ($('#txtGiamGiaTienMat').val() != null)
            $('#txtTongHocPhi').val(TienPhaiDong).simpleMoneyFormat();
        TinhTongTienPhaiDong();
    }
    var ChangeGiamGiaTM = function () {
        $(document).on("change, keyup", "#txtGiamGiaTienMat", GiamGiaTM);
    }
    //-----------------------------------
    //--Giảm giá bằng phần trăm
    function GiamGiaPT() {
        var HocPhi = $('#txtHocPhiKhoaHoc').val();
        var HocPhi = Number(HocPhi.replace(/[^0-9.-]+/g, ""));
        var GiamGiaPT = $('#txtGiamGiaPhanTram').val();
        var GiamGiaPT = Number(GiamGiaPT.replace(/[^0-9.-]+/g, "")) / 100;
        var TienPhaiDong = HocPhi - (HocPhi * GiamGiaPT);
        if ($('#txtGiamGiaPhanTram').val() != null)
            $('#txtTongHocPhi').val(TienPhaiDong).simpleMoneyFormat();
        TinhTongTienPhaiDong();
    }
    var ChangeGiamGiaPT = function () {
        $(document).on("change, keyup", "#txtGiamGiaPhanTram", GiamGiaPT);
    }
    //---------------------------------
    //--Nhập tiền khách hàng thanh toán vào sau đó tính ra khoản tiền còn nợ
    function TienConLai() {
        var TienPhaiDong = $('#txtTongHocPhi').val();
        var TienPhaiDong = Number(TienPhaiDong.replace(/[^0-9.-]+/g, ""));
        var ThanhToan = $('#txtThanhToan').val();
        var ThanhToan = Number(ThanhToan.replace(/[^0-9.-]+/g, ""));
        if ($('#txtThanhToan').val() != null) {
            var TienConLai = TienPhaiDong - ThanhToan;
            $('#txtTienConLai').val(TienConLai).simpleMoneyFormat();
        }
        else {
            var TienConLai = 0;
            $('#txtTienConLai').val(TienConLai).simpleMoneyFormat();
        }

    }
    var ChangeThanhToan = function () {
       
        $(document).on("change, keyup", "#txtThanhToan", TienConLai);
    }
    // --------------------------------
    var CallModalAddKhachHang = function () {
        $('body').on('click', '#btnCallModalAddKhachHang', function (e) {
            e.preventDefault();
            $('h5').text("Thêm nhân viên");
            $('input').val('');
            $('#AddKhachHangModal').modal('show');
        });
    }
    //--Thêm khách hàng----
    var AddKhachHangEvents = function () {
        $('#frmAddKhachHang').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenKhachHang: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddKhachHang', function (e) {
            e.preventDefault();
            var id = $('#txtKhachHangId').val();
            var tenKhachHang = $('#txtTenKhachHang').val();
            var email = $('#txtEmailKhachHang').val();
            var soDienThoai = $('#txtSDTKhachHang').val();
            $.ajax({
                type: 'POST',
                data:
                {
                    Id: id,
                    HoVaTen: tenKhachHang,
                    Email: email,
                    SoDienThoai: soDienThoai
                },
                url: '/Admin/KhachHang/Add',
                success: function (response) {
                    var data = response;
                    if (data == null) {
                        $('#AddKhachHangModal').modal('hide');
                        LoadTableKhachHang();
                        common.notify('Thêm thành công', 'success')
                    }
                    else {
                        common.confirm('Thông tin học viên đã tồn tại - xem thông tin ', function () {
                           // window.location.href = '/Admin/GiangVien/DetailById/' + response;
                        });
                    }
                   
                },
                error: function () {
                    common.notify('Lỗi khi thêm học viên', 'error');
                },
            })
        });
    }
    var AddKhachHang = function (id, tenKhachHang, email, soDienThoai) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id:id,
                HoVaTen: tenKhachHang,
                Email: email,
                SoDienThoai: soDienThoai
            },
            url: '/Admin/KhachHang/Add',
            success: function () {
                $('#AddKhachHangModal').modal('hide');
                LoadTableKhachHang();
                common.notify('Thêm thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi khi thêm nhân viên', 'error');
            },
        })
    }
    //--Thêm khách hàng----
    //--Thêm khách hàng mới----
    var AddKhachHangMoiEvents = function () {
        $('#frmDangKyKhachHangMoi').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenKhachHang: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddKhachHangMoi', function (e) {
            e.preventDefault();
            var tenKhachHang = $('#txtTenKhachHangMoi').val();
            var email = $('#txtEmailKhachHangMoi').val();
            var soDienThoai = $('#txtSDTKhachHangMoi').val();
            var facebook = $('#txtFacebookKhachHangMoi').val();
            var ngaySinh = $('#txtNgaySinhKhachHangMoi').val();
            var gioiTinh = 'M';
            AddKhachHangMoi(tenKhachHang, email, soDienThoai, facebook, ngaySinh, gioiTinh);
        });
    }
    var AddKhachHangMoi = function (tenKhachHang, email, soDienThoai, facebook, ngaySinh, gioiTinh) {
        $.ajax({
            type: 'POST',
            data:
            {
              
                HoVaTen: tenKhachHang,
                Email: email,
                SoDienThoai: soDienThoai,
                Facebook: facebook,
                NgaySinh: ngaySinh,
                GioiTinh: gioiTinh
            },
            url: '/Admin/KhachHang/Add',
            success: function (res) {
                MaKH = res.maKhachHang;
               
                $('#btnDKHoc').show();
                common.notify('Thêm thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi khi thêm nhân viên', 'error');
            },
        })
    }
    //--Thêm khách hàng mới----
    //--Show giao diện thêm mới phiếu thu----
    var ShowDisplayAddPhieuThuEvents = function () {
        $('body').on('click', '#btnDKHoc', function (e) {
            e.preventDefault();
            window.location.href = '/Admin/KhachHang/AddPhieuThu';
           
        });
    }
    function LoadTableKhachHang() {
        $.ajax({
            type: "GET",
            url: "/Admin/KhachHang/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-KhachHang').html();
                var render = "";
                var Stt = 1;
                
                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenKhachHang: item.hoVaTen,
                        Id: item.id,
                        Email: item.email,
                        SDT: item.soDienThoai,
                    });
                });
                $('#tbl-content-KhachHang').html(render);
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}