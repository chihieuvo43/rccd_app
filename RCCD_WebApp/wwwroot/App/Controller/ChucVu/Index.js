﻿var chucVuController = function () {
    this.initialize = function () {
        LoadTableChucVu();
        EditChucVuEvents();
        AddChucVuEvents();
        DelEvents();
        CallModalAddChucVu();
    }
    var CallModalAddChucVu = function () {
        $('body').on('click', '#btnCallModalAddChucVu', function (e) {
            e.preventDefault();
            $('h5').text("Thêm chức vụ");
            $('#txtTenChucVu').val('');
            $('#txtChucVuId').val(0);
            $('#ChucVuModal').modal('show');
        });
    }
    var EditChucVuEvents = function () {
        $('body').on('click', '#btnEditChucVu', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditChucVu(id);
        });
    }
    var AddChucVuEvents = function () {
        $('#frmAddChucVu').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenChucVu: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddChucVu', function (e) {
            e.preventDefault();
            var tenChucVu = $('#txtTenChucVu').val();
            var id = $('#txtChucVuId').val();
            AddChucVu(id,tenChucVu);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelChucVu', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenChucVu = $(this).data('content')
            Delete(id, tenChucVu);
        });
    }
    function EditChucVu(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChucVu/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtChucVuId').val(data.id);
                $('#txtTenChucVu').val(data.tenChucVu);
                $('h5').text("Sửa chức vụ");
                $('#ChucVuModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddChucVu = function (id,tenChucVu) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenChucVu: tenChucVu,
            },
            url: '/Admin/ChucVu/Add',
            success: function () {
                $('#ChucVuModal').modal('hide');
                LoadTableChucVu();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenChucVu) {
        common.confirm('Are you sure to delete? ' + tenChucVu, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/ChucVu/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableChucVu();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa chức vụ', 'error');
                },
            });
        });
    }
    function LoadTableChucVu() {
        $.ajax({
            type: "GET",
            url: "/Admin/ChucVu/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-ChucVu').html();
                Mustache.parse(template);
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenChucVu: item.tenChucVu,
                        Id: item.id
                    });
                });
                if (render != '') {
                    $('#tbl-content-ChucVu').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}