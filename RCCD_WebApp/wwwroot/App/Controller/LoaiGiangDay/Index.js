﻿var loaiGiangDayController = function () {
    this.initialize = function () {
        LoadTableLoaiGiangDay();
        EditLoaiGiangDayEvents();
        AddLoaiGiangDayEvents();
        DelEvents();
        CallModalAddLoaiGiangDay();
    }
    var CallModalAddLoaiGiangDay = function () {
        $('body').on('click', '#btnCallModalAddLoaiGiangDay', function (e) {
            e.preventDefault();
            $('h5').text("Thêm loại giảng dạy");
            $('#txtTenLoaiGiangDay').val('');
            $('#txtLoaiGiangDayId').val(0);
            $('#LoaiGiangDayModal').modal('show');
        });
    }
    var EditLoaiGiangDayEvents = function () {
        $('body').on('click', '#btnEditLoaiGiangDay', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditLoaiGiangDay(id);
        });
    }
    var AddLoaiGiangDayEvents = function () {
        $('#frmAddLoaiGiangDay').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenLoaiGiangDay: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddLoaiGiangDay', function (e) {
            e.preventDefault();
            var tenLoaiGiangDay = $('#txtTenLoaiGiangDay').val();
            var id = $('#txtLoaiGiangDayId').val();
            AddLoaiGiangDay(id,tenLoaiGiangDay);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelLoaiGiangDay', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenLoaiGiangDay = $(this).data('content')
            Delete(id, tenLoaiGiangDay);
        });
    }
    function EditLoaiGiangDay(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiGiangDay/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtLoaiGiangDayId').val(data.id);
                $('#txtTenLoaiGiangDay').val(data.tenLoaiGiangDay);
                $('h5').text("Sửa loại giảng dạy");
                $('#LoaiGiangDayModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddLoaiGiangDay = function (id,tenLoaiGiangDay) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenLoaiGiangDay: tenLoaiGiangDay,
            },
            url: '/Admin/LoaiGiangDay/Add',
            success: function () {
                $('#LoaiGiangDayModal').modal('hide');
                LoadTableLoaiGiangDay();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenLoaiGiangDay) {
        common.confirm('Are you sure to delete? ' + tenLoaiGiangDay, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/LoaiGiangDay/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableLoaiGiangDay();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    function LoadTableLoaiGiangDay() {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiGiangDay/GetAll",
            beforeSend: function (xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
                common.startLoading();
            },
            success: function (response) {
               
                var data = response;
                var template = $('#table-template-LoaiGiangDay').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenLoaiGiangDay: item.tenLoaiGiangDay,
                        Id: item.id
                    });
                });
                if (render != '') {
                    $('#tbl-content-LoaiGiangDay').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}