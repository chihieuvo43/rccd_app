﻿var khoaHocTiepTheoController = function () {
    this.initialize = function () {
        AddKhoaHocTiepTheoEvents();
        ChangeLoaiGiangDayEvents();
    }

    //Thay đổi dropdown loại giảng dạy thì dropdown loại chương trình học thay đổi theo
    function ChangeLoaiGiangDayEvents() {
        $('#LoaiGiangDayId')
            .change(function () {
                var loaiGiangDaycId = $('#LoaiGiangDayId').val();
                GetLoaiChuongTrinhHocByLoaiGiangDayId(loaiGiangDaycId);
            })
            .change();
    }

    function GetLoaiChuongTrinhHocByLoaiGiangDayId(loaiGiangDaycId) {
        $.ajax({
            type: "GET",
            data: {
                Id: loaiGiangDaycId
            },
            url: "/Admin/LoaiChuongTrinhHoc/GetByLoaiGiangDayId",
            success: function (response) {
                var data = response;
                $("#LoaiChuongTrinhHocId > option").remove();

                var mySelect = $('#LoaiChuongTrinhHocId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn loại chương trình học--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenLoaiChuongTrinhHoc)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    //----------------------------------------------------------------------------
    //Thay đổi dropdown loại chương trình học thì chương trình học thay đổi theo
    function ChangeLoaiChuongtrinhHocEvents() {
        $('#LoaiChuongTrinhHocId')
            .change(function () {
                var loaiChuongTrinhHocId = $('#LoaiChuongTrinhHocId').val();
                GetChuongTrinhHocByLoaiChuongTrinhHocId(loaiChuongTrinhHocId);
            })
            .change();
    }
    //Thay đổi giá trị trong dropdown chương trình học
    function GetChuongTrinhHocByLoaiChuongTrinhHocId(loaiChuongTrinhHocId) {
        $.ajax({
            type: "GET",
            data: {
                Id: loaiChuongTrinhHocId
            },
            url: "/Admin/ChuongTrinhHoc/GetByLoaiChuongTrinhHocId",
            success: function (response) {
                var data = response;
                $("#ChuongTrinhHocId > option").remove();

                var mySelect = $('#ChuongTrinhHocId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn chương trình học--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenChuongTrinh)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    //Khi dropdown Chương trình học thì dropdown cấp độ thay đổi theo
    function ChangeChuongtrinhHocEvents() {
        $('#ChuongTrinhHocId')
            .change(function () {
                var chuongTrinhHocId = $('#ChuongTrinhHocId').val();
                GetCapDoByChuongTrinhHocId(chuongTrinhHocId);
            })
            .change();
    }
    //Thay đổi giá trị trong dropdown chương trình học
    function GetCapDoByChuongTrinhHocId(chuongTrinhHocId) {
        $.ajax({
            type: "GET",
            data: {
                Id: chuongTrinhHocId
            },
            url: "/Admin/CapDo/GetByChuongTrinhHocId",
            success: function (response) {
                var data = response;
                $("#CapDoId > option").remove();
                var mySelect = $('#CapDoId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn cấp độ--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenCapDo)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    //--Tính thời gian kết thúc--------------------------------
    var AddKhoaHocTiepTheoEvents = function () {
        $('body').on('click', '#btnAddKhoaHoc', function (e) {
            e.preventDefault();
            var loaiGiangDayId = $('#LoaiGiangDayId').val();
            var loaiChuongTrinhHocId = $('#LoaiChuongTrinhHocId').val();
            var chuongTrinhHocId = $('#ChuongTrinhHocId').val();
            var capDoId = $('#CapDoId').val();
            var tenLop = $('#txtTenLopHoc').val();
            var hocVienToiThieu = $('#txtHocVienToiThieu').val();
            var hocVienToiDa = $('#txtHocVienToiDa').val();
            var thoiLuong = $('#txtThoiLuong').val();
            var thoiGianBatDau = $('#txtThoiGianBatDauKhoaHoc').val();
            var thoiGianKetThuc = $('#txtThoiGianKetThucKhoaHoc').val();
            var hocPhiTietHoc = $('#txtHocPhiTietHoc').val();
            var hocPhiKhoaHoc = $('#txtTongKhoaHoc').val();
            var giamGia = $('#txtGiamGia').val();
            var trangThai = $('#TrangThaiKhoaHoc').val();
            var nguoiPhuTrach = $('#NhanVienId').val();
            var giaoTrinh = $('#GiaoTrinh').val();
            var nhanVienId = $('#txtNhanVienId').val();
            var khoaHocTruoc = $('#txtKhoaHocId').val();

            AddKhoaHoc(capDoId, tenLop, hocVienToiThieu, hocVienToiDa, thoiLuong, thoiGianBatDau, thoiGianKetThuc, hocPhiTietHoc, hocPhiKhoaHoc,
                giamGia, trangThai, nguoiPhuTrach, giaoTrinh, loaiGiangDayId, loaiChuongTrinhHocId, chuongTrinhHocId, nhanVienId, khoaHocTruoc);
        });
    }
    var AddKhoaHoc = function (capDoId, tenLop, hocVienToiThieu, hocVienToiDa, thoiLuong, thoiGianBatDau, thoiGianKetThuc, hocPhiTietHoc, hocPhiKhoaHoc,
        giamGia, trangThai, nguoiPhuTrach, giaoTrinh, loaiGiangDayId, loaiChuongTrinhHocId, chuongTrinhHocId, nhanVienId, khoaHocTruoc) {
        $.ajax({
            type: 'POST',
            data:
            {
                CapDoId: capDoId,
                LoaiGiangDayId: loaiGiangDayId,
                LoaiChuongTrinhHocId: loaiChuongTrinhHocId,
                ChuongTrinhHocId: chuongTrinhHocId,
                TenLop: tenLop,
                SoLuongHocVienToiThieu: hocVienToiThieu,
                SoLuongHocVien: hocVienToiDa,
                ThoiLuong: thoiLuong,
                ThoiGianBatDau: thoiGianBatDau,
                ThoiGianKetThuc: thoiGianKetThuc,
                HocPhiTietHoc: hocPhiTietHoc,
                HocPhiKhoaHoc: hocPhiKhoaHoc,
                GiamGia: giamGia,
                TrangThai: trangThai,
                NguoiPhuTrach: nguoiPhuTrach,
                GiaoTrinh: giaoTrinh,
                NhanVienId: nhanVienId,
                LopHocTruocId: khoaHocTruoc
            },
            url: '/Admin/KhoaHoc/Add',
            success: function (res) {
                var data = res;
                if (data.Response == "True") {
                    common.notify(data.Message, data.Type);
                    window.location.href = "/Admin/KhoaHoc/DetailLopHoc/" + data.MaLop;
                }
                else if (data.Response == "Available") {
                    common.confirm(data.Message + ' Xem chi tiết ', function () {
                        window.location.href = "/Admin/KhoaHoc/DetailLopHoc/" + data.MaLop;
                    });
                }
                else {
                    common.notify(data.Message, data.Type);
                }

            },
            error: function (res) {
                common.notify('Lỗi khi thêm khóa học', 'error');
            },
        })
    }
   
}