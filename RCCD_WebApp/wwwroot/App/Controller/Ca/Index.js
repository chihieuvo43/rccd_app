﻿var caController = function () {
    this.initialize = function () {
        LoadTableCa();
        EditCaEvents();
        AddCaEvents();
        DelEvents();
        CallModalAddCa();
    }
    var CallModalAddCa = function () {
        $('body').on('click', '#btnCallModalAddCa', function (e) {
            e.preventDefault();
            $('h5').text("Thêm cấp độ");
            $('#txtTenCa').val('');
            $('#txtCaId').val(0);
            $('#txtThuTuCa').val(0);
            $('#ChuongTrinhHocId').val('');
            $('#CaModal').modal('show');
        });
    }
    var EditCaEvents = function () {
        $('body').on('click', '#btnEditCa', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditCa(id);
        });
    }
    //-------Thêm ca---------------------------------------------------------
    var AddCaEvents = function () {
        $('#frmAddCa').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenCa: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddCa', function (e) {
            e.preventDefault();
            var tenCa = $('#txtTenCa').val();
            var gioBatDau = $('#txtGioBatDau').val();
            var gioKetThuc = $('#txtGioKetThuc').val();
            var ghiChu = $('#txtGhiChu').val();
            var ngayTrongTuan = $("#NgayTrongTuanId").val();

            var test = true;
            //Kiem tra ton tai
            $.each(listCa, function (i, item) {
                if ((String(item.gioBatDau) === String(gioBatDau)) && (String(item.gioKetThuc) === String(gioKetThuc)) && ngayTrongTuan == item.ngayTrongTuan) {
                    common.notify('Ca đã tồn tại', 'error');
                    test = false;
                    return false;
                }
            });
            $.each(listCa, function (i, item) {
                if (String(item.tenCa) === String(tenCa)) {
                    common.notify('Tên ca đã tồn tại', 'error');
                    test = false;
                    return false;
                }
            });
            //Kiem tra giờ bắt đầu và giờ kết thúc có hợp lệ không
            var batDau = parseInt(gioBatDau.substring(0, 2));
            var ketThuc = parseInt(gioKetThuc.substring(0, 2));
            if (batDau >= ketThuc) {
                common.notify('Giờ bắt đầu và giờ kết thúc chưa hợp lệ', 'error');
                test = false;
            }

            if (ngayTrongTuan == 0) {
                common.notify('Chọn thứ', 'error');
                test = false;
            }
            if (test == true) {
                AddCa(tenCa, gioBatDau, gioKetThuc, ngayTrongTuan, ghiChu);
            }
            

        });
    }
    var AddCa = function (tenCa, gioBatDau, gioKetThuc, ngayTrongTuan, ghiChu) {
        $.ajax({
            type: 'POST',
            data:
            {
                TenCa: tenCa,
                GioBatDau: gioBatDau,
                GioKetThuc: gioKetThuc,
                NgayTrongTuan: ngayTrongTuan,
                GhiChu: ghiChu
            },
            url: '/Admin/Ca/Add',
            success: function () {
                LoadTableCa();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    //---------------------------------------------------------
    var DelEvents = function () {
        $('body').on('click', '#btnDelCa', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenCa = $(this).data('content')
            Delete(id, tenCa);
        });
    }
    function EditCa(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/Ca/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtCaId').val(data.id);
                $('#txtTenCa').val(data.tenCa);
                $('#ChuongTrinhHocId').val(data.chuongTrinhId);
                $('#txtThuTuCa').val(data.soThuTu);
                $('h5').text("Sửa cấp độ");
                $('#CaModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
   
    function Delete(id, tenCa) {
        common.confirm('Bạn muốn xóa? ' + tenCa, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/Ca/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableCa();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    var listCa;
    function LoadTableCa() {
        $.ajax({
            type: "GET",
            url: "/Admin/Ca/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
               
                var data = response;
                listCa = data;
                var template = $('#table-template-Ca').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenNgayTrongTuan;
                    if (item.ngayTrongTuan == 1) {
                        tenNgayTrongTuan = "Thứ 2";
                    }
                    if (item.ngayTrongTuan == 2) {
                        tenNgayTrongTuan = "Thứ 3";
                    }
                    if (item.ngayTrongTuan == 3) {
                        tenNgayTrongTuan = "Thứ 4";
                    }
                    if (item.ngayTrongTuan == 4) {
                        tenNgayTrongTuan = "Thứ 5";
                    }
                    if (item.ngayTrongTuan == 5) {
                        tenNgayTrongTuan = "Thứ 6";
                    }
                    if (item.ngayTrongTuan == 6) {
                        tenNgayTrongTuan = "Thứ 7";
                    }
                    if (item.ngayTrongTuan == 7) {
                        tenNgayTrongTuan = "Chủ nhật";
                    }

                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenCa: item.tenCa,
                        Id: item.id,
                        GioBatDau: item.gioBatDau,
                        GioKetThuc: item.gioKetThuc,
                        NgayTrongTuan: tenNgayTrongTuan,
                        GhiChu: item.ghiChu
                    });
                });
                if (render != '') {
                    $('#tbl-content-Ca').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}