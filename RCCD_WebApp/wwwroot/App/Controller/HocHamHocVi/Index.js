﻿var hocHamHocViController = function () {
    this.initialize = function () {
        LoadTableHocHamHocVi();
        EditHocHamHocViEvents();
        AddHocHamHocViEvents();
        DelEvents();
        CallModalAddHocHamHocVi();
    }
    var CallModalAddHocHamHocVi = function () {
        $('body').on('click', '#btnCallModalAddHocHamHocVi', function (e) {
            e.preventDefault();
            $('h5').text("Thêm học hàm học vị");
            $('#txtTenHocHamHocVi').val('');
            $('#txtHocHamHocViId').val(0);
            $('#HocHamHocViModal').modal('show');
        });
    }
    var EditHocHamHocViEvents = function () {
        $('body').on('click', '#btnEditHocHamHocVi', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditHocHamHocVi(id);
        });
    }
    var AddHocHamHocViEvents = function () {
        $('#frmAddHocHamHocVi').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenHocHamHocVi: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddHocHamHocVi', function (e) {
            e.preventDefault();
            var tenHocHamHocVi = $('#txtTenHocHamHocVi').val();
            var id = $('#txtHocHamHocViId').val();
            AddHocHamHocVi(id,tenHocHamHocVi);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelHocHamHocVi', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenHocHamHocVi = $(this).data('content')
            Delete(id, tenHocHamHocVi);
        });
    }
    function EditHocHamHocVi(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/HocHamHocVi/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtHocHamHocViId').val(data.id);
                $('#txtTenHocHamHocVi').val(data.tenHocHamHocVi);
                $('h5').text("Sửa học hàm học vị");
                $('#HocHamHocViModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddHocHamHocVi = function (id,tenHocHamHocVi) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenHocHamHocVi: tenHocHamHocVi,
            },
            url: '/Admin/HocHamHocVi/Add',
            success: function () {
                $('#HocHamHocViModal').modal('hide');
                LoadTableHocHamHocVi();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenHocHamHocVi) {
        common.confirm('Are you sure to delete? ' + tenHocHamHocVi, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/HocHamHocVi/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableHocHamHocVi();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa học hàm học vị', 'error');
                },
            });
        });
    }
    function LoadTableHocHamHocVi() {
        $.ajax({
            type: "GET",
            url: "/Admin/HocHamHocVi/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-HocHamHocVi').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenHocHamHocVi: item.tenHocHamHocVi,
                        Id: item.id
                    });
                });
                if (render != '') {
                    $('#tbl-content-HocHamHocVi').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}