﻿var phongController = function () {
    this.initialize = function () {
        EditPhongEvents();
        AddPhongEvents();
        DelEvents();
        SaveChangePhongEvent();
        LoadTablePhong();
        CallFormAddPhongEvent();
    }
    var CallFormAddPhongEvent = function () {
        $('body').on('click', '#btnCallFormAddPhong', function (e) {
            e.preventDefault();
            $('h5').text('Thêm phòng');
            $('#txtTenPhong').val('');
            $('#txtPhongId').val('');
            $('#PhongModal').modal('show');
        });
    }
    var EditPhongEvents = function () {
        $('body').on('click', '#btnEditPhong', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditPhong(id);
        });
    }
    //-------------------Thêm phòng---------------------------------
    var AddPhongEvents = function () {
        $('#frmAddPhong').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenPhong: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddPhong', function (e) {
            e.preventDefault();
            var tenPhong = $('#txtTenPhong').val();
            var sucChua = $('#txtSucChua').val();
            var trangThai = $('#TrangThaiId').val();
            var id = $('#txtPhongId').val();

            var test = true;
            if (tenPhong == "")
            {
                test = false;
                common.notify('Nhập tên phòng', 'error');
            }
            if (test == true) {
                AddPhong(id, tenPhong, sucChua, trangThai);
            }
           
        });
    }
    var AddPhong = function (id, tenPhong, sucChua, trangThai) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenPhong: tenPhong,
                SucChua: sucChua,
                TrangThai: trangThai
            },
            url: '/Admin/Phong/Add',
            success: function (res) {
                $('#PhongModal').modal('hide');
                LoadTablePhong();
                common.notify('Thành công', 'success')
            },
            error: function (res) {
                common.notify('Lỗi', 'error');
            },
        })
    }
    //--------------------------------------------------------
    //-------Xóa phòng---------------------------------------
    var DelEvents = function () {
        $('body').on('click', '#btnDelPhong', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenPhong = $(this).data('content')
            Delete(id, tenPhong);
        });
    }
    function Delete(id, tenPhong) {
        common.confirm('Xác nhận xóa phòng? ' + tenPhong, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/Phong/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTablePhong();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa phòng', 'error');
                },
            });
        });
    }
    //--------------------------------------------------------
    var SaveChangePhongEvent = function () {
        $('body').on('click', '#btnSaveChangePhong', function (e) {
            e.preventDefault();
            var id = $('#txtEditPhongId').val();
            var tenPhong = $('#txtEditTenPhong').val();
            SaveChangePhong(id, tenPhong);
        });
    }
    function EditPhong(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/Phong/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtPhongId').val(data.id);
                $('#txtTenPhong').val(data.tenPhong);
                $('h5').text('Sửa phòng');
                $('#PhongModal').modal('show');
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    
   
   
    function LoadTablePhong() {
        $.ajax({
            type: "GET",
            url: "/Admin/Phong/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-Phong').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenTrangThai = '<span class="label label-primary">Hoạt động</span>';
                    if (item.trangThai == 2) {
                        tenTrangThai = '<span class="label label-danger">Tạm hoãn</span>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenPhong: item.tenPhong,
                        Id: item.id,
                        SucChua:item.sucChua,
                        TrangThai: tenTrangThai
                    });
                });
                if (render != '') {
                    $('#tbl-content-Phong').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}