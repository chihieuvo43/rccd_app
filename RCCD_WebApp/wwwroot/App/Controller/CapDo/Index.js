﻿var capDoController = function () {
    this.initialize = function () {
        ListChuongTrinhHoc();
       // LoadTableCapDo();
        EditCapDoEvents();
        AddCapDoEvents();
        DelEvents();
        CallModalAddCapDo();
    }
    var CallModalAddCapDo = function () {
        $('body').on('click', '#btnCallModalAddCapDo', function (e) {
            e.preventDefault();
            $('h5').text("Thêm cấp độ");
            $('#txtTenCapDo').val('');
            $('#txtCapDoId').val(0);
            $('#txtThuTuCapDo').val(0);
            $('#ChuongTrinhHocId').val('');
            $('#CapDoModal').modal('show');
        });
    }
    var EditCapDoEvents = function () {
        $('body').on('click', '#btnEditCapDo', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditCapDo(id);
        });
    }
    var AddCapDoEvents = function () {
        $('#frmAddCapDo').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenCapDo: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddCapDo', function (e) {
            e.preventDefault();
            var tenCapDo = $('#txtTenCapDo').val();
            var id = $('#txtCapDoId').val();
            var chuongTrinhId = $('#ChuongTrinhHocId').val();
            var thuTuCapDo = $('#txtThuTuCapDo').val();
            AddCapDo(id, tenCapDo, chuongTrinhId, thuTuCapDo);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelCapDo', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenCapDo = $(this).data('content')
            Delete(id, tenCapDo);
        });
    }
    function EditCapDo(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/CapDo/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtCapDoId').val(data.id);
                $('#txtTenCapDo').val(data.tenCapDo);
                $('#ChuongTrinhHocId').val(data.chuongTrinhId);
                $('#txtThuTuCapDo').val(data.soThuTu);
                $('h5').text("Sửa cấp độ");
                $('#CapDoModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddCapDo = function (id, tenCapDo, chuongTrinhId,thuTuCapDo) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenCapDo: tenCapDo,
                ChuongTrinhId: chuongTrinhId,
                SoThuTu: thuTuCapDo
            },
            url: '/Admin/CapDo/Add',
            success: function () {
                $('#CapDoModal').modal('hide');
                LoadTableCapDo();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenCapDo) {
        common.confirm('Are you sure to delete? ' + tenCapDo, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/CapDo/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableCapDo();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    var listChuongTrinhHoc;
    function ListChuongTrinhHoc() {
        $.ajax({
            type: "GET",
            url: "/Admin/ChuongTrinhHoc/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listChuongTrinhHoc = data;
                LoadTableCapDo();
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    function LoadTableCapDo() {
        $.ajax({
            type: "GET",
            url: "/Admin/CapDo/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
               
                var data = response;
                var template = $('#table-template-CapDo').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenChuongTrinhHoc;
                    $.each(listChuongTrinhHoc, function (j, itemj) {
                        if (item.chuongTrinhId == itemj.id) {
                            tenChuongTrinhHoc = itemj.tenChuongTrinh;
                            return false;
                        }
                    }),
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenCapDo: item.tenCapDo,
                        Id: item.id,
                        TenChuongTrinhHoc: tenChuongTrinhHoc,
                        SoThuTu: item.soThuTu
                    });
                });
                if (render != '') {
                    $('#tbl-content-CapDo').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}