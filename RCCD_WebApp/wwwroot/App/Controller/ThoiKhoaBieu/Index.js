﻿var thoiKhoaBieuController = function () {
    this.initialize = function () {
        LoadPhongHocBySoLuongHocVienToiDaEvent();
        LoadGiangVienByLoaiGiangDayEvent();
        LoadThoiKhoaBieuByKhoaHocIdEvent();
        AddThoiKhoaBieuEvent();
        DeleteThoiKhoaBieuEvent();
        GetGiangVienByCaIdEvents();
        GetPhongByCaIdEvents();
        TinhThoiGianKetThucEvents();
        ThucHienKhoaHocEvent();
        KetThucKhoaHocEvent();
    }
    //--Tính thời gian kết thúc--------------------------------
    var TinhThoiGianKetThucEvents = function () {
        $('body').on('click', '#txtTinhThoiGianKetThuc', function (e) {
            e.preventDefault();
            var khoaHocId = $("#txtKhoaHocId").val();
            TinhThoiGianKetThuc(khoaHocId);
        });
    }
    function TinhThoiGianKetThuc(khoaHocId) {
        $.ajax({
            type: "GET",
            url: "/Admin/KhoaHoc/TinhThoiGianKetThuc",
            data: {
                id: khoaHocId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var check = true;
                if (data == "") {
                    check = false;
                }
                if (check == true) {
                    $("#txtThoiGianKetThucDuKien").val(data.thoiGianKetThuc);
                    common.notify('Tính ngày kết thúc thành công', 'success');
                    location.reload();
                }
                else {
                    common.notify('Chưa có biểu học nào', 'error');
                }
               
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //----------------------------------------------------------
    //--Hiển thị danh sách phòng trống theo ca
    function GetPhongByCaIdEvents() {
        $('#CaId')
            .change(function () {
                var caId = $('#CaId').val();
                var hocVienToiDa = $('#txtHocVienToiDa').val();
                GetPhongByCaId(caId, hocVienToiDa);
            })
            .change();
    }
    function GetPhongByCaId(caId, hocVienToiDa) {
        $.ajax({
            type: "GET",
            data: {
                Id: caId,
                HocVienToiDa: hocVienToiDa
            },
            url: "/Admin/KhoaHoc/PhongByCaId",
            success: function (response) {

                var data = response;
                var template = $('#table-template-PhongBySoLuongHocVienToiDa').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenTrangThai = '<span class="label label-primary">Hoạt động</span>';
                    var radio = '<input type="radio" class="flat" name="ChonPhong" value=' + item.id + '>'
                    if (item.trangThai == 2) {
                        tenTrangThai = '<span class="label label-danger">Tạm hoãn</span>';
                        radio = '<input type="radio" class="flat" name="ChonPhong" value=' + item.id + ' disabled="disabled">'
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenPhong: item.tenPhong,
                        Id: item.id,
                        TrangThai: tenTrangThai,
                        ChonPhong: radio,
                        SoLuongHocVienToiDa: item.sucChua

                    });
                });
                if (render != '') {
                    $('#tbl-content-PhongBySoLuongHocVienToiDa').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                console.log(status);
                //common.notify('Cannot loading data', 'error');
            }
        });
    }
   
    //-----------------------------------------------------------
    //--Chọn ca sao đó hiển thị danh sách giảng viên có ca trống
    function GetGiangVienByCaIdEvents() {
        $('#CaId')
            .change(function () {
                var caId = $('#CaId').val();
                var loaiGiangDayId = $('#txtLoaiGiangDayId').val();
                GetGiangVienByCaId(caId, loaiGiangDayId, true);
            })
            .change();
    }
    function GetGiangVienByCaId(caId, loaiGiangDayId, isPageChanged) {
        $.ajax({
            type: "GET",
            data: {
                Id: caId,
                Page: common.configs.pageIndex,
                LoaiGiangDayId: loaiGiangDayId
            },
            url: "/Admin/KhoaHoc/GiangVienByCaId",
            success: function (response) {

                var count = response.rowCount;

                var data = response.results;
                
                var template = $('#table-template-GiangVienByLoaiGiangDayId').html();
                var render = "";

                var Stt = 1;

                $.each(data, function (i, item) {
                    var troGiang = '<input type="checkbox" class="flat" name="TroGiang" value=' + item.id + '>';
                    var radio = '<input type="radio" class="flat" name="ChonGiangVien" value=' + item.id + '>';
                    var tenLoaiGiangVien = '<span class="label label-success ">Part-time</span>';
                    if (item.loaiGiangVien == 2) {
                        tenLoaiGiangVien = '<span class="label label-warning ">Full-time</span>';
                    }

                    render += Mustache.render(template, {
                        STT: Stt++,
                        Id: item.id,
                        HoVaTen: item.hoVaTen,
                        LoaiGiangVien: tenLoaiGiangVien,
                        TroGiang: troGiang,
                        ChonGiangVien: radio,

                    });
                });


                //if (render != '') {
                    $('#tbl-content-GiangVienByLoaiGiangDayId').html(render);
                //}
                //else {
                //    render += Mustache.render(template, {
                //        STT: 0,
                //        Id: 0,
                //        HoVaTen: 0,
                //        LoaiGiangVien: 0,
                //        TroGiang: 0,
                //        ChonGiangVien:0,

                //    });
                //}
                //if (count == 0) {
                //    count = 1;
                //}
               
                //wrapPagingGetGiangVienByCaId(count, function () {
                //    GetGiangVienByCaId();
                //}, isPageChanged);

               
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function wrapPagingGetGiangVienByCaId(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        //if ($('#paginationUL a').length === 0 || changePageSize === true) {
        //    $('#paginationUL').empty();
        //    $('#paginationUL').removeData("twbs-pagination");
        //    $('#paginationUL').unbind("page");
        //}
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            first: 'Đầu',
            prev: 'Trước',
            next: 'Tiếp',
            last: 'Cuối',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callBack(), 200);
            }
        });
        //break;
    }
    //----------------------------------------------------------
      //--Show phòng học theo số lượng tối đa của khóa học
    var LoadPhongHocBySoLuongHocVienToiDaEvent = function () {
        $('body').on('click', '#profile-tab3', function (e) {
            e.preventDefault();
            var soLuongHocVienToiDa = $("#txtSoLuongHocVienHienTai").val();
            if (soLuongHocVienToiDa == null || soLuongHocVienToiDa == "") {
                soLuongHocVienToiDa = 0;
            }
            LoadPhongHocBySoLuongHocVienToiDa(soLuongHocVienToiDa);
        });
    }
  
    function LoadPhongHocBySoLuongHocVienToiDa(soLuongHocVienToiDa) {
        $.ajax({
            type: "GET",
            url: "/Admin/Phong/GetBySoLuongHocVienToiDa",
            data: {
                id: soLuongHocVienToiDa
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-PhongBySoLuongHocVienToiDa').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenTrangThai = '<span class="label label-primary">Hoạt động</span>';
                    var radio = '<input type="radio" class="flat" name="ChonPhong" value=' + item.id +'>'
                    if (item.trangThai == 2) {
                        tenTrangThai = '<span class="label label-danger">Tạm hoãn</span>';
                        radio = '<input type="radio" class="flat" name="ChonPhong" value=' + item.id+' disabled="disabled">'
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenPhong: item.tenPhong,
                        Id: item.id,
                        TrangThai: tenTrangThai,
                        ChonPhong: radio,
                        SoLuongHocVienToiDa: item.sucChua
                       
                    });
                });
                if (render != '') {
                    $('#tbl-content-PhongBySoLuongHocVienToiDa').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
//--------------------------------------------------
//--Show giảng viên theo loại giảng dạy của khóa học
    var LoadGiangVienByLoaiGiangDayEvent = function () {
        $('body').on('click', '#profile-tab3', function (e) {
            e.preventDefault();
            var loaiGiangDay = $("#txtLoaiGiangDay").val();
          
            LoadGiangVienByLoaiGiangDay(loaiGiangDay);
        });
    }

    function LoadGiangVienByLoaiGiangDay(loaiGiangDay) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietChuyenMonGiangVien/GetByLoaiGiangDayId",
            data: {
                id: loaiGiangDay
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-GiangVienByLoaiGiangDayId').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var troGiang = '<input type="checkbox" class="flat" name="TroGiang" value=' + item.id + '>';
                    var radio = '<input type="radio" class="flat" name="ChonGiangVien" value=' + item.id + '>';
                    var tenLoaiGiangVien = '<span class="label label-success ">Part-time</span>';
                    if (item.loaiGiangVien == 2) {
                        tenLoaiGiangVien = '<span class="label label-warning ">Full-time</span>';
                    }

                    render += Mustache.render(template, {
                        STT: Stt++,
                        Id: item.id,
                        HoVaTen: item.hoVaTen,
                        LoaiGiangVien: tenLoaiGiangVien,
                        TroGiang: troGiang,
                        ChonGiangVien: radio,
                       
                    });
                });
                if (render != '') {
                    $('#tbl-content-GiangVienByLoaiGiangDayId').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
//--------------------------------------------------
//--Show thời khóa biểu theo khóa học id
    var LoadThoiKhoaBieuByKhoaHocIdEvent = function () {
        $('body').on('click', '#profile-tab3', function (e) {
            e.preventDefault();
            var khoaHocId = $("#txtKhoaHocId").val();

            LoadThoiKhoaBieuByKhoaHocId(khoaHocId);
        });
    }

    function LoadThoiKhoaBieuByKhoaHocId(khoaHocId) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietGiangDay/GetByKhoaHocId",
            data: {
                id: khoaHocId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-ThoiKhoaBieu').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var thu;
                    if (item.thu == 1) {
                        thu ='<span class="label label-default">Thứ 2</span>'
                    }
                    if (item.thu == 2) {
                        thu = '<span class="label label-primary">Thứ 3</span>'
                    }
                    if (item.thu == 3) {
                        thu = '<span class="label label-danger">Thứ 4</span>'
                    }
                    if (item.thu == 4) {
                        thu = '<span class="label label-warning">Thứ 5</span>'
                    }
                    if (item.thu == 5) {
                        thu = '<span class="label label-info">Thứ 6</span>'
                    }
                    if (item.thu == 6) {
                        thu = '<span class="label label-success">Thứ 7</span>'
                    }
                    if (item.thu == 7) {
                        thu = '<span class="badge badge-light">Chủ nhật</span>'
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        Id: item.id,
                        Ca: item.tenCa,
                        GiangVien: item.tenGiangVien,
                        TroGiang: item.troGiang,
                        Phong: item.tenPhong,
                        GhiChuThoiKhoaBieu: item.ghiChu,
                        Thu: thu

                    });
                });
                if (render != '') {
                    $('#tbl-content-ThoiKhoaBieu').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
//--------------------------------------------------
//--Thêm thời khóa biểu-----------------------------
    var AddThoiKhoaBieuEvent = function () {
        $('body').on('click', '#btnAddThoiKhoaBieu', function (e) {
            e.preventDefault();
            var ghiChu = $("#txtGhiChuThoiKhoaBieu").val();
            var caId = $("#CaId").val();
            var phongId = $("input[name='ChonPhong']:checked").val();
            var giangVienId = $("input[name='ChonGiangVien']:checked").val();
            var troGiang = $("input[name='TroGiang']:checked").val();
            var khoaHocId = $("#txtKhoaHocId").val();

            if (caId == 0 || phongId == null || giangVienId == null) {
                common.notify('Nhập đầy đủ thông tin', 'error');
            }
            else {
                AddThoiKhoaBieu(ghiChu, caId, phongId, giangVienId, troGiang, khoaHocId);

            }

        });
    }
    function AddThoiKhoaBieu(ghiChu, caId, phongId, giangVienId, troGiang, khoaHocId) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietGiangDay/Add",
            data: {
                GhiChu: ghiChu,
                CaId: caId,
                PhongHocId: phongId,
                GiangVienId: giangVienId,
                TroGiang: troGiang,
                KhoaHocId: khoaHocId

            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var result = data.Response;
                var message = data.Message;
                var type = data.Type;
                if ( result== "false") {
                    
                    common.notify(message, type);
                }
                else {
                    common.notify(message, type);
                    var khoaHocId = $("#txtKhoaHocId").val();
                    LoadThoiKhoaBieuByKhoaHocId(khoaHocId);
                }
                
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //--Xóa thời khóa biểu-----------------------------
    var DeleteThoiKhoaBieuEvent = function () {
        $('body').on('click', '#btnDeleteThoiKhoaBieu', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
           

           DeleteThoiKhoaBieu(id);
        });
    }
    function DeleteThoiKhoaBieu(id) {
        common.confirm('Bạn muốn xóa? ', function () {
            $.ajax({
                type: "POST",
                url: "/Admin/ChiTietGiangDay/Delete",
                data: {
                    id: id
                },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    common.notify('Xóa thời khóa biểu thành công', 'success');
                    var khoaHocId = $("#txtKhoaHocId").val();
                    LoadThoiKhoaBieuByKhoaHocId(khoaHocId);
                    common.stopLoading();
                },
                error: function (status) {
                    common.notify('Có lỗi xảy ra', 'error');
                    common.stopLoading();
                }
            });
        });
    }
    var ThucHienKhoaHocEvent = function () {
        $('body').on('click', '#btnThucHien', function (e) {
            e.preventDefault();
            var trangThai = 2;
            var khoaHocId = $('#txtKhoaHocId').val();
            EditKhoaHoc(khoaHocId, trangThai);
        });
    }
    var KetThucKhoaHocEvent = function () {
        $('body').on('click', '#btnKetThuc', function (e) {
            e.preventDefault();
            var trangThai = 4;
            var khoaHocId = $('#txtKhoaHocId').val();
            EditKhoaHoc(khoaHocId, trangThai);
        });
    }
    EditKhoaHoc = function (khoaHocId, trangThai) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhoaHoc/EditTrangThai",
            data: {
                khoaHocId: khoaHocId,
                trangThai: trangThai
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
                var data = res;
                var Response = data.Response;
                var Messsage = data.Message;
                var Type = data.Type;

                if (Response == "True") {
                    location.reload();
                }
                common.notify(Messsage, Type);
                
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        });
    }
}