﻿var chiTietGiangDayController = function () {
    this.initialize = function () {
        DetailKhoaHocEvent();
        DeleteChiTietGiangDayEvent();
        BtnEditChiTietGiangDayEvent();
        BtnSaveChangeEditThoiGianHocInGetByIdEvent();
    }
    //Sửa một dòng chi tiết giảng dạy
    //Gọi thông tin lên modal ModalEditThoiGianHocInGetByIdPartialView
    //Nhấn nút btnEditChiTietGiangDay ở listChiTietGiangDay, lấy id 
    var BtnEditChiTietGiangDayEvent = function () {
        $('body').on('click', '#btnEditChiTietGiangDay', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            ModalEditThoiGianHocInGetByIdPartialView(id);
        });
    }
    //Show thông tin lên modal
    var ModalEditThoiGianHocInGetByIdPartialView = function (id) {
        $.ajax({
            type: "GET",
            data: { id: id },
            url: "/Admin/ChiTietGiangDay/Get",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtEditChiTietGiangDayId').val(data.id);
                $('#EditNgay').val(data.ngayTrongTuan);
                $('#txtEditGioBatDau').val(data.thoiGianBatDau);
                $('#txtEditKhoaHocId').val(data.khoaHocId);
                $('#txtEditGioKetThuc').val(data.thoiGianKetThuc);
                $('.PhongId').val(data.phongHocId);
                $('.GiangVienId').val(data.giangVienId);
                $('#ModalEditThoiGianHocInGetByIdPartialView').modal('show');
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //Lưu thông tin chi tiết giảng dạy vừa sửa
    //Nhấn nút btnSaveChangeEditThoiGianHocInGetById
    var BtnSaveChangeEditThoiGianHocInGetByIdEvent = function () {
        $('body').on('click', '#btnSaveChangeEditThoiGianHocInGetById', function (e) {
            e.preventDefault();
            var id= $('#txtEditChiTietGiangDayId').val();
            var ngay= $('#EditNgay').val();
            var gioiBatDau= $('#txtEditGioBatDau').val();
            //var khoaHocId= $('#txtEditKhoaHocId').val();
            var gioiKetThuc= $('#txtEditGioKetThuc').val();
            var phong= $('#PhongId').val();
            var giangVien= $('#GiangVienId').val();
            BtnSaveChangeEditThoiGianHocInGetById(id, ngay, gioiBatDau, gioiKetThuc, phong, giangVien);
        });
    }
    var BtnSaveChangeEditThoiGianHocInGetById = function (id, ngay, gioiBatDau, gioiKetThuc, phong, giangVien) {
        $.ajax({
            type: "POST",
            data: {
                Id: id,
                ThoiGianBatDau: gioiBatDau,
                ThoiGianKetThuc: gioiKetThuc,
                NgayTrongTuan: ngay,
                PhongHocId: phong,
               // KhoaHocId: khoaHocId,
                GiangVienId: giangVien
            },
            url: "/Admin/ChiTietGiangDay/Edit",
            beforeSend: function () {
                common.startLoading();
            },
            success: function () {
                $('#ModalEditThoiGianHocInGetByIdPartialView').modal('hide');
                common.notify('Sửa thành công', 'success');
                location.reload();
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //-------------------------------
    //Xóa 1 chi tiết giảng dạy trong GetById
    var DeleteChiTietGiangDayEvent = function () {
        $('body').on('click', '#btnDelChiTietGiangDay', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            DeleteChiTietGiangDay(id);
        });
    }
    DeleteChiTietGiangDay = function (id) {
        common.confirm('Xóa thời gian học? ', function () {
            $.ajax({
                type: "POST",
                url: "/Admin/ChiTietGiangDay/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    location.reload();
                },
                error: function () {
                    common.notify('Lỗi khi xóa khóa học', 'error');
                },
            });
        });
    }
    //----------------------------------------
    var DetailKhoaHocEvent = function () {
        $('body').on('click', '#btnDetailKhoaHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var khoaHocId = $(this).data('content');
           
            window.location.href = '/Admin/KhoaHoc/GetById/' + id;
            ShowThoiGianHocInGetById(khoaHocId);
        });
    }
    ShowThoiGianHocInGetById = function (khoaHocId) {
        var khoaHocId = parseInt(khoaHocId); 
        $.ajax({
            type: "GET",
            data: { Id: khoaHocId },
            url: "/Admin/KhoaHoc/GetThoiGianHocValue",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-ShowThoiGianHocInGetByID').html();
                var render = "";
                var html = "<a class='btnDelTime'><i class='fa fa - times - circle - o'></i></a>";

                $.each(data, function (i, item) {
                    var NgayTrongTuan = '';
                    if (item.ngayTrongTuan == 1) {
                        NgayTrongTuan = 'Thứ 2';
                    }
                    else if (item.ngayTrongTuan == 2) {
                        NgayTrongTuan = 'Thứ 3';
                    }
                    else if (item.ngayTrongTuan == 3) {
                        NgayTrongTuan = 'Thứ 4';
                    }
                    else if (item.ngayTrongTuan == 4) {
                        NgayTrongTuan = 'Thứ 5';
                    }
                    else if (item.ngayTrongTuan == 5) {
                        NgayTrongTuan = 'Thứ 6';
                    }
                    else if (item.ngayTrongTuan == 6) {
                        NgayTrongTuan = 'Thứ 7';
                    }
                    else {
                        NgayTrongTuan = 'Chủ nhật';
                    }
                    //////Phòng
                    //var TenPhong = '';
                    //$.each(listPhong, function (j, itemj) {
                    //    if (item.phongHocId == itemj.id) {
                    //        TenPhong = itemj.tenPhong;
                    //        return false;
                    //    }
                    //    else {
                    //        TenPhong = 'Đang cập nhật';
                    //    }
                    //})
                    //////Giảng viên
                    //var TenGiangVien = '';
                    //$.each(listGiangVien, function (j, itemj) {
                    //    if (item.giangVienId == itemj.id) {
                    //        TenGiangVien = itemj.hoVaTen;
                    //        return false;
                    //    }
                    //    else {
                    //        TenGiangVien = 'Đang cập nhật';
                    //    }
                    //})
                    render += Mustache.render(template, {
                        Ngay: NgayTrongTuan,
                        GioBatDau: item.thoiGianBatDau,
                        GioKetThuc: item.thoiGianKetThuc,
                        Phong: item.phongId,
                        GiangVien: item.giangVienId,
                        HanhDong: html
                    });
                });

                if (render != '') {
                    $('#tbl-content-ShowThoiGianHocInGetByID').html(render);

                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}