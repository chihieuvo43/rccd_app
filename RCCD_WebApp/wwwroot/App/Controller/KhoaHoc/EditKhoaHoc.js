﻿var editKhoaHocController = function () {
    this.initialize = function () {
        
        ChangeLoaiGiangDayEvents();
        ChangeLoaiChuongtrinhHocEvents();
        ChangeChuongtrinhHocEvents();
        EditKhoaHocEvent();
    }
    //Lưu thay đổi khóa học
    //Nhấn nút sửa khóa học
    var EditKhoaHocEvent = function () {
        $('body').on('click', '#btEditKhoaHoc', function (e) {
            e.preventDefault();
            var id = $('#txtEditId').val();
            var maLopHoc = $('#txtMaLopHoc').val();
            var loaiGiangDayId = $('#LoaiGiangDayId').val();
            var loaiChuongTrinhHocId = $('#LoaiChuongTrinhHocId').val();
            var chuongTrinhHocId = $('#ChuongTrinhHocId').val();
            var capDoId = $('#CapDoId').val();
            var tenLop = $('#txtTenLopHoc').val();
            var hocVienToiThieu = $('#txtHocVienToiThieu').val();
            var hocVienToiDa = $('#txtHocVienToiDa').val();
            var thoiLuong = $('#txtThoiLuong').val();
            var thoiGianBatDau = $('#txtThoiGianBatDauKhoaHoc').val();
            var hocPhiTietHoc = $('#txtHocPhiTietHoc').val();
            var hocPhiKhoaHoc = $('#txtTongKhoaHoc').val();
            var giamGia = $('#txtGiamGia').val();
            var trangThai = $('#TrangThaiKhoaHoc').val();
            var nguoiPhuTrach = $('#NhanVienId').val();
            var giaoTrinh = $('#GiaoTrinh').val();
            var thoiGianKetThuc = $('#txtThoiGianKetThuc').val();
            var flag = $('#txtFlag').val();
            var lopHocTruoc = $('#txtLopHocTruoc').val();
            var nhanVienId = $('#txtNhanVienTaoId').val();

           


            if (loaiGiangDayId == 0 || tenLop == null || hocVienToiThieu == null || hocVienToiDa == null
                || thoiLuong == null || thoiGianBatDau == null || nguoiPhuTrach == 0) {
                common.notify('Nhập thông tin đầy đủ', 'error');
            }
            else {
                EditKhoaHoc(id, maLopHoc, loaiGiangDayId, loaiChuongTrinhHocId, chuongTrinhHocId, capDoId,
                    tenLop, hocVienToiThieu, hocVienToiDa, thoiLuong, thoiGianBatDau, hocPhiTietHoc, hocPhiKhoaHoc,
                    giamGia, trangThai, nguoiPhuTrach, giaoTrinh, thoiGianKetThuc, flag, lopHocTruoc,nhanVienId);
            }
        });
    }
    EditKhoaHoc = function (id, maLopHoc, loaiGiangDayId, loaiChuongTrinhHocId, chuongTrinhHocId, capDoId,
        tenLop, hocVienToiThieu, hocVienToiDa, thoiLuong, thoiGianBatDau, hocPhiTietHoc, hocPhiKhoaHoc,
        giamGia, trangThai, nguoiPhuTrach, giaoTrinh, thoiGianKetThuc, flag, lopHocTruoc, nhanVienId) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhoaHoc/Edit",
            data: {
                Id: id,
                TenLop: tenLop,
                GiaoTrinh: giaoTrinh,
                ThoiLuong: thoiLuong,
                HocPhiKhoaHoc: hocPhiKhoaHoc,
                HocPhiTietHoc: hocPhiTietHoc,
                GiamGia: giamGia,
                ThoiGianBatDau: thoiGianBatDau,
                ThoiGianKetThuc: thoiGianKetThuc,
                SoLuongHocVien: hocVienToiDa,
                TrangThai: trangThai,
                Flag: flag,
                SoLuongHocVienToiThieu: hocVienToiThieu,
                MaLop: maLopHoc,
                NhanVienId: nhanVienId,
                LoaiGiangDayId: loaiGiangDayId,
                LoaiChuongTrinhHocId: loaiChuongTrinhHocId,
                ChuongTrinhHocId: chuongTrinhHocId,
                CapDoId: capDoId,
                LopHocTruocId: lopHocTruoc,
                NguoiPhuTrach: nguoiPhuTrach

            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
                var data = res;
                common.notify('Sửa thành công', 'success');
                window.location.href = "/Admin/KhoaHoc/DetailLopHoc/" + data.maLop;
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        });
    }



    //

    //Thay đổi dropdown loại giảng dạy thì dropdown loại chương trình học thay đổi theo
    function ChangeLoaiGiangDayEvents() {
        $('#LoaiGiangDayId')
            .change(function () {
                var loaiGiangDaycId = $('#LoaiGiangDayId').val();
                GetLoaiChuongTrinhHocByLoaiGiangDayId(loaiGiangDaycId);
            })
            .change();
    }

    function GetLoaiChuongTrinhHocByLoaiGiangDayId(loaiGiangDaycId) {
        $.ajax({
            type: "GET",
            data: {
                Id: loaiGiangDaycId
            },
            url: "/Admin/LoaiChuongTrinhHoc/GetByLoaiGiangDayId",
            success: function (response) {
                var data = response;
                $("#LoaiChuongTrinhHocId > option").remove();

                var mySelect = $('#LoaiChuongTrinhHocId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn loại chương trình học--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenLoaiChuongTrinhHoc)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    //----------------------------------------------------------------------------
    //Thay đổi dropdown loại chương trình học thì chương trình học thay đổi theo
    function ChangeLoaiChuongtrinhHocEvents() {
        $('#LoaiChuongTrinhHocId')
            .change(function () {
                var loaiChuongTrinhHocId = $('#LoaiChuongTrinhHocId').val();
                GetChuongTrinhHocByLoaiChuongTrinhHocId(loaiChuongTrinhHocId);
            })
            .change();
    }
    //Thay đổi giá trị trong dropdown chương trình học
    function GetChuongTrinhHocByLoaiChuongTrinhHocId(loaiChuongTrinhHocId) {
        $.ajax({
            type: "GET",
            data: {
                Id: loaiChuongTrinhHocId
            },
            url: "/Admin/ChuongTrinhHoc/GetByLoaiChuongTrinhHocId",
            success: function (response) {
                var data = response;
                $("#ChuongTrinhHocId > option").remove();

                var mySelect = $('#ChuongTrinhHocId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn chương trình học--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenChuongTrinh)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    //Khi dropdown Chương trình học thì dropdown cấp độ thay đổi theo
    function ChangeChuongtrinhHocEvents() {
        $('#ChuongTrinhHocId')
            .change(function () {
                var chuongTrinhHocId = $('#ChuongTrinhHocId').val();
                GetCapDoByChuongTrinhHocId(chuongTrinhHocId);
            })
            .change();
    }
    //Thay đổi giá trị trong dropdown chương trình học
    function GetCapDoByChuongTrinhHocId(chuongTrinhHocId) {
        $.ajax({
            type: "GET",
            data: {
                Id: chuongTrinhHocId
            },
            url: "/Admin/CapDo/GetByChuongTrinhHocId",
            success: function (response) {
                var data = response;
                $("#CapDoId > option").remove();
                var mySelect = $('#CapDoId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn cấp độ--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenCapDo)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
   
}