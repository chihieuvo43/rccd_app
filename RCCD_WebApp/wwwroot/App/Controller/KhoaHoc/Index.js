﻿var khoaHocController = function () {
    this.initialize = function () {
        
        ListLoaiGiangDay();
        ListLoaiChuongTrinhHoc();
        ListChuongTrinhHoc();
        ListCapDo();
        ListPhong();
        ListGiangVien();
        //LoadTableKhoaHoc();
        loadData(true);
        registerEvents();
        DelEvents();
        AddKhoaHocEvents();
        CallModalAddKhoaHoc();
        AddThoiGianHocEvents();
        EditThoiGianHocEvents();
        //DetailKhoaHocEvent();
        SaveThoiGiangHocEvent();
        CallEditThoiGianHoc();
        EditThoiGianHocEvents();
        SaveEditThoiGianHocEvent();
        EditKhoaHocEvent();
        SaveChangeEditKhoaHocEvent();
        ShowThoiGianHocEvent();
        FlagEvent();
        ChangeLoaiGiangDayEvents();
        ChangeSelectHinhThucKhoaHoc();
        ChangeLoaiChuongtrinhHocEvents();
        ChangeChuongtrinhHocEvents();
        GetDetailKhoaHocEvent();
    }

   
    //--Xem chi tiết khóa học------------------------------------
    var GetDetailKhoaHocEvent = function () {
        $('body').on('click', '#btnGetDetailKhoaHoc', function (e) {
            e.preventDefault();
            var maLop = $(this).data('id');
            window.location.href = "/Admin/KhoaHoc/DetailLopHoc/" + maLop;
        });
    }
    //-----------------------------------------------------------
    //Thay đổi dropdown loại giảng dạy thì dropdown loại chương trình học thay đổi theo
    function ChangeLoaiGiangDayEvents() {
        $('#LoaiGiangDayId')
            .change(function () {
                var loaiGiangDaycId = $('#LoaiGiangDayId').val();
                GetLoaiChuongTrinhHocByLoaiGiangDayId(loaiGiangDaycId);
            }) 
            .change();
    }
    
    function GetLoaiChuongTrinhHocByLoaiGiangDayId(loaiGiangDaycId) {
        $.ajax({
            type: "GET",
            data: {
                Id: loaiGiangDaycId
            },
            url: "/Admin/LoaiChuongTrinhHoc/GetByLoaiGiangDayId",
            success: function (response) {
                var data = response;
                $("#LoaiChuongTrinhHocId > option").remove();

                var mySelect = $('#LoaiChuongTrinhHocId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn loại chương trình học--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenLoaiChuongTrinhHoc)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    //----------------------------------------------------------------------------
    //Thay đổi dropdown loại chương trình học thì chương trình học thay đổi theo
    function ChangeLoaiChuongtrinhHocEvents() {
        $('#LoaiChuongTrinhHocId')
            .change(function () {
                var loaiChuongTrinhHocId = $('#LoaiChuongTrinhHocId').val();
                GetChuongTrinhHocByLoaiChuongTrinhHocId(loaiChuongTrinhHocId);
            })
            .change();
    }
    //Thay đổi giá trị trong dropdown chương trình học
    function GetChuongTrinhHocByLoaiChuongTrinhHocId(loaiChuongTrinhHocId) {
        $.ajax({
            type: "GET",
            data: {
                Id: loaiChuongTrinhHocId
            },
            url: "/Admin/ChuongTrinhHoc/GetByLoaiChuongTrinhHocId",
            success: function (response) {
                var data = response;
                $("#ChuongTrinhHocId > option").remove();

                var mySelect = $('#ChuongTrinhHocId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn chương trình học--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenChuongTrinh)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    //Khi dropdown Chương trình học thì dropdown cấp độ thay đổi theo
    function ChangeChuongtrinhHocEvents() {
        $('#ChuongTrinhHocId')
            .change(function () {
                var chuongTrinhHocId = $('#ChuongTrinhHocId').val();
                GetCapDoByChuongTrinhHocId(chuongTrinhHocId);
            })
            .change();
    }
    //Thay đổi giá trị trong dropdown chương trình học
    function GetCapDoByChuongTrinhHocId(chuongTrinhHocId) {
        $.ajax({
            type: "GET",
            data: {
                Id: chuongTrinhHocId
            },
            url: "/Admin/CapDo/GetByChuongTrinhHocId",
            success: function (response) {
                var data = response;
                $("#CapDoId > option").remove();
                var mySelect = $('#CapDoId');
                mySelect.append(
                    $('<option></option>').val(0).html('--Chọn cấp độ--')
                );
                $.each(data, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.id).html(text.tenCapDo)
                    );
                });
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function registerEvents() {
        //todo: binding events to controls
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            var trangThai = $('#HinhThucKhoaHoc').val();
            loadData(true, trangThai);
        });
    }
    //load khóa học theo trạng thái
    //Trạng thái
    //--1: kế hoạch
    //--2: đang thực hiện
    //--3: hoãn
    //--4: kế thúc
    //--5: đang tuyển sinh
    //--6:tất cả
    //Khi chọn trạng thái khóa học thì hiển thị danh sách khóa học theo trạng thái
    var ChangeSelectHinhThucKhoaHoc = function () {
        $('#HinhThucKhoaHoc')
            .change(function () {
                var trangThai = $('#HinhThucKhoaHoc').val();
                if (trangThai == 1) {
                    $('h2').text('Các khóa học kế hoạch');
                }
                else if (trangThai == 2) {
                    $('h2').text('Các khóa học đang thực hiện');
                }
                else if (trangThai == 3) {
                    $('h2').text('Các khóa học đang tạm hoãn');
                }
                else if (trangThai == 4) {
                    $('h2').text('Các khóa học đã kết thúc');
                }
                else if (trangThai == 5) {
                    $('h2').text('Các khóa học đang tuyển sinh');
                }
                else {
                    $('h2').text('Tất cả khóa học');
                }
                loadData(true, trangThai);
            })
            .change();
    }
    //Flag cho khóa học
    //Nếu  Flag == 1 thì show ở trang chủ
    var FlagEvent = function () {
        $('body').on('click', '#btnFlag', function (e) {
            e.preventDefault();
            var id = $(this).closest('tr').children('td.KhoaHocId').text();
            var flag = $(this).closest('tr').children('td.FlagId').text();
            Flag(id, flag);
        });
    }
    Flag = function (id, flag) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhoaHoc/Flag",
            data: {
                Id: id,
                Flag: flag,
                NhanVienId: 0

            },
            success: function () {
                ChangeSelectHinhThucKhoaHoc();
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        });
    }

    //------------------------------------
    //Hiển thị thời gian học ở Index
    var ShowThoiGianHocEvent = function () {
        $('body').on('click', '#btnShowThoiGianHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenLop = $(this).data('content');
            ShowThoiGianHoc(id, tenLop);
        });
    }
    ShowThoiGianHoc = function (id, tenLop) {
        $.ajax({
            type: "GET",
            data: { Id: id },
            url: "/Admin/KhoaHoc/GetThoiGianHocValue",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-ShowThoiGianHoc').html();
                var render = "";

                $.each(data, function (i, item)
                {
                    var NgayTrongTuan = '';
                    if (item.ngayTrongTuan == 1)
                    {
                        NgayTrongTuan = 'Thứ 2';
                    }
                    else if (item.ngayTrongTuan == 2)
                    {
                        NgayTrongTuan = 'Thứ 3';
                    }
                    else if (item.ngayTrongTuan == 3)
                    {
                        NgayTrongTuan = 'Thứ 4';
                    }
                    else if (item.ngayTrongTuan == 4)
                    {
                        NgayTrongTuan = 'Thứ 5';
                    }
                    else if (item.ngayTrongTuan == 5)
                    {
                        NgayTrongTuan = 'Thứ 6';
                    }
                    else if (item.ngayTrongTuan == 6)
                    {
                        NgayTrongTuan = 'Thứ 7';
                    }
                    else
                    {
                        NgayTrongTuan = 'Chủ nhật';
                    };
                    ////Phòng
                    var TenPhong = '';
                    $.each(listPhong, function (j, itemj) {
                        if (item.phongHocId == itemj.id) {
                            TenPhong = itemj.tenPhong;
                            return false;
                        }
                        else {
                            TenPhong = 'Đang cập nhật';
                        }
                    })
                    ////Giảng viên
                    var TenGiangVien = '';
                    $.each(listGiangVien, function (j, itemj) {
                        if (item.giangVienId == itemj.id) {
                            TenGiangVien = itemj.hoVaTen;
                            return false;
                        }
                        else {
                            TenGiangVien = 'Đang cập nhật';
                        }
                    })
                  
                    render += Mustache.render(template, {
                        Ngay: NgayTrongTuan,
                        GioBatDau: item.thoiGianBatDau,
                        GioKetThuc: item.thoiGianKetThuc,
                        Phong: TenPhong,
                        GiangVien: TenGiangVien
                    });
                });

                if (render != '') {
                    $('#tbl-content-ShowThoiGianHoc').html(render);
                }
                var TenLop = tenLop;
                $('h5').text('Thời gian học ' + TenLop);
                $('#ShowThoiGianHocModal').modal('show');
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //-----------------------------------------------
    //Sửa khóa học ở danh sách và trong trang GetById
    //Nhấn nút sửa khóa học
    var EditKhoaHocEvent = function () {
        $('body').on('click', '#btnEditKhoaHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditKhoaHoc(id);
        });
    }
    EditKhoaHoc = function (id) {
         $.ajax({
            type: "POST",
             url: "/Admin/KhoaHoc/GetByIdToValue",
            data: {
                id: id
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
                var data = res;
                $('#txtEditId').val(data.id);
                $('#txtEditTenLopHoc').val(data.tenLop);
                $('#txtEditTenKhoaHoc').val(data.tenKhoa);
                $('#txtEditThoiLuong').val(data.thoiLuong);
                $('#txtEditHocPhiTietHoc').val(data.hocPhiTietHoc);
                $('#txtEditTongKhoaHoc').val(data.hocPhiTietHoc);
                $('#txtEditHocPhiTietHoc').val(data.hocPhiKhoaHoc);
                $('#txtEditGiamGia').val(data.giamGia);
                $('#EditTrangThaiKhoaHoc').val(data.trangThai);
                $('#EditThoiGianBatDau').val(data.thoiGianBatDau);
                $('#EditThoiGianKetThuc').val(data.thoiGianKetThuc);
                $('#EditGiaoTrinh').val(data.giaoTrinh);
                $('#txtNhanVienId').val(data.nhanVienId);
                $('#txtKhoaHocId').val(data.khoaHocId);
                $('#EditKhocHocModal').modal('show');
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        });
    }
    //Nhấn nút btnEditSaveChange lưu khóa học đã sửa
    var SaveChangeEditKhoaHocEvent = function () {
        $('body').on('click', '#btnEditSaveChange', function (e) {
            e.preventDefault();
            var tenLop = $('#txtEditTenLopHoc').val();
            var id = $('#txtEditId').val();
            var tenKhoa = $('#txtEditTenKhoaHoc').val();
            var thoiLuong = $('#txtEditThoiLuong').val();
            var hocPhiTietHoc = $('#txtEditTongKhoaHoc').val();
            var hocPhiKhoaHoc = $('#txtEditHocPhiTietHoc').val();
            var giamGia = $('#txtEditGiamGia').val();
            var trangThai = $('#EditTrangThaiKhoaHoc').val();
            var thoiGianBatDau = $('#EditThoiGianBatDau').val();
            var thoiGianKetThuc = $('#EditThoiGianKetThuc').val();
            var giaoTrinh = $('#EditGiaoTrinh').val();
            var nhanVienId = $('#txtNhanVienId').val();
            var khoaHocId = $('#txtKhoaHocId').val();
            SaveChangeEditKhoaHoc(id, tenLop, tenKhoa, thoiLuong, hocPhiTietHoc, hocPhiKhoaHoc, giamGia,
                trangThai, thoiGianBatDau, thoiGianKetThuc, giaoTrinh, nhanVienId, khoaHocId);
        });
    }
    SaveChangeEditKhoaHoc = function (id, tenLop, tenKhoa, thoiLuong, hocPhiTietHoc, hocPhiKhoaHoc, giamGia,
        trangThai, thoiGianBatDau, thoiGianKetThuc, giaoTrinh, nhanVienId, khoaHocId) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhoaHoc/Edit",
            data: {
                TenLop : tenLop,
                Id : id,
                TenKhoa : tenKhoa,
                ThoiLuong : thoiLuong,
                HocPhiTietHoc : hocPhiTietHoc,
                HocPhiKhoaHoc : hocPhiKhoaHoc,
                GiamGia : giamGia,
                TrangThai : trangThai ,
                ThoiGianBatDau : thoiGianBatDau,
                ThoiGianKetThuc : thoiGianKetThuc,
                GiaoTrinh: giaoTrinh,
                NhanVienId: nhanVienId,
                KhoaHocId: khoaHocId
            },
            success: function () {
                if (window.location.href == "https://localhost:44322/Admin/KhoaHoc/GetById/" + id) {
                    location.reload(true);
                    $('#EditKhocHocModal').modal('hide');
                }
                else {
                    LoadTableKhoaHoc();
                    $('#EditKhocHocModal').modal('hide');
                }
               
                common.notify('Sửa thành công', 'success');
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        });
    }
    //-----------------------------------------------
    //SaveEditThoiGianHoc lưu thời gian học sau khi sửa
    //nhấn nút lưu
    //Tìm tất cả chi tiết giảng dạy theo KhoaHocID trong csdl xóa tất
    //Sau đó lưu chi tiết giảng dạy mới từ bảng EdittblThoiGianHoc
    var SaveEditThoiGianHocEvent = function () {
        $('body').on('click', '#SaveEditThoiGianHoc', function (e) {
            e.preventDefault();
            //Tìm KhoaHocId trong bảng Chi tiết giảng dạy xóa tất
            var khoaHocId = $('#txtKhoaHocIdInGetById').val();
            //FindChiTietKhoaHocId(khoaHocId);
            //lưu chi tiết giảng dạy mới từ bảng EdittblThoiGianHoc
            SaveEditTableThoiGiangHocEvent();
        });
    }
   //lưu chi tiết giảng dạy từ bảng EdittblThoiGianHoc nếu ID chi tiết giảng dạy tồn tại thì bỏ qua
   //nếu không có Id khóa học thì thêm mới
    var SaveEditTableThoiGiangHocEvent = function () {
        var khoaHocId = $('#txtKhoaHocIdInGetById').val();
        $('#EdittblThoiGianHoc > tbody  > tr').each(function () {
            var Id = $('.IdThoiGianHoc', this).text();
            var ngay = $('.EditNgay', this).text();
            var gioBatDau = $('.EditGioBatDau', this).text();
            var gioKetThuc = $('.EditGioKetThuc', this).text();
            var phongId = $('.EditPhongId', this).text();
            var giangVienId = $('.EditGiangVienId', this).text();
            if (Id == "") {
                AddEditTableThoiGianHoc(ngay, gioBatDau, gioKetThuc, phongId, giangVienId, khoaHocId);
            }
            
        });
    }
    AddEditTableThoiGianHoc = function (ngay, gioBatDau, gioKetThuc, phongId, giangVienId, khoaHocId) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietGiangDay/Add",
            data: {
                NgayTrongTuan: ngay,
                ThoiGianBatDau: gioBatDau,
                ThoiGianKetThuc: gioKetThuc,
                PhongHocId: phongId,
                GiangVienId: giangVienId,
                KhoaHocId: khoaHocId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function () {
                var id = $('#txtIdInGetById').val();
                window.location.href = '/Admin/KhoaHoc/GetById/' + id;
            },
            error: function () {
                common.notify('Lỗi khi xóa khóa học', 'error');
            },
        });
    }

    //Load danh sách giảng viên
    var listGiangVien;
    function ListGiangVien() {
        $.ajax({
            type: "GET",
            url: "/Admin/GiangVien/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listGiangVien = data;
                common.stopLoading();
            }
        });
    }
    //Load danh sách phòng
    var listPhong;
    function ListPhong() {
        $.ajax({
            type: "GET",
            url: "/Admin/Phong/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listPhong = data;
                common.stopLoading();
            }
        });
    }
    //Thêm thời gian học tại modal sửa thời gian học
    var EditThoiGianHocEvents = function () {
        $('body').on('click', '#btnAddThoiGianHocInModal', function (e) {
            e.preventDefault();
            var Thu = '';
            var Ngay = $('#Ngay').val();
            if (Ngay == 1) {
                Thu = 'Thứ 2';
            }
            else if (Ngay == 2) {
                Thu = 'Thứ 3';
            }
            else if (Ngay == 3) {
                Thu = 'Thứ 4';
            }
            else if (Ngay == 4) {
                Thu = 'Thứ 5';
            }
            else if (Ngay == 5) {
                Thu = 'Thứ 6';
            }
            else if (Ngay == 6) {
                Thu = 'Thứ 7';
            }
            else {
                Thu = 'Chủ nhật';
            }

            var GioBatDau = $('#txtGioBatDau').val();
            var GioKetThuc = $('#txtGioKetThuc').val();
            var PhongId = $('#PhongId').val();
            var TenPhong = '';
            $.each(listPhong, function (i, item) {
                if (item.id == PhongId) {
                    TenPhong = item.tenPhong;
                    return false;
                }
                else {
                    TenPhong = 'Đang cập nhật';
                }
            });
            var GiangVienId = $('#GiangVienId').val();
            var TenGiangVien = '';
            $.each(listGiangVien, function (i, item) {
                if (item.id == GiangVienId) {
                    TenGiangVien = item.hoVaTen;
                    return false;
                }
                else {
                    TenGiangVien = 'Đang cập nhật';
                }
            });
            var markup =
                "<tr><td>" + Thu + "</td> <td class='EditNgay' hidden>" + Ngay + "</td> <td class='EditGioBatDau'>" + GioBatDau + "</td><td class='EditGioKetThuc'>" + GioKetThuc + "</td><td>" + TenPhong + "</td><td class='EditPhongId' hidden>" + PhongId + "</td><td >" + TenGiangVien + "</td><td class='EditGiangVienId' hidden>" + GiangVienId + "</td><td><a class='EditbtnDelTime'><i class='fa fa-times-circle-o'></i></a></td></tr> ";
            $("#EdittblThoiGianHoc").append(markup);
        });
        $('body').on('click', '.EditbtnDelTime', function (e) {
            $(this).closest('tr').remove();
        });
    }
    //--Gọi modal sửa thời gian học
    var CallEditThoiGianHoc = function () {
        $('body').on('click', '#btnEditThoiGianHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditThoiGianHoc(id);
        });
    }
    EditThoiGianHoc = function (id) {
        $.ajax({
            type: "GET",
            data: { Id: id },
            url: "/Admin/KhoaHoc/GetThoiGianHocValue",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-EditThoiGianHoc').html();
                var render = "";
                var html = "<a class='btnDelTime'><i class='fa fa - times - circle - o'></i></a>";
               
                $.each(data, function (i, item) {
                    var NgayTrongTuan = '';
                    if (item.ngayTrongTuan == 1) {
                        NgayTrongTuan = 'Thứ 2';
                    }
                    else if (item.ngayTrongTuan == 2) {
                        NgayTrongTuan = 'Thứ 3';
                    }
                    else if (item.ngayTrongTuan == 3) {
                        NgayTrongTuan = 'Thứ 4';
                    }
                    else if (item.ngayTrongTuan == 4) {
                        NgayTrongTuan = 'Thứ 5';
                    }
                    else if (item.ngayTrongTuan == 5) {
                        NgayTrongTuan = 'Thứ 6';
                    }
                    else if (item.ngayTrongTuan == 6) {
                        NgayTrongTuan = 'Thứ 7';
                    }
                    else {
                        NgayTrongTuan = 'Chủ nhật';
                    }
                    ////Phòng
                    var TenPhong = '';
                    $.each(listPhong, function (j, itemj) {
                        if (item.phongHocId == itemj.id) {
                            TenPhong = itemj.tenPhong;
                            return false;
                        }
                        else {
                            TenPhong = 'Đang cập nhật';
                        }
                    })
                    ////Giảng viên
                    var TenGiangVien = '';
                    $.each(listGiangVien, function (j, itemj) {
                        if (item.giangVienId == itemj.id) {
                            TenGiangVien = itemj.hoVaTen;
                            return false;
                        }
                        else {
                            TenGiangVien = 'Đang cập nhật';
                        }
                    })
                    render += Mustache.render(template, {
                        Id: item.id,
                        Ngay: NgayTrongTuan,
                        GioBatDau: item.thoiGianBatDau,
                        GioKetThuc: item.thoiGianKetThuc,
                        Phong: TenPhong,
                        GiangVien: TenGiangVien,
                        HanhDong: html
                    });
                });
               
                if (render != '') {
                    $('#tbl-content-EditThoiGianHoc').html(render);
                    
                }
                $('#EditThoiGianHocModal').modal('show');
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var CallModalAddKhoaHoc = function () {
        $('body').on('click', '#btnCallFormAddKhoaHoc', function (e) {
            e.preventDefault();
            window.location.href = '/Admin/KhoaHoc/GetAdd';
        });
    }
   
    var SaveThoiGiangHocEvent = function () {
        $('#tblThoiGianHoc > tbody  > tr').each(function () {
            var ngay = $('.Ngay', this).text();
            var gioBatDau = $('.GioBatDau', this).text();
            var gioKetThuc = $('.GioKetThuc', this).text();
            var phongId = $('.PhongId', this).text();
            var giangVienId = $('.GiangVienId', this).text();
            AddThoiGianHoc(ngay, gioBatDau, gioKetThuc, phongId, giangVienId, khoaHocId);
        });
    }
    AddThoiGianHoc = function (ngay, gioBatDau, gioKetThuc, phongId, giangVienId,khoaHocId) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietGiangDay/Add",
            data: {
                NgayTrongTuan: ngay,
                ThoiGianBatDau: gioBatDau,
                ThoiGianKetThuc: gioKetThuc,
                PhongHocId: phongId,
                GiangVienId: giangVienId,
                KhoaHocId: khoaHocId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function () {
                
                
            },
            error: function () {
                common.notify('Lỗi khi xóa khóa học', 'error');
            },
        });
    }

    //----------Thêm lớp học (khóa học sửa thành lớp học) ------------------------------------------------------
    var AddKhoaHocEvents = function () {
        $('#frmAddKhoaHoc').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                txtTenLopHoc: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddKhoaHoc', function (e) {
            e.preventDefault();
            var loaiGiangDayId = $('#LoaiGiangDayId').val();
            var loaiChuongTrinhHocId = $('#LoaiChuongTrinhHocId').val();
            var chuongTrinhHocId = $('#ChuongTrinhHocId').val();
            var capDoId = $('#CapDoId').val();
            var tenLop = $('#txtTenLopHoc').val();
            var hocVienToiThieu = $('#txtHocVienToiThieu').val();
            var hocVienToiDa = $('#txtHocVienToiDa').val();
            var thoiLuong = $('#txtThoiLuong').val();
            var thoiGianBatDau = $('#txtThoiGianBatDauKhoaHoc').val();
            var thoiGianKetThuc = $('#txtThoiGianKetThucKhoaHoc').val();
            var hocPhiTietHoc = $('#txtHocPhiTietHoc').val();
            var hocPhiKhoaHoc = $('#txtTongKhoaHoc').val();
            var giamGia = $('#txtGiamGia').val();
            var trangThai = $('#TrangThaiKhoaHoc').val();
            var nguoiPhuTrach = $('#NhanVienId').val();
            var giaoTrinh = $('#GiaoTrinh').val();
            var nhanVienId = $('#txtNhanVienId').val();
           
            AddKhoaHoc(capDoId, tenLop, hocVienToiThieu, hocVienToiDa, thoiLuong, thoiGianBatDau, thoiGianKetThuc, hocPhiTietHoc, hocPhiKhoaHoc,
                giamGia, trangThai, nguoiPhuTrach, giaoTrinh, loaiGiangDayId, loaiChuongTrinhHocId, chuongTrinhHocId, nhanVienId);
           
             
        });
    }
    var AddKhoaHoc = function (capDoId, tenLop, hocVienToiThieu, hocVienToiDa, thoiLuong, thoiGianBatDau, thoiGianKetThuc, hocPhiTietHoc, hocPhiKhoaHoc,
        giamGia, trangThai, nguoiPhuTrach, giaoTrinh, loaiGiangDayId, loaiChuongTrinhHocId, chuongTrinhHocId, nhanVienId) {
        $.ajax({
            type: 'POST',
            data:
            {
                CapDoId: capDoId,
                LoaiGiangDayId: loaiGiangDayId,
                LoaiChuongTrinhHocId: loaiChuongTrinhHocId,
                ChuongTrinhHocId: chuongTrinhHocId,
                TenLop: tenLop,
                SoLuongHocVienToiThieu: hocVienToiThieu,
                SoLuongHocVien: hocVienToiDa,
                ThoiLuong: thoiLuong,
                ThoiGianBatDau: thoiGianBatDau,
                ThoiGianKetThuc: thoiGianKetThuc,
                HocPhiTietHoc: hocPhiTietHoc,
                HocPhiKhoaHoc: hocPhiKhoaHoc,
                GiamGia: giamGia,
                TrangThai: trangThai,
                NguoiPhuTrach: nguoiPhuTrach,
                GiaoTrinh: giaoTrinh,
                NhanVienId: nhanVienId
            },
            url: '/Admin/KhoaHoc/Add',
            success: function (res) {
                var data = res;
                if (data.Response == "True") {
                    common.notify(data.Message, data.Type);
                    window.location.href = "/Admin/KhoaHoc/DetailLopHoc/" + data.MaLop;
                }
                else if (data.Response == "Available")
                {
                    common.confirm(data.Message +' Xem chi tiết ', function () {
                        window.location.href = "/Admin/KhoaHoc/DetailLopHoc/" + data.MaLop;
                    });
                }
                else {
                    common.notify(data.Message, data.Type);
                }
                
            },
            error: function (res) {
                common.notify('Lỗi khi thêm khóa học', 'error');
            },
        })
    }
    //------------------------------------------------------------------------------------------
    var AddThoiGianHocEvents = function () {
        $('body').on('click','#btnAddThoiGianHoc', function (e) {
            e.preventDefault();
            var Thu = '';
            var Ngay = $('#Ngay').val();
            if (Ngay == 1) {
                Thu = 'Thứ 2';
            }
            else if (Ngay == 2) {
                Thu = 'Thứ 3';
            }
            else if (Ngay == 3) {
                Thu = 'Thứ 4';
            }
            else if (Ngay == 4) {
                Thu = 'Thứ 5';
            }
            else if (Ngay == 5) {
                Thu = 'Thứ 6';
            }
            else if (Ngay == 6) {
                Thu = 'Thứ 7';
            }
            else  {
                Thu = 'Chủ nhật';
            }

            var GioBatDau = $('#txtGioBatDau').val();
            var GioKetThuc = $('#txtGioKetThuc').val();
            var PhongId = $('#PhongId').val();
            var TenPhong = '';
            $.each(listPhong, function (i, item) {
                if (item.id == PhongId) {
                    TenPhong = item.tenPhong;
                    return false;
                }
                else {
                    TenPhong = 'Đang cập nhật';
                }
            });
            var GiangVienId = $('#GiangVienId').val();
            var TenGiangVien = '';
            $.each(listGiangVien, function (i, item) {
                if (item.id == GiangVienId) {
                    TenGiangVien = item.hoVaTen;
                    return false;
                }
                else {
                    TenGiangVien = 'Đang cập nhật';
                }
            });
            var markup =
                "<tr><td class='IdThoiGianHoc' hidden>" + 0 + " </td><td>" + Thu + "</td> <td class='Ngay' hidden>" + Ngay + "</td> <td class='GioBatDau'>" + GioBatDau + "</td><td class='GioKetThuc'>" + GioKetThuc + "</td><td>" + TenPhong + "</td><td class='PhongId' hidden>" + PhongId + "</td><td >" + TenGiangVien + "</td><td class='GiangVienId' hidden>" + GiangVienId + "</td><td><a class='btnDelTime'><i class='fa fa-times-circle-o'></i></a></td></tr> ";
            $("#tblThoiGianHoc").append(markup);
        });
        $('body').on('click', '.btnDelTime', function (e) {
            $(this).closest('tr').remove();
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelKhoaHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenLop = $(this).data('content')
            
            Delete(id, tenLop);
        });
    }

   
    //Xóa khóa học thì phải xóa luôn chi tiết khóa học
    function Delete(id, tenLop) {
        common.confirm('Are you sure to delete? ' + tenLop, function () {
            FindKhoaHocById(id);
            DeletekhoaHoc(id);
           
        });
    }
    //Lấy thông tin khóa học id để biết KhoaHocId
    FindKhoaHocById = function (id) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhoaHoc/GetByIdToValue",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var khoaHocId = data.khoaHocId;
                FindChiTietKhoaHocId(khoaHocId);
            },
            error: function () {
                common.notify('Lỗi khi xóa khóa học', 'error');
            },
        });
    }
    //Tìm id chi tiết khóa học theo id khóa học
    FindChiTietKhoaHocId = function (khoaHocId) {
        $.ajax({
            type: "GET",
            data: { Id: khoaHocId },
            url: "/Admin/KhoaHoc/GetThoiGianHocValue",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $.each(data, function (i, item) {
                    var ChiTietKhoaHocId = item.id;
                    DeleteChiTietKhoaHoc(ChiTietKhoaHocId);
                });
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //Xóa chi tiết khóa học
    function DeleteChiTietKhoaHoc(id) {
        $.ajax({
            type: "POST",
            url: "/Admin/ChiTietGiangDay/Delete",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function () {
                //LoadTableKhoaHoc();
            },
            error: function () {
                common.notify('Lỗi khi xóa khóa học', 'error');
            },
        });
    }
    //Xóa Khóa học
    DeletekhoaHoc = function (id) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhoaHoc/Delete",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function () {
                LoadTableKhoaHoc();
                
            },
            error: function () {
                common.notify('Lỗi khi xóa khóa học', 'error');
            },
        });
    }
    //Load danh sách loại giảng dạy
    var listLoaiGiangDay;
    function ListLoaiGiangDay() {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiGiangDay/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listLoaiGiangDay = data;
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi khi tải danh sách loại giảng dạy', 'error');
                common.stopLoading();
            }
        });
    }

    //-----------------------------
    //Load danh sách loại chương trình học
    var listLoaiChuongTrinhHoc;
    function ListLoaiChuongTrinhHoc() {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiChuongTrinhHoc/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listLoaiChuongTrinhHoc = data;
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi khi tải danh sách loại chương trình học', 'error');
                common.stopLoading();
            }
        });
    }

    //-----------------------------
    //Load danh sách chương trình học
    var listChuongTrinhHoc;
    function ListChuongTrinhHoc() {
        $.ajax({
            type: "GET",
            url: "/Admin/ChuongTrinhHoc/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listChuongTrinhHoc = data;
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi khi tải danh sách chương trình học', 'error');
                common.stopLoading();
            }
        });
    }

    //-----------------------------
    //Load danh sách cấp độ
    var listCapDo;
    function ListCapDo() {
        $.ajax({
            type: "GET",
            url: "/Admin/CapDo/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listCapDo = data;
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi khi tải danh sách cấp độ', 'error');
                common.stopLoading();
            }
        });
    }
    //-----------------------------
   
   
    //Trạng thái khóa học
    //--1: kế hoạch
    //--2: đang thực hiện
    //--3: hoãn
    //--4: kế thúc
    //--5: đang tuyển sinh
    function LoadTableKhoaHoc() {
        $.ajax({
            type: "GET",
            url: "/Admin/KhoaHoc/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-KhoaHoc').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var TrangThai = '';
                    if (item.trangThai == 1) {
                        TrangThai = 'Kế hoạch';
                    }
                    else if (item.trangThai == 2) {
                        TrangThai = 'Đang thực hiện';
                    }
                    else if (item.trangThai == 3) {
                        TrangThai = 'Hoãn';
                    }
                    else  {
                        TrangThai = 'Hoàn thành';
                    };
                    var html = '';
                    if (item.flag == 0) {
                        html = '<a href="#" id="btnFlag"  ><i class="fa fa-toggle-off fa-2x"></i></a>';
                    }
                    else {
                        html = '<a href="#" id="btnFlag"  ><i class="fa fa-toggle-on fa-2x"></i></a>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenLop: item.tenLop,
                        Id: item.id,
                        TenKhoa: item.tenKhoa,
                        ThoiLuong: item.thoiLuong,
                        TrangThai: TrangThai,
                        KhoaHocId: item.khoaHocId,
                        Flag: html,
                        FlagId: item.flag
                    });
                });
                if (render != '') {
                    $('#tbl-content-KhoaHoc').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //Load khóa học theo trạng thái
    function LoadTableKhoaHocByTrangThai(trangThai) {
        $.ajax({
            type: "POST",
            url: "/Admin/KhoaHoc/GetByTrangThai",
            data: {
                id:trangThai
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-KhoaHoc').html();
                var render = "";
                var Stt = 1;
                if (response.length > 0) {
                    if (trangThai == 1) {
                        $('h2').text('Danh sách khóa học kế hoạch');
                    }
                    else if (trangThai == 2) {
                        $('h2').text('Danh sách khóa học đang thực hiện');
                    }
                    else if (trangThai == 3) {
                        $('h2').text('Danh sách khóa học tạm hoãn');
                    }
                    else if (trangThai == 4) {
                        $('h2').text('Danh sách khóa học kết thúc');
                    }
                }
                else {
                    common.notify('Trống', 'error');
                    return false;
                }
                $.each(data, function (i, item) {
                    var TrangThai = '';
                    if (item.trangThai == 1) {
                        TrangThai = 'Kế hoạch';
                    }
                    else if (item.trangThai == 2) {
                        TrangThai = 'Đang thực hiện';
                    }
                    else if (item.trangThai == 3) {
                        TrangThai = 'Hoãn';
                    }
                    else {
                        TrangThai = 'Hoàn thành';
                    };
                    var html = '';
                    if (item.flag == 0) {
                        html = '<a href="#" id="btnFlag"  ><i class="fa fa-toggle-off fa-2x"></i></a>';
                    }
                    else {
                        html = '<a href="#" id="btnFlag"  ><i class="fa fa-toggle-on fa-2x"></i></a>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenLop: item.tenLop,
                        Id: item.id,
                        TenKhoa: item.tenKhoa,
                        ThoiLuong: item.thoiLuong,
                        TrangThai: TrangThai,
                        KhoaHocId: item.khoaHocId,
                        Flag: html,
                        FlagId: item.flag
                    });
                });
                if (render != '') {
                    $('#tbl-content-KhoaHoc').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    function loadData(isPageChanged) {
        $.ajax({
            type: "GET",
            data: {
                Page: common.configs.pageIndex,
                PageSize: common.configs.pageSize,
            },
            url: "/Admin/KhoaHoc/GetAllPaging",
            success: function (response) {
                //console.log(response);
                var data = response.results;
                var template = $('#table-template-KhoaHoc').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var TrangThai = '<td style="background-color:dodgerblue;"></td>';
                    if (item.trangThai == 1) {
                        TrangThai = '<td style="background-color:#99ff99;color:#000;">Kế hoạch</td>';
                    }
                    else if (item.trangThai == 2) {
                        TrangThai = '<td style="background-color:#ff0000;color:#000;">Đang thực hiện</td>';
                    }
                    else if (item.trangThai == 3) {
                        TrangThai = '<td style="background-color:#009933;color:#000;">Hoãn</td>';
                    }
                    else {
                        TrangThai = '<td style="background-color:#ffcc66;color:#000;">Hoàn thành</td>';
                    };
                    var html = '';
                    if (item.flag == 0) {
                        html = '<a href="#" id="btnFlag"  ><i class="fa fa-toggle-off fa-2x"></i></a>';
                    }
                    else {
                        html = '<a href="#" id="btnFlag"  ><i class="fa fa-toggle-on fa-2x"></i></a>';
                    }
                    //Loại giảng dạy
                    var loaiGiangDay = "Không";
                    $.each(listLoaiGiangDay, function (j, itemj) {

                        if (item.loaiGiangDayId == itemj.id) {
                            loaiGiangDay = itemj.tenLoaiGiangDay;
                        }
                    });
                    //Loại chương trình học
                    var loaiChuongTrinhHoc = "Không";
                    $.each(listLoaiChuongTrinhHoc, function (j, itemj) {

                        if (item.loaiChuongTrinhHocId == itemj.id) {
                            loaiChuongTrinhHoc = itemj.tenLoaiChuongTrinhHoc;
                        }
                    });
                    //Chương trình học
                    var chuongTrinhHoc = "Không";
                    $.each(listChuongTrinhHoc, function (j, itemj) {

                        if (item.chuongTrinhHocId == itemj.id) {
                            chuongTrinhHoc = itemj.tenChuongTrinhHoc;
                        }
                    });

                    //Cấp độ
                    var capDo = "Không";
                    $.each(listCapDo, function (j, itemj) {

                        if (item.capDoId == itemj.id) {
                            capDo = itemj.capDoId;
                        }
                    });
                    var ngayBatDau = common.dateFormatJson(item.thoiGianBatDau);
                    var ngayKetThuc = common.dateFormatJson(item.thoiGianKetThuc);
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenLop: item.tenLop,
                        Id: item.id,
                        TenKhoa: item.tenKhoa,
                        MaLop: item.maLop,
                        ThoiLuong: item.thoiLuong,
                        NgayBatDau: ngayBatDau,
                        NgayKetThuc: ngayKetThuc,
                        HocPhi: item.hocPhiKhoaHoc,
                        TrangThai: TrangThai,
                        KhoaHocId: item.khoaHocId,
                        Flag: html,
                        FlagId: item.flag,
                        LoaiGiangDay: loaiGiangDay,
                        LoaiChuongTrinhHoc: loaiChuongTrinhHoc,
                        ChuongTrinhHoc: chuongTrinhHoc,
                        CapDo: capDo
                    });
                });
                $('#lblTotalRecords').text(response.rowCount);
                if (render != '') {
                    $('#tbl-content-KhoaHoc').html(render);
                }
                wrapPaging(response.rowCount, function () {
                    loadData();
                }, isPageChanged);
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function wrapPaging(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            first: 'Đầu',
            prev: 'Trước',
            next: 'Tiếp',
            last: 'Cuối',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callBack(), 200);
            }
        });
    }
}