﻿var loaiChuongTrinhHocController = function () {
    this.initialize = function () {
        ListLoaiGiangDay();
       // LoadTableLoaiChuongTrinhHoc();
        EditLoaiChuongTrinhHocEvents();
        AddLoaiChuongTrinhHocEvents();
        DelEvents();
        CallModalAddLoaiChuongTrinhHoc();
    }
    var CallModalAddLoaiChuongTrinhHoc = function () {
        $('body').on('click', '#btnCallModalAddLoaiChuongTrinhHoc', function (e) {
            e.preventDefault();
            $('h5').text("Thêm loại chương trình học");
            $('#txtTenLoaiChuongTrinhHoc').val(''); 
            $('#txtMoTaLoaiChuongTrinhHoc').val('');
            $('#txtLoaiChuongTrinhHocId').val(0);
            $('#LoaiGiangDayId').val('');
            $('#LoaiChuongTrinhHocModal').modal('show');
        });
    }
    var EditLoaiChuongTrinhHocEvents = function () {
        $('body').on('click', '#btnEditLoaiChuongTrinhHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditLoaiChuongTrinhHoc(id);
        });
    }
    var AddLoaiChuongTrinhHocEvents = function () {
        $('#frmAddLoaiChuongTrinhHoc').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenLoaiChuongTrinhHoc: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddLoaiChuongTrinhHoc', function (e) {
            e.preventDefault();
            var tenLoaiChuongTrinhHoc = $('#txtTenLoaiChuongTrinhHoc').val();
            var id = $('#txtLoaiChuongTrinhHocId').val();
            var loaiGiangDayId = $('#LoaiGiangDayId').val();
            var moTa = $('#txtMoTaLoaiChuongTrinhHoc').val();
            AddLoaiChuongTrinhHoc(id, tenLoaiChuongTrinhHoc, loaiGiangDayId, moTa);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelLoaiChuongTrinhHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenLoaiChuongTrinhHoc = $(this).data('content')
            Delete(id, tenLoaiChuongTrinhHoc);
        });
    }
    function EditLoaiChuongTrinhHoc(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiChuongTrinhHoc/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtLoaiChuongTrinhHocId').val(data.id);
                $('#txtTenLoaiChuongTrinhHoc').val(data.tenLoaiChuongTrinhHoc);
                $('.LoaiGiangDayId').val(data.loaiGiangDayId);
                $('#txtMoTaLoaiChuongTrinhHoc').val(data.moTa);
                $('h5').text("Sửa loại chương trình học");
                $('#LoaiChuongTrinhHocModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddLoaiChuongTrinhHoc = function (id, tenLoaiChuongTrinhHoc, loaiGiangDayId, moTa) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenLoaiChuongTrinhHoc: tenLoaiChuongTrinhHoc,
                LoaiGiangDayId: loaiGiangDayId,
                MoTa:moTa
            },
            url: '/Admin/LoaiChuongTrinhHoc/Add',
            success: function () {
                $('#LoaiChuongTrinhHocModal').modal('hide');
                LoadTableLoaiChuongTrinhHoc();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenLoaiChuongTrinhHoc) {
        common.confirm('Are you sure to delete? ' + tenLoaiChuongTrinhHoc, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/LoaiChuongTrinhHoc/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableLoaiChuongTrinhHoc();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    var listLoaiGiangDay;
    function ListLoaiGiangDay() {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiGiangDay/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listLoaiGiangDay = data;
                LoadTableLoaiChuongTrinhHoc();
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    function LoadTableLoaiChuongTrinhHoc() {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiChuongTrinhHoc/GetAll",
            beforeSend: function (xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
                common.startLoading();
            },
            success: function (response) {
               
                var data = response;
                var template = $('#table-template-LoaiChuongTrinhHoc').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenLoaiGiangDay;
                    $.each(listLoaiGiangDay, function (j, itemj) {
                        if (item.loaiGiangDayId == itemj.id) {
                            tenLoaiGiangDay = itemj.tenLoaiGiangDay;
                            return false;
                        }
                        //else {
                        //    tenLoaiGiangDay = "Đang cập nhật";
                        //    return false;
                        //}
                    }),
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenLoaiChuongTrinhHoc: item.tenLoaiChuongTrinhHoc,
                        Id: item.id,
                        TenLoaiGiangDay: tenLoaiGiangDay,
                        MoTa: item.moTa
                    });
                });
                if (render != '') {
                    $('#tbl-content-LoaiChuongTrinhHoc').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}