﻿var nhanVienController = function () {
    this.initialize = function () {
        ShowListChucVu();
        loadData(true);
        registerEvents();
       
        //LoadTableNhanVien();
        EditNhanVienEvents();
        AddNhanVienEvents();
        DelEvents();
        SaveChangeNhanVienEvent();
        CallModalAddNhanVien();
    }
    function registerEvents() {
        //todo: binding events to controls
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
    }
    var CallModalAddNhanVien = function () {
        $('body').on('click', '#btnCallModalAddNhanVien', function (e) {
            e.preventDefault();
            $('h5').text("Thêm nhân viên");
            $('input').val('');
            $('#ChucVuId').val('');
            $('#HocHamHocViId').val('');
            $('#AddNhanVienModal').modal('show');
        });
    }
    var EditNhanVienEvents = function () {
        $('body').on('click', '#btnEditNhanVien', function (e) {
            e.preventDefault();
            $('h5').text("Sửa nhân viên");
            var id = $(this).data('id');
            EditNhanVien(id);
        });
    }
    var AddNhanVienEvents = function () {
        $('#frmAddNhanVien').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenNhanVien: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddNhanVien', function (e) {
            e.preventDefault();
            var id = $('#txtEditNhanVienId').val();
            var tenNhanVien = $('#txtAddTenNhanVien').val();
            var email = $('#txtAddEmailNhanVien').val();
            var soDienThoai = $('#txtAddSDTNhanVien').val();
            var diaChi = $('#txtAddDiaChiNhanVien').val();
            var chucVuId = $('#ChucVuId').val();
            var hocHamHocViId = $('#HocHamHocViId').val();
            AddNhanVien(id,tenNhanVien, email, soDienThoai, diaChi, chucVuId, hocHamHocViId);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelNhanVien', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenNhanVien = $(this).data('content')
            Delete(id, tenNhanVien);
        });
    }

    var SaveChangeNhanVienEvent = function () {
        $('body').on('click', '#btnSaveChangeNhanVien', function (e) {
            e.preventDefault();
            var id = $('#txtEditNhanVienId').val();
            var tenNhanVien = $('#txtEditTenNhanVien').val();
            var email = $('#txtEditEmailNhanVien').val();
            var soDienThoai = $('#txtEditSDTNhanVien').val();
            var diaChi = $('#txtEditDiaChiNhanVien').val();
            var chucVuId = $('#ChucVuId').val();
            var hocHamHocViId = $('#HocHamHocViId').val();
            SaveChangeNhanVien(id, tenNhanVien, email, soDienThoai, diaChi, chucVuId, hocHamHocViId);
        });
    }
    function EditNhanVien(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/NhanVien/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtEditNhanVienId').val(data.id);
                $('#txtAddTenNhanVien').val(data.hoVaTen);
                $('#txtAddEmailNhanVien').val(data.email);
                $('#txtAddSDTNhanVien').val(data.soDienThoai);
                $('#txtAddDiaChiNhanVien').val(data.diaChi);
                $('#ChucVuId').val(data.chucVuId);
                $('#HocHamHocViId').val(data.hocHamHocViId);
                $('#AddNhanVienModal').modal('show');
                common.stopLoading();

            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
   
    var AddNhanVien = function (id,tenNhanVien,email,soDienThoai,diaChi,chucVuId,hocHamHocViId) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id:id,
                HoVaTen: tenNhanVien,
                Email: email,
                SoDienThoai: soDienThoai,
                DiaChi: diaChi,
                ChucVuId: chucVuId,
                HocHamHocViId: hocHamHocViId
            },
            url: '/Admin/NhanVien/Add',
            success: function () {
                $('#AddNhanVienModal').modal('hide');
                LoadTableNhanVien();
               
                common.notify('Thêm thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi khi thêm nhân viên', 'error');
            },
        })
    }
    function Delete(id, tenNhanVien) {
        common.confirm('Are you sure to delete? ' + tenNhanVien, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/NhanVien/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableNhanVien();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa nhân viên', 'error');
                },
            });
        });
    }
    function SaveChangeNhanVien(id, tenNhanVien,email, soDienThoai, diaChi, chucVuId, hocHamHocViId) {
        $.ajax({
            type: "POST",
            url: "/Admin/NhanVien/Update",
            data: {
                Id: id,
                HoVaTen: tenNhanVien,
                Emai: email,
                SoDienThoai: soDienThoai,
                DiaChi: diaChi,
                ChucVuId: chucVuId,
                HocHamHocViId: hocHamHocViId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function () {
                $('#AddNhanVienModal').modal('hide');
                LoadTableNhanVien();
                
                common.notify('Sửa thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi khi sửa nhân viên', 'error');
            },
        });
    }
    var listChucVu;
    var ShowListChucVu= function () {
        $.ajax({
            type: "GET",
            url: "/Admin/ChucVu/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listChucVu = data;
                registerEvents();
            },
            error: function() {
                common.notify('Lỗi khi load chức vụ', 'error');
                common.stopLoading();
            }
        });
    }
    function LoadTableNhanVien() {
        $.ajax({
            type: "GET",
            url: "/Admin/NhanVien/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-NhanVien').html();
                var render = "";
                var Stt = 1;
                
                $.each(data, function (i, item) {
                    var tenChucVu = '';
                    $.each(listChucVu, function (j, itemj) {
                        if (item.chucVuId == itemj.id) {
                            tenChucVu = item.tenChucVu;
                            return false;
                        }
                    })
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenNhanVien: item.hoVaTen,
                        Id: item.id,
                        Email: item.email,
                        SDT: item.soDienThoai,
                        ChucVu: tenChucVu,
                       
                    });
                });
                $('#tbl-content-NhanVien').html(render);
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    function loadData(isPageChanged) {
        $.ajax({
            type: "GET",
            data: {
                Page: common.configs.pageIndex,
                PageSize: common.configs.pageSize
            },
            url: "/Admin/NhanVien/GetAllPaging",
            success: function (response) {
                //console.log(response);
                var data = response.results;
                var template = $('#table-template-NhanVien').html();
                var render = "";
                var Stt = 1;

                var ListChucVu = listChucVu;
                $.each(data, function (i, item) {
                    var tenChucVu = '';
                    $.each(ListChucVu, function (j, itemj) {
                        if (item.chucVuId == itemj.id) {
                            tenChucVu = item.tenChucVu;
                            return false;
                        }
                    })
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenNhanVien: item.hoVaTen,
                        Id: item.id,
                        Email: item.email,
                        SDT: item.soDienThoai,
                        ChucVu: tenChucVu,
                    });
                });
                $('#lblTotalRecords').text(response.rowCount);
                if (render != '') {
                    $('#tbl-content-NhanVien').html(render);
                }
                wrapPaging(response.rowCount, function () {
                    loadData();
                }, isPageChanged);
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function wrapPaging(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            first: 'Đầu',
            prev: 'Trước',
            next: 'Tiếp',
            last: 'Cuối',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callBack(), 200);
            }
        });
    }
}