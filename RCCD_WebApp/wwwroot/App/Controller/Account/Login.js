﻿var loginController = function () {
    this.initialize = function () {
        LoginEvents();
    }
    var LoginEvents = function () {
        $('#btnLogin').on('click', function (e) {
            e.preventDefault();
            var user = $('#txtUserName').val();
            var password = $('#txtPassword').val();
            login(user, password);
        });
    }
    var login = function (user, password) {
        $.ajax({
            type: 'POST',
            data: {
                UserName: user,
                Password: password
            },
            url: "/Account/Login",
            success: function (res) {
                window.location.href = "/admin";
            },
            error: function () {
                common.notify('Đăng nhập không đúng', 'error');
            }
        })
    }
}