﻿var chuongTrinhHocController = function () {
    this.initialize = function () {
        ListLoaiChuongTrinhHoc();
        LoadTableChuongTrinhHoc();
        EditChuongTrinhHocEvents();
        AddChuongTrinhHocEvents();
        DelEvents();
        CallModalAddChuongTrinhHoc();
    }
    var CallModalAddChuongTrinhHoc = function () {
        $('body').on('click', '#btnCallModalAddChuongTrinhHoc', function (e) {
            e.preventDefault();
            $('h5').text("Thêm chương trình học");
            $('#txtTenChuongTrinhHoc').val('');
            $('#txtChuongTrinhHocId').val(0);
            $('#ChuongTrinhHocId').val('');
            $('#ChuongTrinhHocModal').modal('show');
        });
    }
    var EditChuongTrinhHocEvents = function () {
        $('body').on('click', '#btnEditChuongTrinhHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditChuongTrinhHoc(id);
        });
    }
    var AddChuongTrinhHocEvents = function () {
        $('#frmAddChuongTrinhHoc').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenChuongTrinhHoc: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddChuongTrinhHoc', function (e) {
            e.preventDefault();
            var tenChuongTrinhHoc = $('#txtTenChuongTrinhHoc').val();
            var id = $('#txtChuongTrinhHocId').val();
            var loaiChuongTrinhHocId = $('.LoaiChuongTrinhHocId').val();
            AddChuongTrinhHoc(id, tenChuongTrinhHoc, loaiChuongTrinhHocId);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelChuongTrinhHoc', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenChuongTrinhHoc = $(this).data('content')
            Delete(id, tenChuongTrinhHoc);
        });
    }
    function EditChuongTrinhHoc(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChuongTrinhHoc/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtChuongTrinhHocId').val(data.id);
                $('#txtTenChuongTrinhHoc').val(data.tenChuongTrinh);
                $('.LoaiChuongTrinhHocId').val(data.loaiChuongTrinhHocId);
                $('h5').text("Sửa chương trình học");
                $('#ChuongTrinhHocModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddChuongTrinhHoc = function (id, tenChuongTrinhHoc, loaiChuongTrinhHocId) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenChuongTrinh: tenChuongTrinhHoc,
                LoaiChuongTrinhHocId: loaiChuongTrinhHocId
            },
            url: '/Admin/ChuongTrinhHoc/Add',
            success: function () {
                $('#ChuongTrinhHocModal').modal('hide');
                LoadTableChuongTrinhHoc();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenChuongTrinhHoc) {
        common.confirm('Are you sure to delete? ' + tenChuongTrinhHoc, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/ChuongTrinhHoc/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableChuongTrinhHoc();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    var listLoaiChuongTrinhHoc;
    function ListLoaiChuongTrinhHoc() {
        $.ajax({
            type: "GET",
            url: "/Admin/LoaiChuongTrinhHoc/GetAll",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                listLoaiChuongTrinhHoc = data;
                //LoadTableChuongTrinhHoc();
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    function LoadTableChuongTrinhHoc() {
        $.ajax({
            type: "GET",
            url: "/Admin/ChuongTrinhHoc/GetAll",
            beforeSend: function (xhr) {
               
                //xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
                common.startLoading();
            },
            success: function (response) {
               
                var data = response;
                var template = $('#table-template-ChuongTrinhHoc').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var tenLoaiChuongTrinhHoc;
                    $.each(listLoaiChuongTrinhHoc, function (j, itemj) {
                        if (item.loaiChuongTrinhHocId == itemj.id) {
                            tenLoaiChuongTrinhHoc = itemj.tenLoaiChuongTrinhHoc;
                            return false;
                        }
                        //else {
                        //    tenLoaiGiangDay = "Đang cập nhật";
                        //    return false;
                        //}
                    }),
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenChuongTrinhHoc: item.tenChuongTrinh,
                        Id: item.id,
                        TenLoaiChuongTrinhHoc: tenLoaiChuongTrinhHoc
                    });
                });
                if (render != '') {
                    $('#tbl-content-ChuongTrinhHoc').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}