﻿var donViController = function () {
    this.initialize = function () {
        LoadTableDonVi();
        EditDonViEvents();
        AddDonViEvents();
        DelEvents();
        CallModalAddDonVi();
    }
    var CallModalAddDonVi = function () {
        $('body').on('click', '#btnCallModalAddDonVi', function (e) {
            e.preventDefault();
            $('h5').text("Thêm đơn vị");
            $('#txtTenDonVi').val('');
            $('#txtDonViId').val(0);
            $('#DonViModal').modal('show');
        });
    }
    var EditDonViEvents = function () {
        $('body').on('click', '#btnEditDonVi', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditDonVi(id);
        });
    }
    var AddDonViEvents = function () {
        $('#frmAddDonVi').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenDonVi: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddDonVi', function (e) {
            e.preventDefault();
            var tenDonVi = $('#txtTenDonVi').val();
            var id = $('#txtDonViId').val();
            AddDonVi(id,tenDonVi);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelDonVi', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenDonVi = $(this).data('content')
            Delete(id, tenDonVi);
        });
    }
    function EditDonVi(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/DonVi/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtDonViId').val(data.id);
                $('#txtTenDonVi').val(data.tenDonVi);
                $('h5').text("Sửa đơn vị");
                $('#DonViModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddDonVi = function (id,tenDonVi) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenDonVi: tenDonVi,
            },
            url: '/Admin/DonVi/Add',
            success: function () {
                $('#DonViModal').modal('hide');
                LoadTableDonVi();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenDonVi) {
        common.confirm('Are you sure to delete? ' + tenDonVi, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/DonVi/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableDonVi();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa đơn vị', 'error');
                },
            });
        });
    }
    function LoadTableDonVi() {
        $.ajax({
            type: "GET",
            url: "/Admin/DonVi/GetAll",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-DonVi').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenDonVi: item.tenDonVi,
                        Id: item.id
                    });
                });
                if (render != '') {
                    $('#tbl-content-DonVi').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}