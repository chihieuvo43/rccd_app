﻿var homeController = function () {
    this.initialize = function () {
        DangKyKhoaHocEvent();
    }
    //Khách hàng đăng ký khóa học
    //Nhập thông tin sau đó nhấn đăng ký
    //kiểm tra là khách hàng mới hay cũ bằng cách kiểm tra số điện thoại có tồn tại k
    //
    var DangKyKhoaHocEvent = function () {
        $('body').on('click', '#btnDangKy', function (e) {
            e.preventDefault();
            //var id = $(this).data('id');
            var hoVaTen = $('#txtHoVaTen').val();
            var soDienThoai = $('#txtSoDienThoai').val();
            var email = $('#txtEmail').val();
            var khoaHocId = $('#txtKhoaHocId').val();
            CheckNumberPhone(hoVaTen, email, soDienThoai);
        });
    }
    //Kiểm tra số điện thoại có trong csdl hay không
    //nếu có thì tìm thông tin khách hàng bằng số điện thoại
    //nếu không thì lưu dưới dạng khách hàng mới
    CheckNumberPhone = function (hoVaTen, email, soDienThoai) {
        $.ajax({
            type: "POST",
            url: "/Home/CheckNumberPhone",
            data: {
                numberPhone: soDienThoai
            },
            success: function (res) {
                var data = res;
                //Nếu trả về bằng null thì lưu thông tin mới
                if (data == null) {
                    HoVaTen = hoVaTen;
                    Email = email;
                    SoDienThoai = soDienThoai;
                    Id = 0;
                    AddKhachHang(Id, HoVaTen, Email, SoDienThoai);
                }
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        });
    }
    //Lưu thông tin khách hàng, sau đó lưu thông tin đăng ký khóa học
    var AddKhachHang = function (id, tenKhachHang, email, soDienThoai) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                HoVaTen: tenKhachHang,
                Email: email,
                SoDienThoai: soDienThoai
            },
            url: '/Admin/KhachHang/Add',
            success: function () {
                AddChiTietDangKyKhoaHoc();
                //common.notify('Thêm thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi khi thêm nhân viên', 'error');
            },
        })
    }
    //Lưu thông tin đăng ký khóa học
    var AddChiTietDangKyKhoaHoc = function (khoaHocId, khachHangId) {
        $.ajax({
            type: 'POST',
            data:
            {
                KhoaHocId: khoaHocId,
                KhachHangId: khachHangId
            },
            url: '/Admin/ChiTietDangKyKhoaHoc/AddChiTietDangKyKhoaHoc',
            success: function () {
                //common.notify('Thêm thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi khi thêm nhân viên', 'error');
            },
        })
    }
    //Tìm Id khách hàng theo mã khách hàng

    //----------------------------------------------
}