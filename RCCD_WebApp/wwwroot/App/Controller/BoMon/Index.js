﻿var boMonController = function () {
    this.initialize = function () {
        LoadTableBoMon();
        EditBoMonEvents();
        AddBoMonEvents();
        DelEvents();
        CallModalAddBoMon();
    }
    var CallModalAddBoMon = function () {
        $('body').on('click', '#btnCallModalAddBoMon', function (e) {
            e.preventDefault();
            $('h5').text("Thêm bộ môn");
            $('#txtTenBoMon').val('');
            $('#txtBoMonId').val(0);
            $('#BoMonModal').modal('show');
        });
    }
    var EditBoMonEvents = function () {
        $('body').on('click', '#btnEditBoMon', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditBoMon(id);
        });
    }
    var AddBoMonEvents = function () {
        $('#frmAddBoMon').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenBoMon: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddBoMon', function (e) {
            e.preventDefault();
            var tenBoMon = $('#txtTenBoMon').val();
            var id = $('#txtBoMonId').val();
            AddBoMon(id,tenBoMon);
        });
    }
    var DelEvents = function () {
        $('body').on('click', '#btnDelBoMon', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenBoMon = $(this).data('content')
            Delete(id, tenBoMon);
        });
    }
    function EditBoMon(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/BoMon/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtBoMonId').val(data.id);
                $('#txtTenBoMon').val(data.tenBoMon);
                $('h5').text("Sửa bộ môn");
                $('#BoMonModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var AddBoMon = function (id,tenBoMon) {
        $.ajax({
            type: 'POST',
            data:
            {
                Id: id,
                TenBoMon: tenBoMon,
            },
            url: '/Admin/BoMon/Add',
            success: function () {
                $('#BoMonModal').modal('hide');
                LoadTableBoMon();
                common.notify('Thành công', 'success')
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    function Delete(id, tenBoMon) {
        common.confirm('Are you sure to delete? ' + tenBoMon, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/BoMon/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableBoMon();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    function LoadTableBoMon() {
        $.ajax({
            type: "GET",
            url: "/Admin/BoMon/GetAll",
            beforeSend: function (xhr) {
               
                //xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
                common.startLoading();
            },
            success: function (response) {
               
                var data = response;
                var template = $('#table-template-BoMon').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenBoMon: item.tenBoMon,
                        Id: item.id
                    });
                });
                if (render != '') {
                    $('#tbl-content-BoMon').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}