﻿var hopDongController = function () {
    this.initialize = function () {
        CallModalAddHopDong();
        AddHopDongEvent();
        loadTableHopDongEvent();
        DeleteHopDongEvent();
        LoadLichDayByGiangVienIdEvent();
    }
    //--Show lịch dạy của giảng viên----------------------------------------------------
    var LoadLichDayByGiangVienIdEvent = function () {
        $('body').on('click', '#profile-tab2', function (e) {
            e.preventDefault();

            var giangVienId = $("#txtGiangVienId").val();
            LoadLichDayByGiangVienId(giangVienId);
        });
    }

    function LoadLichDayByGiangVienId(giangVienId) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietGiangDay/GetByGiangVienId",
            data: {
                id: giangVienId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-LichDayGiangVien').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var thu;
                    if (item.thu == 1) {
                        thu = '<span class="label label-default">Thứ 2</span>'
                    }
                    if (item.thu == 2) {
                        thu = '<span class="label label-primary">Thứ 3</span>'
                    }
                    if (item.thu == 3) {
                        thu = '<span class="label label-danger">Thứ 4</span>'
                    }
                    if (item.thu == 4) {
                        thu = '<span class="label label-warning">Thứ 5</span>'
                    }
                    if (item.thu == 5) {
                        thu = '<span class="label label-info">Thứ 6</span>'
                    }
                    if (item.thu == 6) {
                        thu = '<span class="label label-success">Thứ 7</span>'
                    }
                    if (item.thu == 7) {
                        thu = '<span class="badge badge-light">Chủ nhật</span>'
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        Id: item.id,
                        Ca: item.tenCa,
                        GiangVien: item.tenGiangVien,
                        TroGiang: item.troGiang,
                        Phong: item.tenPhong,
                        GhiChuThoiKhoaBieu: item.ghiChu,
                        Thu: thu

                    });
                });
                if (render != '') {
                    $('#tbl-content-LichDayGiangVien').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
//--------------------------------------------------
    //--Thêm hợp đồng-----------------------------
    var CallModalAddHopDong = function () {
        $('body').on('click', '#CallModalAddHopDong', function (e) {
            e.preventDefault();
            var tenGiangVien = $("#txtTenGiangVien").val();
            $("#TenGiangVien span").val(tenGiangVien);
            $("#AddHopDongModal").modal('show');
        });
    }
    var AddHopDongEvent = function () {
        $('body').on('click', '#btnAddHopDong', function (e) {
            e.preventDefault();
            var loaiHopDong = $("#txtLoaiHopDong").val();
            var ngayTaoHopDong = $("#txtNgayTaoHopDong").val();
            var ngayBatDauHopDong = $("#txtNgayBatDauHopDong").val();
            var ngayKetThucHopDong = $("#txtNgayKetThucHopDong").val();
            var hinhThucHopDong = $("#HinhThucHopDong").val();
            var ghiChu = $("#areaGhiChu").val();
            var tepHopDong = $("#txtTepHopDong").val();
            var giangVienId = $("#txtGiangVienId").val();
            var nhanVienId = 1003;
            var hopDongId = $("#txtHopDongId").val();;
            AddHopDong(hopDongId, loaiHopDong, ngayTaoHopDong, ngayBatDauHopDong, ngayKetThucHopDong,
                hinhThucHopDong, ghiChu, tepHopDong, giangVienId, nhanVienId)
        });
    }
    function AddHopDong(hopDongId, loaiHopDong, ngayTaoHopDong, ngayBatDauHopDong, ngayKetThucHopDong,
        hinhThucHopDong, ghiChu, tepHopDong, giangVienId, nhanVienId) {
        $.ajax({
            type: "POST",
            url: "/Admin/HopDong/Add",
            data: {
                Id: hopDongId,
                LoaiHopDong: loaiHopDong,
                NgayTaoHopDong: ngayTaoHopDong,
                NgayBatDauHopDong: ngayBatDauHopDong,
                NgayKetThucHopDong: ngayKetThucHopDong,
                HinhThucHopDong: hinhThucHopDong,
                GhiChu: ghiChu,
                TepHopDong: tepHopDong,
                GiangVienId: giangVienId,
                NhanVienId: nhanVienId

            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                //var data = response;
                //var result = data.Response;
                //var message = data.Message;
                //var type = data.Type;
                //if (result == "false") {

                //    common.notify(message, type);
                //}
                //else {
                //    common.notify(message, type);
                //    var giangVienId = $("#txtGiangVienId").val();
                //    loadTableHopDong(giangVienId);
                //    $("#AddHopDongModal").modal('hide');
                //}
                common.notify('Thêm thành công', 'success');
                var giangVienId = $("#txtGiangVienId").val();
                loadTableHopDong(giangVienId);
                $("#AddHopDongModal").modal('hide');
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //--Xóa hợp đồng------------------------------------------------------
    var DeleteHopDongEvent = function () {
        $('body').on('click', '#btnDeleteHopDong', function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                DeleteHopDong(id);
            });
    }
function DeleteHopDong(id) {
    common.confirm('Xác nhận xóa hợp đồng', function () {
        $.ajax({
            type: "POST",
            url: "/Admin/HopDong/Delete",
            data: {
                id: id
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                common.notify('Xóa thành công', 'success');
                var giangVienId = $("#txtGiangVienId").val();
                loadTableHopDong(giangVienId);
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi khi xóa hợp đồng', 'error');
                common.stopLoading();
            }
        });
    });
}
    //---------------------------------------------------------------------
    //--Load table hợp đồng của giảng viên---------------------------------
    var loadTableHopDongEvent = function () {
        $('body').on('click', '#profile-tab', function (e) {
            e.preventDefault();
            var giangVienId = $("#txtGiangVienId").val();
            loadTableHopDong(giangVienId);
        });
    }

    function loadTableHopDong(giangVienId) {
        $.ajax({
            type: "POST",
            url: "/Admin/HopDong/GetByGiangVienId",
            data: {
                id: giangVienId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var template = $('#table-template-HopDongGiangVien').html();
                var render = "";
                var Stt = 1;
               
                $.each(data, function (i, item) {
                    var HinhThucHopDong = 'Full-time';
                    if (item.hinhThucHopDong == 1) {
                        HinhThucHopDong = 'Part-time';
                    }
                    var ngayHienTai = new Date();
                    var xoa = '';
                    if (ngayHienTai <= new Date(item.ngayBatDauHopDong)) {
                        xoa = '<a class="btn btn-xs btn-danger" id="btnDeleteHopDong" data-id=' + item.id + '><i class="fa fa-trash"></i></a>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenHopDong: item.loaiHopDong,
                        Id: item.id,
                        NgayBatDauHopDong: common.dateFormatJson(item.ngayBatDauHopDong),
                        NgayKetThucHopDong: common.dateFormatJson(item.ngayKetThucHopDong),
                        NgayTaoHopDong: common.dateFormatJson(item.ngayTaoHopDong),
                        GiangVien: item.tenGiangVien,
                        NhanVien: item.tenNhanVien,
                        HinhThucHopDong: HinhThucHopDong,
                        GhiChu: item.ghiChu,
                        Xoa:xoa
                    });
                });
                if (render != '') {
                    $('#tbl-content-HopDongGiangVien').html(render);
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}