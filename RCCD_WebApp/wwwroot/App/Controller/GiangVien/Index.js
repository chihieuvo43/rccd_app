﻿var giangVienController = function () {
    this.initialize = function () {
        //ListBoMon();
        loadData(true);
        EditGiangVienEvents();
        AddGiangVienEvents();
        DelEvents();
        SaveChangeGiangVienEvent();
        EditChuyenMonGiangVienEvents();
        SaveChangeChuyenMonGiangVienEvent();
       // CallModalAddGiangVien();
        DetailGiangVienEvent();
        $("#txtNgaySinh").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtNgayHieuLucGPLD").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtNgayHetHanGPLD").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtDenVietNamTuNgay").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtNgayHetHanHoChieu").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtNgayCapVisa").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#txtNgayHetHanVisa").datepicker({ dateFormat: 'yy-mm-dd' });
    }
    function registerEvents() {
        //todo: binding events to controls
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
    }
   //---Sửa chi tiết chuyên môn giảng dạy---------------------------
    var EditChuyenMonGiangVienEvents = function () {
        $('body').on('click', '#btnEditChuyenMonGiangDay', function (e) {
            e.preventDefault();
            $('#ChuyenMonGiangVienModal').modal('show');
        });
    }
    var SaveChangeChuyenMonGiangVienEvent = function () {
        $('body').on('click', '#btnSaveChangeChuyenMonGiangVien', function (e) {
            e.preventDefault();
            var giangVienId = $("#txtGiangVienId").val();
            SaveChangeChuyenMonGiangVien(giangVienId);
        });
    }
    function SaveChangeChuyenMonGiangVien(giangVienId) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietChuyenMonGiangVien/DeleteByGiangVienId",
            data: { id: giangVienId },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var giangVienId = $("#txtGiangVienId").val();

                var checkCount = 0;
                $.each($("input[name='ChuyenMonGiangVienId']:checked"), function () {
                    checkCount++;
                });
                if (checkCount == 0) {
                    common.notify('Chọn chuyên môn giảng dạy của giảng viên', 'error');
                }
                if (checkCount != 0) {
                    $.each($("input[name='ChuyenMonGiangVienId']:checked"), function () {
                        var loaiGiangDayId = $(this).val();
                        AddChiTietChuyenMonGiangDay(giangVienId, loaiGiangDayId);
                    });
                    Location.reload();
                }
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //--------------------------------------------------------------
    //--Sửa giảng viên-----------------------------------------------
    var EditGiangVienEvents = function () {
        $('body').on('click', '#btnEditGiangVien', function (e) {
            e.preventDefault();
            $('h5').text("Sửa giảng viên");
            var id = $("#txtGiangVienId").val();
            EditGiangVien(id);
        });
    }
    function EditGiangVien(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/GiangVien/GetValues",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtMaGiangVienCuoi').val(data.maGiangVienCuoi);
                $('#txtMaGiangVienDau').val(data.maGiangVienDau);
                $('#txtEditGiangVienId').val(data.id);
                $('#txtHoVaTen').val(data.hoVaTen);
                $('#txtEmail').val(data.email);
                $('#txtSoDienThoai').val(data.soDienThoai);
                $('#txtLoaiGiangVien').val(data.loaiGiangVien);
                $('#DonViId').val(data.donViId);
                $('#txtNgaySinh').val(data.ngaySinh);
                $('#txtNoiSinh').val(data.noiSinh);
                $('#txtDiaChi').val(data.diaChi);
                $('#QuocTichId').val(data.quocTichId);
                $('#txtChungChiGiangDay').val(data.chungChiGiangDay);
                $('#txtDaiHoc').val(data.daiHoc);
                $('#txtKinhNghiem').val(data.kinhNghiem);
                $('#txtSoGiayPhepLaoDong').val(data.soGiayPhepLaoDong);
                $('#txtNgayHieuLucGPLD').val(data.ngayHieuLucGPLD);
                $('#txtNgayHetHanGPLD').val(data.ngayHetHanGPLD);
                $('#txtDenVietNamTuNgay').val(data.denVietNamTuNgay);
                $('#txtSoHoChieu').val(data.soHoChieu);
                $('#txtNgayHetHanHoChieu').val(data.ngayHetHanHoChieu);
                $('#txtNoiCapHoChieu').val(data.noiCapHoChieu);
                $('#txtSoVisa').val(data.soVisa);
                $('#txtNgayCapVisa').val(data.ngayCapVisa);
                $('#txtNgayHetHanVisa').val(data.ngayHetHanVisa);
                $('#txtSoNganHang').val(data.soNganHang);
                CKEDITOR.instances['txtEditTieuSu'].setData(data.tieuSu);

                $('#AddGiangVienModal').modal('show');
                common.stopLoading();
            },
            error: function (status) {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    var SaveChangeGiangVienEvent = function () {
        $('body').on('click', '#btnSaveChangeGiangVien', function (e) {
            e.preventDefault();
            var id = $('#txtEditGiangVienId').val();
            var maGiangVienDau = $('#txtMaGiangVienDau').val();
            var maGiangVienCuoi = $('#txtMaGiangVienCuoi').val();
            var tenGiangVien = $('#txtHoVaTen').val();
            var email = $('#txtEmail').val();
            var soDienThoai = $('#txtSoDienThoai').val();
            var loaiGiangVien = $('#txtLoaiGiangVien').val();
            var donViId = $('#DonViId').val();
            var ngaySinh = $('#txtNgaySinh').val();
            var noiSinh = $('#txtNoiSinh').val();
            var diaChi = $('#txtDiaChi').val();
            var quocTichId = $('#QuocTichId').val();
            var chungChiGiangDay = $('#txtChungChiGiangDay').val();
            var daiHoc = $('#txtDaiHoc').val();
            var kinhNghiem = $('#txtKinhNghiem').val();
            var soGiayPhepLaoDong = $('#txtSoGiayPhepLaoDong').val();
            var ngayHieuLucGPLD = $('#txtNgayHieuLucGPLD').val();
            var ngayHetHanGPLD = $('#txtNgayHetHanGPLD').val();
            var denVietNamTuNgay = $('#txtDenVietNamTuNgay').val();
            var soHoChieu = $('#txtSoHoChieu').val();
            var ngayHetHanHoChieu = $('#txtNgayHetHanHoChieu').val();
            var noiCapHoChieu = $('#txtNoiCapHoChieu').val();
            var soVisa = $('#txtSoVisa').val();
            var ngayCapVisa = $('#txtNgayCapVisa').val();
            var ngayHetHanVisa = $('#txtNgayHetHanVisa').val();
            var soNganHang = $('#txtSoNganHang').val();

            var tieuSu = CKEDITOR.instances['txtEditTieuSu'].getData();

            SaveChangeGiangVien(id, maGiangVienDau, maGiangVienCuoi, tenGiangVien, email, soDienThoai, loaiGiangVien, donViId, ngaySinh,
                noiSinh, diaChi, quocTichId, chungChiGiangDay, daiHoc, kinhNghiem, soGiayPhepLaoDong,
                ngayHieuLucGPLD, ngayHetHanGPLD, denVietNamTuNgay, soHoChieu, ngayHetHanHoChieu, noiCapHoChieu,
                soVisa, ngayCapVisa, ngayHetHanVisa, soNganHang, tieuSu);
        });
    }
    function SaveChangeGiangVien(id, maGiangVienDau, maGiangVienCuoi, tenGiangVien, email, soDienThoai, loaiGiangVien, donViId, ngaySinh,
        noiSinh, diaChi, quocTichId, chungChiGiangDay, daiHoc, kinhNghiem, soGiayPhepLaoDong,
        ngayHieuLucGPLD, ngayHetHanGPLD, denVietNamTuNgay, soHoChieu, ngayHetHanHoChieu, noiCapHoChieu,
        soVisa, ngayCapVisa, ngayHetHanVisa, soNganHang, tieuSu) {
        $.ajax({
            type: "POST",
            url: "/Admin/GiangVien/Edit",
            data: {
                Id: id,
                MaGiangVienDau: maGiangVienDau,
                MaGiangVienCuoi: maGiangVienCuoi,
                HoVaTen: tenGiangVien,
                Email: email,
                SoDienThoai: soDienThoai,
                DonViId: donViId,
                DiaChi: diaChi,
                NgaySinh: ngaySinh,
                LoaiGiangVien: loaiGiangVien,
                NoiSinh: noiSinh,
                QuocTichId: quocTichId,
                ChungChiGiangDay: chungChiGiangDay,
                DaiHoc: daiHoc,
                KinhNghiem: kinhNghiem,
                SoGiayPhepLaoDong: soGiayPhepLaoDong,
                NgayHieuLucGPLD: ngayHieuLucGPLD,
                NgayHetHanGPLD: ngayHetHanGPLD,
                NgayDenVietNam: denVietNamTuNgay,
                SoHoChieu: soHoChieu,
                NgayHetHanHoChieu: ngayHetHanHoChieu,
                NoiCapHoChieu: noiCapHoChieu,
                SoVisa: soVisa,
                NgayCapVisa: ngayCapVisa,
                NgayHetHanVisa: ngayHetHanVisa,
                SoNganHang: soNganHang,
                TieuSu: tieuSu
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response.id;
                common.notify('Sửa thành công', 'success');
                location.reload();

            },
            error: function () {
                common.notify('Lỗi khi sửa giảng viên', 'error');
            },
        });
    }
    //--------------------------------------------------------------------------
    //------------Hiển thị chi tiết thông tin-----------------------------------
    var DetailGiangVienEvent = function () {
        $('body').on('click', '#btnDetailGiangVien', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            window.location.href = '/Admin/GiangVien/GetById/'+id;
        });
    }
    //---------------------------------------------------------------------------
    //----------Thêm giảng viên--------------------------------------------------
    var AddGiangVienEvents = function () {
        $('#frmAddGiangVien').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                TenGiangVien: {
                    required: true
                },
            }
        });
        $('body').on('click', '#btnAddGiangVien', function (e) {
            e.preventDefault();
            var tenGiangVien = $('#txtHoVaTen').val();
            var email = $('#txtEmail').val();
            var soDienThoai = $('#txtSoDienThoai').val();
            var loaiGiangVien = $('#txtLoaiGiangVien').val();
            var donViId = $('#DonViId').val();
            var ngaySinh = $('#txtNgaySinh').val();
            var noiSinh = $('#txtNoiSinh').val();
            var diaChi = $('#txtDiaChi').val();
            var quocTichId = $('#QuocTichId').val();
            var chungChiGiangDay = $('#txtChungChiGiangDay').val();
            var daiHoc = $('#txtDaiHoc').val();
            var kinhNghiem = $('#txtKinhNghiem').val();
            var soGiayPhepLaoDong = $('#txtSoGiayPhepLaoDong').val();
            var ngayHieuLucGPLD = $('#txtNgayHieuLucGPLD').val();
            var ngayHetHanGPLD = $('#txtNgayHetHanGPLD').val();
            var denVietNamTuNgay = $('#txtDenVietNamTuNgay').val();
            var soHoChieu = $('#txtSoHoChieu').val();
            var ngayHetHanHoChieu = $('#txtNgayHetHanHoChieu').val();
            var noiCapHoChieu = $('#txtNoiCapHoChieu').val();
            var soVisa = $('#txtSoVisa').val();
            var ngayCapVisa = $('#txtNgayCapVisa').val();
            var ngayHetHanVisa = $('#txtNgayHetHanVisa').val();
            var soNganHang = $('#txtSoNganHang').val();

            var tieuSu = CKEDITOR.instances['txtTieuSu'].getData();

           
            //var tieuSu = $('.txtTieuSu').val();
            //Kiểm tra đã check chuyên môn giảng dạy chưa
            var checkCount = 0;
            $.each($("input[name='ChuyenMonGiangVienId']:checked"), function () {
                checkCount++;
            });
            if (checkCount == 0) {
                common.notify('Chọn chuyên môn giảng dạy của giảng viên', 'error');
            }

            
            if (checkCount!=0) {
                AddGiangVien(tenGiangVien, email, soDienThoai, loaiGiangVien, donViId, ngaySinh,
                    noiSinh, diaChi, quocTichId, chungChiGiangDay, chungChiGiangDay, daiHoc, kinhNghiem, soGiayPhepLaoDong,
                    soGiayPhepLaoDong, ngayHieuLucGPLD, ngayHieuLucGPLD, ngayHetHanGPLD, denVietNamTuNgay, soHoChieu,
                    ngayHetHanHoChieu, noiCapHoChieu, soVisa, ngayCapVisa, ngayHetHanVisa, soNganHang, tieuSu);
            }
        });
    }

    var AddGiangVien = function (tenGiangVien, email, soDienThoai, loaiGiangVien, donViId, ngaySinh,
        noiSinh, diaChi, quocTichId, chungChiGiangDay, chungChiGiangDay, daiHoc, kinhNghiem, soGiayPhepLaoDong,
        soGiayPhepLaoDong, ngayHieuLucGPLD, ngayHieuLucGPLD, ngayHetHanGPLD, denVietNamTuNgay, soHoChieu,
        ngayHetHanHoChieu, noiCapHoChieu, soVisa, ngayCapVisa, ngayHetHanVisa, soNganHang, tieuSu) {
        $.ajax({
            type: 'POST',
            data:
            {
                HoVaTen: tenGiangVien,
                Email: email,
                SoDienThoai: soDienThoai,
                DonViId: donViId,
                DiaChi: diaChi,
                NgaySinh: ngaySinh,
                LoaiGiangVien: loaiGiangVien,
                NoiSinh: noiSinh,
                QuocTichId: quocTichId,
                ChungChiGiangDay: chungChiGiangDay,
                DaiHoc: daiHoc,
                KinhNghiem: kinhNghiem,
                SoGiayPhepLaoDong: soGiayPhepLaoDong,
                NgayHieuLucGPLD: ngayHieuLucGPLD,
                NgayHetHanGPLD: ngayHetHanGPLD,
                NgayDenVietNam: denVietNamTuNgay,
                SoHoChieu: soHoChieu,
                NgayHetHanHoChieu: ngayHetHanHoChieu,
                NoiCapHoChieu: noiCapHoChieu,
                SoVisa: soVisa,
                NgayCapVisa: ngayCapVisa,
                NgayHetHanVisa: ngayHetHanVisa,
                SoNganHang: soNganHang,
                TieuSu: tieuSu

            },
            url: '/Admin/GiangVien/Add',
            success: function (response) {
                var data = response;
                var id = response.Message;
                var giangVienId = data.value.id;
                if (id === "False")
                {
                    common.confirm('Thông tin giảng viên đã tồn tại - xem thông tin ', function () {
                        window.location.href = '/Admin/GiangVien/DetailById/' + response.Id;
                    });
                }
                else
                {
                    $.each($("input[name='ChuyenMonGiangVienId']:checked"), function () {
                        var loaiGiangDayId = $(this).val();
                        AddChiTietChuyenMonGiangDay(giangVienId, loaiGiangDayId);
                    });
                    common.notify('Thêm thành công', 'success');
                    window.location.href = '/Admin/GiangVien/GetById/' + data.value.id;
                }
            },
            error: function () {
                common.notify('Lỗi khi thêm giảng viên', 'error');
            },
        })
    }
    //------------------------------------------------------------------------------------------------
    var DelEvents = function () {
        $('body').on('click', '#btnDelGiangVien', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenGiangVien = $(this).data('content')
            Delete(id, tenGiangVien);
        });
    }

    
    function DetailGiangVien(id) {
        var result;
        $.ajax({
            type: "POST",
            data: {id:id},
            url: "/Admin/GiangVien/GetById",
            success: function () {
                
            },
            error: function () {
                result = "Đang cập nhật";
            }
        });
        return result;
    }
    //Kiểm tra thông tin giảng viên trước khi thêm

   
    //Add chi tiết chuyên môn gian dạy
    var AddChiTietChuyenMonGiangDay = function (giangVienId, loaiGiangDayId) {
        $.ajax({
            type: 'POST',
            data:
            {
                GiangVienId: giangVienId,
                LoaiGiangDayId: loaiGiangDayId

            },
            url: '/Admin/ChiTietChuyenMonGiangVien/AddChiTietChuyenMonGiangVien',
            success: function () {
               
            },
            error: function () {
                common.notify('Lỗi khi thêm chuyên môn giảng viên', 'error');
            },
        })
    }

    //-------------------------
    function Delete(id, tenGiangVien) {
        common.confirm('Are you sure to delete? ' + tenGiangVien, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/GiangVien/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableGiangVien();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa giảng viên', 'error');
                },
            });
        });
    }
    
    //var listBoMon;
    //function ListBoMon() {
    //    $.ajax({
    //        type: "GET",
    //        url: "/Admin/BoMon/GetAll",
    //        beforeSend: function () {
    //            common.startLoading();
    //        },
    //        success: function (response) {
    //            var data = response;
    //            listBoMon = data;
    //            //LoadTableGiangVien();
    //            common.stopLoading();
    //        },
    //        error: function () {
    //            common.notify('Có lỗi xảy ra khi tải danh sách bộ môn', 'error');
    //            common.stopLoading();
    //        }
    //    });
    //}

    var countLichDay=0;
    function ListLichDay(giangVienId) {
        $.ajax({
            type: "GET",
            url: "/Admin/ChiTietGiangDay/GetByGiangVienId",
            data: {
                id: giangVienId
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $.each(data, function (i, item) {
                    countLichDay++;
                });
              
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra khi tải danh sách bộ môn', 'error');
                common.stopLoading();
            }
        });
    }

    //function LoadTableGiangVien() {
    //    $.ajax({
    //        type: "GET",
    //        url: "/Admin/GiangVien/GetAll",
    //        beforeSend: function () {
    //            common.startLoading();
    //        },
    //        success: function (response) {
    //            var data = response;
    //            var template = $('#table-template-GiangVien').html();
    //            var render = "";
    //            var Stt = 1;
               
    //            $.each(data, function (i, item) {
    //                ListLichDay(item.id);
    //                var trangThai = '<span class="label label-danger">Hoạt động</span>';
    //                if (countLichDay == 0) {
    //                    trangThai = '<span class="label label-primary">Đang chờ</span>';
    //                }

    //                render += Mustache.render(template, {
    //                    STT: Stt++,
    //                    HoVaTen: item.hoVaTen,
    //                    Id: item.id,
    //                    Email: item.email,
    //                    SDT: item.soDienThoai,
    //                    TrangThai: trangThai
    //                });
    //            });
    //            if (render != '') {
    //                $('#tbl-content-GiangVien').html(render);
    //            }
    //            common.stopLoading();
    //        },
    //        error: function () {
    //            common.notify('Có lỗi xảy ra', 'error');
    //            common.stopLoading();
    //        }
    //    });
    //}
    function loadData(isPageChanged)
    {
        $.ajax({
            type: "GET",
            data: {
                Page: common.configs.pageIndex,
                PageSize: 10
            },
            url: "/Admin/GiangVien/GetAllPaging",
            success: function (response) {
                //console.log(response);
                var data = response.results;
                var template = $('#table-template-GiangVien').html();
                var render = "";
                var Stt = 1;

                $.each(data, function (i, item) {
                    var trangThai = '<span class="label label-danger">Hoạt động</span>';
                    if (item.trangThai == 0) {
                        trangThai = '<span class="label label-primary">Đang chờ</span>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        HoVaTen: item.tenGiangVien,
                        Id: item.id,
                        Email: item.email,
                        SDT: item.soDienThoai,
                        TrangThai: trangThai
                    });
                });
                $('#lblTotalRecords').text(response.rowCount);
                if (render != '') {
                    $('#tbl-content-GiangVien').html(render);
                }
                wrapPaging(response.rowCount, function () {
                    loadData();
                }, isPageChanged);
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function wrapPaging(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            first: 'Đầu',
            prev: 'Trước',
            next: 'Tiếp',
            last: 'Cuối',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callBack(), 200);
            }
        });
    }
}