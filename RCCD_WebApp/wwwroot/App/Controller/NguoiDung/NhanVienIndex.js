﻿var nhanVienIndexController = function () {
    this.initialize = function () {
        LoadTableTaiKhoanNhanVien();
        CallModalAddNhanVienIndex();
        AddTaiKhoanNhanVienEvents();
    }
    //---Show table nhân viên chưa có tài khoản
    var CallModalAddNhanVienIndex = function () {
        $('body').on('click', '#btnCallModalAddNguoiDung', function (e) {
            e.preventDefault();
            GetNhanVienChuaCoTaiKhoan(true);
        });
    }

    var GetNhanVienChuaCoTaiKhoan = function (isPageChanged) {
        $.ajax({
            type: "GET",
            data: {
                Page: common.configs.pageIndex
            },
            url: "/Admin/NguoiDung/GetNhanVienChuaCoTaiKhoan",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
              
                $('#LabelBoMon h5 ').text("Thêm tài khoản cho nhân viên");
                var data = response.results;
                var template = $('#table-template-NhanVienChuaCoTaiKhoan').html();
                var render = "";
                var Stt = 1;

                $.each(data, function (i, item) {
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenNhanVien: item.hoVaTen,
                        Id: item.id,
                        Email: item.email,
                        SDT: item.soDienThoai
                    });
                });
                $('#lblTotalRecords').text(response.rowCount);
                if (render != '') {
                    $('#tbl-content-NhanVienChuaCoTaiKhoan').html(render);
                }
                wrapPagingTableNhanVienChuaCoTaiKhoan(response.rowCount, function () {
                    GetNhanVienChuaCoTaiKhoan();
                }, isPageChanged);


                $('#NhanVienIndexModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }

    function wrapPagingTableNhanVienChuaCoTaiKhoan(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            first: 'Đầu',
            prev: 'Trước',
            next: 'Tiếp',
            last: 'Cuối',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callBack(), 200);
            }
        });
    }
    //---------------------------------------------------------------------
    var EditNhanVienIndexEvents = function () {
        $('body').on('click', '#btnEditNhanVienIndex', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditNhanVienIndex(id);
        });
    }
    //--------Thêm tài khoản nhân viên----------------------------------------
    var AddTaiKhoanNhanVienEvents = function () {
        $('body').on('click', '#btnAddTaiKhoan', function (e) {
            e.preventDefault();
            var nhanVienId = $(this).data('id');
            AddTaiKhoanNhanVien(nhanVienId);
        });
    }
    var AddTaiKhoanNhanVien = function (nhanVienId) {
        $.ajax({
            type: 'POST',
            data:
            {
                NguoiDungId: nhanVienId,
                Position: 1,
            },
            url: '/Admin/NguoiDung/Add',
            success: function (res) {
                var data = res;

                if (data.Response == "True") {
                    common.notify(data.Message, data.Type);
                    LoadTableTaiKhoanNhanVien();
                    $('#NhanVienIndexModal').modal('hide');
                }
                else {
                    common.notify(data.Message, data.Type);
                }
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    //-----------------------------------------------------------------------------
    var DelEvents = function () {
        $('body').on('click', '#btnDelNhanVienIndex', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenNhanVienIndex = $(this).data('content')
            Delete(id, tenNhanVienIndex);
        });
    }
    function EditNhanVienIndex(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/NhanVienIndex/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtNhanVienIndexId').val(data.id);
                $('#txtTenNhanVienIndex').val(data.tenNhanVienIndex);
                $('h5').text("Sửa bộ môn");
                $('#NhanVienIndexModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    
    function Delete(id, tenNhanVienIndex) {
        common.confirm('Are you sure to delete? ' + tenNhanVienIndex, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/NhanVienIndex/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableNhanVienIndex();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    function LoadTableTaiKhoanNhanVien() {
        $.ajax({
            type: "GET",
            url: "/Admin/NguoiDung/GetByPositionNhanVien",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {
               
                var data = response;
                var template = $('#table-template-NhanVienIndex').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var trangThai = '<a class="btn  btn-xs" id="btnTrangThaiTaiKhoan" data-id="' + item.id + '"><span class="label label-primary" >Hoạt động</span></a>';
                    if (item.trangThai==0) {
                        trangThai = '<a class="btn  btn-xs" id="btnTrangThaiTaiKhoan" data-id="' + item.id + '"><span class="label label-danger" >Hoạt động</span></a>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenDangNhap: item.email,
                        TenNhanVien: item.fullName,
                        Id: item.id,
                        TrangThai: trangThai
                    });
                });
                if (render != '') {
                    $('#tbl-content-NhanVienIndex').html(render);
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}