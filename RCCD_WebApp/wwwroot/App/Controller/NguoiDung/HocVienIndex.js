﻿var hocVienIndexController = function () {
    this.initialize = function () {
        LoadTableTaiKhoanHocVien(true);
        AddTaiKhoanHocVienEvents();
    }
    //---Show table nhân viên chưa có tài khoản



    //---------------------------------------------------------------------
    var EditHocVienIndexEvents = function () {
        $('body').on('click', '#btnEditHocVienIndex', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditHocVienIndex(id);
        });
    }
    //--------Thêm tài khoản nhân viên----------------------------------------
    var AddTaiKhoanHocVienEvents = function () {
        $('body').on('click', '#btnAddTaiKhoan', function (e) {
            e.preventDefault();
            var hocVienId = $("#HocVienChuaCoTaiKhoanId").val();
            AddTaiKhoanHocVien(hocVienId);
        });
    }
    var AddTaiKhoanHocVien = function (hocVienId) {
        $.ajax({
            type: 'POST',
            data:
            {
                NguoiDungId: hocVienId,
                Position: 3,
            },
            url: '/Admin/NguoiDung/Add',
            success: function (res) {
                var data = res;

                if (data.Response == "True") {
                    common.notify(data.Message, data.Type);
                    LoadTableTaiKhoanHocVien(true);
                }
                else {
                    common.notify(data.Message, data.Type);
                }
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    //-----------------------------------------------------------------------------
    var DelEvents = function () {
        $('body').on('click', '#btnDelHocVienIndex', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenHocVienIndex = $(this).data('content')
            Delete(id, tenHocVienIndex);
        });
    }
    function EditHocVienIndex(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/HocVienIndex/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtHocVienIndexId').val(data.id);
                $('#txtTenHocVienIndex').val(data.tenHocVienIndex);
                $('h5').text("Sửa bộ môn");
                $('#HocVienIndexModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }

    function Delete(id, tenHocVienIndex) {
        common.confirm('Are you sure to delete? ' + tenHocVienIndex, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/HocVienIndex/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableHocVienIndex();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    function LoadTableTaiKhoanHocVien(isPageChanged) {
        $.ajax({
            type: "GET",
            data: {
                Page: common.configs.pageIndex
            },
            url: "/Admin/NguoiDung/GetByPositionHocVien",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {

                var data = response.results;
                var template = $('#table-template-HocVienIndex').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var trangThai = '<a class="btn  btn-xs" id="btnTrangThaiTaiKhoan" data-id="' + item.id + '"><span class="label label-primary" >Hoạt động</span></a>';
                    if (item.trangThai == 0) {
                        trangThai = '<a class="btn  btn-xs" id="btnTrangThaiTaiKhoan" data-id="' + item.id + '"><span class="label label-danger" >Hoạt động</span></a>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenDangNhap: item.email,
                        TenHocVien: item.fullName,
                        Id: item.id,
                        TrangThai: trangThai
                    });
                });
                if (render != '') {
                    $('#tbl-content-HocVienIndex').html(render);
                }
                $('#lblTotalRecords').text(response.rowCount);

                wrapPagingLoadTableTaiKhoanHocVien(response.rowCount, function () {
                    LoadTableTaiKhoanHocVien();
                }, isPageChanged);
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    function wrapPagingLoadTableTaiKhoanHocVien(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            first: 'Đầu',
            prev: 'Trước',
            next: 'Tiếp',
            last: 'Cuối',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callBack(), 200);
            }
        });
    }
}