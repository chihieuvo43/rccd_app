﻿var giangVienIndexController = function () {
    this.initialize = function () {
        LoadTableTaiKhoanGiangVien(true);
        AddTaiKhoanGiangVienEvents();
    }
    //---Show table nhân viên chưa có tài khoản
   


    //---------------------------------------------------------------------
    var EditGiangVienIndexEvents = function () {
        $('body').on('click', '#btnEditGiangVienIndex', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            EditGiangVienIndex(id);
        });
    }
    //--------Thêm tài khoản nhân viên----------------------------------------
    var AddTaiKhoanGiangVienEvents = function () {
        $('body').on('click', '#btnAddTaiKhoan', function (e) {
            e.preventDefault();
            var giangVienId = $("#GiangVienChuaCoTaiKhoanId").val();
            AddTaiKhoanGiangVien(giangVienId);
        });
    }
    var AddTaiKhoanGiangVien = function (giangVienId) {
        $.ajax({
            type: 'POST',
            data:
            {
                NguoiDungId: giangVienId,
                Position: 2,
            },
            url: '/Admin/NguoiDung/Add',
            success: function (res) {
                var data = res;

                if (data.Response == "True") {
                    common.notify(data.Message, data.Type);
                    LoadTableTaiKhoanGiangVien(true);
                }
                else {
                    common.notify(data.Message, data.Type);
                }
            },
            error: function () {
                common.notify('Lỗi', 'error');
            },
        })
    }
    //-----------------------------------------------------------------------------
    var DelEvents = function () {
        $('body').on('click', '#btnDelGiangVienIndex', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var tenGiangVienIndex = $(this).data('content')
            Delete(id, tenGiangVienIndex);
        });
    }
    function EditGiangVienIndex(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/GiangVienIndex/GetById",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtGiangVienIndexId').val(data.id);
                $('#txtTenGiangVienIndex').val(data.tenGiangVienIndex);
                $('h5').text("Sửa bộ môn");
                $('#GiangVienIndexModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }

    function Delete(id, tenGiangVienIndex) {
        common.confirm('Are you sure to delete? ' + tenGiangVienIndex, function () {
            $.ajax({
                type: "POST",
                url: "/Admin/GiangVienIndex/Delete",
                data: { id: id },
                beforeSend: function () {
                    common.startLoading();
                },
                success: function () {
                    LoadTableGiangVienIndex();
                    common.notify('Xóa thành công', 'success')
                },
                error: function () {
                    common.notify('Lỗi khi xóa bộ môn', 'error');
                },
            });
        });
    }
    function LoadTableTaiKhoanGiangVien(isPageChanged) {
        $.ajax({
            type: "GET",
            data: {
                Page: common.configs.pageIndex
            },
            url: "/Admin/NguoiDung/GetByPositionGiangVien",
            beforeSend: function (xhr) {
                common.startLoading();
            },
            success: function (response) {

                var data = response.results;
                var template = $('#table-template-GiangVienIndex').html();
                var render = "";
                var Stt = 1;
                $.each(data, function (i, item) {
                    var trangThai = '<a class="btn  btn-xs" id="btnTrangThaiTaiKhoan" data-id="' + item.id + '"><span class="label label-primary" >Hoạt động</span></a>';
                    if (item.trangThai == 0) {
                        trangThai = '<a class="btn  btn-xs" id="btnTrangThaiTaiKhoan" data-id="' + item.id + '"><span class="label label-danger" >Hoạt động</span></a>';
                    }
                    render += Mustache.render(template, {
                        STT: Stt++,
                        TenDangNhap: item.email,
                        TenGiangVien: item.fullName,
                        Id: item.id,
                        TrangThai: trangThai
                    });
                });
                if (render != '') {
                    $('#tbl-content-GiangVienIndex').html(render);
                }
                $('#lblTotalRecords').text(response.rowCount);

                wrapPagingLoadTableTaiKhoanGiangVien(response.rowCount, function () {
                    LoadTableTaiKhoanGiangVien();
                }, isPageChanged);
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    function wrapPagingLoadTableTaiKhoanGiangVien(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            first: 'Đầu',
            prev: 'Trước',
            next: 'Tiếp',
            last: 'Cuối',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callBack(), 200);
            }
        });
    }
}