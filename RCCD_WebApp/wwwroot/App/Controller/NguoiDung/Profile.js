﻿var profileController = function () {
    this.initialize = function () {
        CallModalDoiMatKhauEvent();
        DoiMatKhauEvent();
    }
    //----------Lưu mật khẩu mới------------------------------
    var DoiMatKhauEvent = function () {
        $('body').on('click', '#btnSaveDoiMatKhau', function (e) {
            e.preventDefault();

            var matKhauCu = $("#txtMatKhauCu").val();
            var matKhauMoi = $("#txtMatKhauMoi").val();
            var xacNhanMatKhauMoi = $("#txtXacNhanMatKhauMoi").val();

            var id = $('#txtNguoiDungId').val();
            var maNguoiDung= $('#txtMaNguoiDung').val();
            var email= $('#txtEmail').val();
            var fullName= $('#txtFullName').val();
            var avatar=$('#txtAvatar').val();
            var position= $('#txtPosition').val();
            var status=$('#txtStatus').val();
            var nhanVienId=$('#txtNhanVienId').val();
            var giangVienId=$('#txtGiangVienId').val();
            var hocVienId = $('#txtHocVienId').val();

            var check = true;
            if (matKhauCu == "" || matKhauMoi == "" || xacNhanMatKhauMoi == "") 
            {
                common.notify('Nhập thông tin', 'error');
                check = false;
            }
            if (matKhauMoi != xacNhanMatKhauMoi) {
                common.notify('Xác nhận mật khẩu không chính xác', 'error');
                check = false;
            }
            if (check == true) {
                DoiMatKhau(matKhauCu, matKhauMoi, xacNhanMatKhauMoi, id, maNguoiDung, email,
                    fullName, avatar, position, status, nhanVienId, giangVienId, hocVienId);
            }
           
        });
    }
    function DoiMatKhau(matKhauCu, matKhauMoi, xacNhanMatKhauMoi, id, maNguoiDung, email,
        fullName, avatar, position, status, nhanVienId, giangVienId, hocVienId) {
        $.ajax({
            type: "GET",
            url: "/Admin/NguoiDung/SaveChangeDoiMatKhau",
            data: {
                Id: id,
                MaNguoiDung: maNguoiDung,
                Email: email,
                Password: matKhauCu,
                FullName: fullName,
                Avatar: avatar,
                Position: position,
                Status: status,
                NhanVienId: nhanVienId,
                GiangVienId: giangVienId,
                HocVienId: hocVienId,
                PasswordNew: matKhauMoi,
                ConfirmPasswordNew: xacNhanMatKhauMoi
            },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                var res = data.Response;
                var mes = data.Message;
                var type = data.Type;

                if (res == "True") {
                    common.notify(mes, type);
                    sessionStorage.clear();
                }
                common.notify(mes, type);
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
    //--------------------------------------------------------
    var CallModalDoiMatKhauEvent = function () {
        $('body').on('click', '#btnDoiMatKhau', function (e) {
            e.preventDefault();

            var id = $("#txtId").val();
            CallModalDoiMatKhau(id);
        });
    }

    function CallModalDoiMatKhau(id) {
        $.ajax({
            type: "GET",
            url: "/Admin/NguoiDung/DoiMatKhau",
            data: { id: id },
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                var data = response;
                $('#txtNguoiDungId').val(data.id);
                $('#txtMaNguoiDung').val(data.maNguoiDung);
                $('#txtEmail').val(data.email);
                $('#txtFullName').val(data.fullName);
                $('#txtAvatar').val(data.avatar);
                $('#txtPosition').val(data.position);
                $('#txtStatus').val(data.status);
                $('#txtNhanVienId').val(data.nhanVienId);
                $('#txtGiangVienId').val(data.giangVienId);
                $('#txtHocVienId').val(data.hocVienId);
                $('#DoiMatKhauModal').modal('show');
                common.stopLoading();
            },
            error: function () {
                common.notify('Có lỗi xảy ra', 'error');
                common.stopLoading();
            }
        });
    }
}