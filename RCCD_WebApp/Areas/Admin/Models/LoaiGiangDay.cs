﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class LoaiGiangDay
    {
        public int Id { get; set; }
        public string TenLoaiGiangDay { get; set; }
    }
}
