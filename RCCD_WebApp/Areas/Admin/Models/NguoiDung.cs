﻿namespace RCCD_WebApp.Areas.Admin.Models
{
    public class NguoiDung
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public int Position { get; set; }
        public int Status { get; set; }
        public int? NhanVienId { get; set; }
        public int? GiangVienId { get; set; }
        public int? HocVienId { get; set; }
    }
}
