﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class GiangVienLichDay
    {
        public int Id { get; set; }
        public string TenGiangVien { get; set; }
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
        public int TrangThai { get; set; }
    }
}
