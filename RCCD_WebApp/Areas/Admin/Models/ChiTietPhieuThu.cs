﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ChiTietPhieuThu
    {
        public int Id { get; set; }
        public string NoiDung { get; set; }
        public string DonViTinh { get; set; }
        public decimal DonGia { get; set; }
        public int SoLuong { get; set; }
        public int PhieuThuId { get; set; }
    }
}
