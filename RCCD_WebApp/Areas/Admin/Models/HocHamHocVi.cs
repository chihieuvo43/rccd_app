﻿using System.ComponentModel.DataAnnotations;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class HocHamHocVi
    {
        public int Id { get; set; }
        [Required]
        public string TenHocHamHocVi { get; set; }
    }
}
