﻿using System;
using System.Collections.Generic;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class NhanVien
    {
        public int Id { get; set; }
        public string HoVaTen { get; set; }
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
        public string DiaChi { get; set; }
        public string HinhAnh { get; set; }
        public string TieuSu { get; set; }
        public DateTime NgaySinh { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgaySua { get; set; }
        public int ChucVuId { get; set; }
        public int HocHamHocViId { get; set; }
        public virtual ICollection<ChucVu> ChucVus { get; set; }
    }
}
