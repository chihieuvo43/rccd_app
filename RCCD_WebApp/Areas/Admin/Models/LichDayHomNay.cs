﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class LichDayHomNay
    {
        public string GiangVien { get; set; }
        public int GiangVienId { get; set; }
        public string TroGiang { get; set; }
        public string TenKhoaHoc { get; set; }
        public int KhoaHocId { get; set; }
        public string TenPhong { get; set; }
        public string TenCa { get; set; }
        public string Thu { get; set; }
        public string GioBatDau { get; set; }
        public string GioKetThuc { get; set; }
    }
}
