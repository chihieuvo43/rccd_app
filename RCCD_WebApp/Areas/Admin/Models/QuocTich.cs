﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class QuocTich
    {
        public int Id { get; set; }
        public string TenQuocTich { get; set; }
    }
}
