﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class Paging
    { 
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
