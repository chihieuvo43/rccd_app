﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class KhachHang
    {
        public int Id { get; set; }
        public string HoVaTen { get; set; }

        public string SoDienThoai { get; set; }

        public string Email { get; set; }

        public string Facebook { get; set; }

        public DateTime? NgaySinh { get; set; }

        public string GioiTinh { get; set; }

        public int? PhuHuynhId { get; set; }
        public int? MaKhachHang { get; set; }
        public DateTime NgayTao { get; set; }

        public DateTime NgaySua { get; set; }
        public ICollection<PhieuThu> PhieuThus { get; set; }
    }
}
