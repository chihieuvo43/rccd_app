﻿using System;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class NgayNghi
    {
        public int Id { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime? DenNgay { get; set; }
        public int Nam { get; set; }
        public int? NamId { get; set; }
        public string NoiDung { get; set; }
    }
}
