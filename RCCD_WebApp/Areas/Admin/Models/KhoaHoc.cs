﻿using RCCD_WebApp.Areas.Admin.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class KhoaHoc
    {
        public int Id { get; set; }
        [Required]
        public string TenLop { get; set; }

        public string TenKhoa { get; set; }

        public string GiaoTrinh { get; set; }

        public int? ThoiLuong { get; set; }

        public decimal? HocPhiKhoaHoc { get; set; }

        public decimal? HocPhiTietHoc { get; set; }

        public decimal? GiamGia { get; set; }

        public DateTime? ThoiGianBatDau { get; set; }

        public DateTime? ThoiGianKetThuc { get; set; }

        public string GhiChu { get; set; }

        public int? SoLuongHocVien { get; set; }

        public int TrangThai { get; set; }

        public DateTime NgayTao { get; set; }

        public DateTime NgaySua { get; set; }
        public int? NhanVienId { get; set; }
        //public int? PhongId { get; set; }
        //public int? KhoaHocId { get; set; }
        public int? Flag { get; set; }
        public int? SoLuongHocVienToiThieu { get; set; }
        public string MaLop { get; set; }
        public int? NguoiPhuTrach { get; set; }

        public int LoaiGiangDayId { get; set; }
        public int LoaiChuongTrinhHocId { get; set; }
        public int ChuongTrinhHocId { get; set; }
        public int CapDoId { get; set; }

        public int? LopHocTruocId { get; set; }
    }
}
