﻿using System;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ChiTietGiangDay
    {
        public int Id { get; set; }
        public string GhiChu { get; set; }
        public int CaId { get; set; }
        public int TrangThai { get; set; }
        public int PhongHocId { get; set; }
        public int KhoaHocId { get; set; }
        public int GiangVienId { get; set; }
        public int TroGiang { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgaySua { get; set; }
    }
}
