﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ChucVu
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Nhập tên chức vụ")]
        public string TenChucVu { get; set; }
    }
}
