﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class PhieuThu
    {
        public int Id { get; set; }
        public decimal TongTien { get; set; }
        public decimal TienDaThanhToan { get; set; }
        public DateTime HanDong { get; set; }
        public string GhiChu { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgaySua { get; set; }
        public int KhoaHocId { get; set; }
        public int KhachHangId { get; set; }

        //public ICollection<ChiTietPhieuThu> ChiTietPhieuThus { get; set; }
        //public ICollection<ChiTietNhanVienPhieuThu> ChiTietNhanVienPhieuThus { get; set; }
    }
}
