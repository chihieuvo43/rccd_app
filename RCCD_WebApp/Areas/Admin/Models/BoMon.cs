﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class BoMon
    {
        public int Id { get; set; }
        [Required]
        public string TenBoMon { get; set; }
    }
}
