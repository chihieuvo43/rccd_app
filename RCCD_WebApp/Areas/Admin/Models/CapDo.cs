﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class CapDo
    {
        public int Id { get; set; }
        public string TenCapDo { get; set; }
        public int? ChuongTrinhId { get; set; }
        public int? SoThuTu { get; set; }
    }
}
