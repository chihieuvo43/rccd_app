﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models.Enum
{
    public enum TrangThai
    {
        KeHoach,
        TuyenSinh,
        DangDay,
        Hoan
    }
}
