﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class DoiMatKhau
    {
        public int Id { get; set; }
        public string MaNguoiDung { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public int Position { get; set; }
        public int Status { get; set; }
        public int? NhanVienId { get; set; }
        public int? GiangVienId { get; set; }
        public int? HocVienId { get; set; }

        public string PasswordNew { get; set; }
        public string ConfirmPasswordNew { get; set; }
    }
}
