﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ChiTietNhanVienPhieuThu
    {
        public int Id { get; set; }
        public int PhieuThuId { get; set; }
        public int NhanVienId { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgaySua { get; set; }
    }
}
