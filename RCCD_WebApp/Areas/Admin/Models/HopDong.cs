﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class HopDong
    {
        public int Id { get; set; }
        public string LoaiHopDong { get; set; }
        public DateTime NgayTaoHopDong { get; set; }
        public DateTime NgayBatDauHopDong { get; set; }
        public DateTime NgayKetThucHopDong { get; set; }
        public int HinhThucHopDong { get; set; }
        public string TenGiangVien { get; set; }
        public int GiangVienId { get; set; }
        public string TenNhanVien { get; set; }
        public int NhanVienId { get; set; }
        public string GhiChu { get; set; }
        public string TepHopDong { get; set; }
    }
}
