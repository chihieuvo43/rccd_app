﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ChuongTrinhHoc
    {
        public int Id { get; set; }
        public string TenChuongTrinh { get; set; }
        public int? LoaiChuongTrinhHocId { get; set; }
    }
}
