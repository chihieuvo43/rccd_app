﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ChiTietChuyenMonGiangVien
    {
        public int Id { get; set; }
        public int LoaiGiangDayId { get; set; }
        public int GiangVienId { get; set; }
    }
}
