﻿namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ChiTietLichTrong
    {
        public int Id { get; set; }
        public int GiangVienId { get; set; }
        public int CaId { get; set; }
    }
}
