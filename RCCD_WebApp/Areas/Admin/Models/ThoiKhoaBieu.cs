﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class ThoiKhoaBieu
    {
        public int Id { get; set; }
        public string GhiChu { get; set; }
        public string TenCa { get; set; }
        public string TenPhong { get; set; }
        public string TenGiangVien { get; set; }
        public string TroGiang { get; set; }
        public int Thu { get; set; }
    }
}

