﻿using System.ComponentModel.DataAnnotations;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class AddNguoiDung
    {
        public int NguoiDungId { get; set; }
        public int Position { get; set; }
    }
}
