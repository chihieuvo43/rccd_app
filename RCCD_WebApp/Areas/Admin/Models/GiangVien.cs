﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class GiangVien
    {
        public int Id { get; set; }
        [Required]
        public string HoVaTen { get; set; }
        [Required]
        public string SoDienThoai { get; set; }
        [Required]
        public string Email { get; set; }

        public int? BoMonId { get; set; }

        public int? ChucVuId { get; set; }

        public int? DonViId { get; set; }

        public int? HocHamHocViId { get; set; }

        public string DiaChi { get; set; }

        public DateTime? NgaySinh { get; set; }

        public string HinhAnh { get; set; }

        public string ThoiGianDiaDiemLamViec { get; set; }

        public string TieuSu { get; set; }

        public DateTime NgayTao { get; set; }

        public DateTime NgaySua { get; set; }

        public int? LoaiGiangVien { get; set; }
        public string MaGiangVienDau { get; set; }
        public int? MaGiangVienCuoi { get; set; }
        public string NoiSinh { get; set; }
        public int? QuocTichId { get; set; }
        public string ChungChiGiangDay { get; set; }
        public string DaiHoc { get; set; }
        public string KinhNghiem { get; set; }
        public string SoGiayPhepLaoDong { get; set; }
        public DateTime? NgayHieuLucGPLD { get; set; }
        public DateTime? NgayHetHanGPLD { get; set; }
        public DateTime? NgayDenVietNam { get; set; }
        public string SoHoChieu { get; set; }
        public DateTime? NgayHetHanHoChieu { get; set; }
        public string NoiCapHoChieu { get; set; }
        public string SoVisa { get; set; }
        public DateTime? NgayCapVisa { get; set; }
        public DateTime? NgayHetHanVisa { get; set; }
        public string SoNganHang { get; set; }
    }
}
