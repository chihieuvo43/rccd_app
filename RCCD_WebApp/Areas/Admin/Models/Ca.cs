﻿using System.ComponentModel.DataAnnotations;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class Ca
    {
        public int Id { get; set; }
        public string TenCa { get; set; }
        [Required]
        public string GioBatDau { get; set; }
        [Required]
        public string GioKetThuc { get; set; }
        public int NgayTrongTuan { get; set; }
        public string GhiChu { get; set; }
    }
}
