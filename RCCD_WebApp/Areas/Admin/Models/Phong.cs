﻿using System.ComponentModel.DataAnnotations;

namespace RCCD_WebApp.Areas.Admin.Models
{
    public class Phong
    {
        public int Id { get; set; }
        public string TenPhong { get; set; }
        public int NhanVienId { get; set; }
        public int TrangThai { get; set; }
        public int SucChua { get; set; }
    }
}
