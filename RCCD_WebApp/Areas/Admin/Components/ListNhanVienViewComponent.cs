﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Components
{
    [ViewComponent(Name = "ListNhanVien")]
    public class ListNhanVienViewComponent:ViewComponent
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public ListNhanVienViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<NhanVien> nhanVien = new List<NhanVien>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NhanVien");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nhanVien = JsonConvert.DeserializeObject<List<NhanVien>>(result);
                return View(nhanVien);
            }
            return View();
        }
    }
}
