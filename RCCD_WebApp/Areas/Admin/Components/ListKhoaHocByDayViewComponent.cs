﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Components
{
    [ViewComponent(Name = "ListKhoaHocByDay")]
    public class ListKhoaHocByDayViewComponent:ViewComponent
    {
        const string SessionToken = "_Token";
        const string Position = "_position";
        const string NguoiDungId = "_nguoiDungId";
        RCCD_API rccd_api = new RCCD_API();
        public ListKhoaHocByDayViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync(int  id)
        {
         
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            var position = HttpContext.Session.GetString(Position);
            var giangVienId = HttpContext.Session.GetString(NguoiDungId);

            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

        List<LichDayHomNay> listLichDayHomNay = new List<LichDayHomNay>();

            if(position=="1" || position == "0")
            {
               
                HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetByNow/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    listLichDayHomNay = JsonConvert.DeserializeObject<List<LichDayHomNay>>(result);
                    return View(listLichDayHomNay);
                }
            }
            if (position == "2")
            {
                HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetByNowAndGiangVienId/?id=" + id+"&giangVienId="+ giangVienId);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    listLichDayHomNay = JsonConvert.DeserializeObject<List<LichDayHomNay>>(result);
                    return View(listLichDayHomNay);
                }
            }

                return View();
        }
    }
}
