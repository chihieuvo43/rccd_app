﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Components
{
    [ViewComponent(Name = "ListHVChuaCoTaiKhoan")]
    public class ListHVChuaCoTaiKhoanViewComponent : ViewComponent
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public ListHVChuaCoTaiKhoanViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<KhachHang> hocVien = new List<KhachHang>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/GetHocVienChuaCoTaiKhoan");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                hocVien = JsonConvert.DeserializeObject<List<KhachHang>>(result);
                return View(hocVien);
            }
            return View();
        }
    }
}
