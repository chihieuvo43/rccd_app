﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Components
{
    [ViewComponent(Name = "ListChiTietGiangDay")]
    public class ListChiTietGiangDayViewComponent : ViewComponent
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public ListChiTietGiangDayViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync(int khoaHocId)
        {
            List<ChiTietGiangDay> chiTietGiangDay = new List<ChiTietGiangDay>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetMultiple/" + khoaHocId);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietGiangDay = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(result);
                ViewBag.ListGiangVien = await GetListGiangVien();
                ViewBag.ListPhong = await GetListPhong();
                return View(chiTietGiangDay);
            }
            return View();
        }
        public async Task<List<GiangVien>> GetListGiangVien()
        {
            List<GiangVien> giangVien = new List<GiangVien>();
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/GiangVien");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                giangVien = JsonConvert.DeserializeObject<List<GiangVien>>(result);
                ViewBag.ChucVu = chucVu;
                return giangVien;
            }
            return giangVien;
        }
        public async Task<List<Phong>> GetListPhong()
        {
            List<Phong> phong = new List<Phong>();
            HttpClient client = rccd_api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/Phong");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                phong = JsonConvert.DeserializeObject<List<Phong>>(result);
                return phong;
            }
            return phong;
        }
    }
}
