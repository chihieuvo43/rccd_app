﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Components
{
    [ViewComponent(Name = "ListCa")]
    public class ListCaViewComponent:ViewComponent
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public ListCaViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<Ca> ca = new List<Ca>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/Ca");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                ca = JsonConvert.DeserializeObject<List<Ca>>(result);
                return View(ca);
            }
            return View();
        }
    }
}
