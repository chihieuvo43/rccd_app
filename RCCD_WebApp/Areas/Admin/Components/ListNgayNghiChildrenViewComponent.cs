﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Components
{
    [ViewComponent(Name = "ListNgayNghiChildren")]
    public class ListNgayNghiChildrenViewComponent : ViewComponent
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public ListNgayNghiChildrenViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync(int Id)
        {
            List<NgayNghi> ngayNghi = new List<NgayNghi>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NgayNghi/GetChildren/" + Id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                ngayNghi = JsonConvert.DeserializeObject<List<NgayNghi>>(result);
                return View(ngayNghi);
            }
            return View();
        }
    }
}
