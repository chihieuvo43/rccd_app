﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RCCD_WebApp.Areas.Admin.Components
{
    [ViewComponent(Name = "ListCapDo")]
    public class ListCapDoViewComponent:ViewComponent
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public ListCapDoViewComponent()
        { }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<CapDo> capDo = new List<CapDo>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/CapDo");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                capDo = JsonConvert.DeserializeObject<List<CapDo>>(result);
                return View(capDo);
            }
            return View();
        }
    }
}
