﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChiTietLichTrongController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
       
        public async Task<IActionResult> GetAll()
        {
            List<ChiTietLichTrong> chiTietLichTrong = new List<ChiTietLichTrong>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietLichTrong");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietLichTrong = JsonConvert.DeserializeObject<List<ChiTietLichTrong>>(result);
                return new OkObjectResult(chiTietLichTrong);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            ChiTietLichTrong chiTietLichTrong = new ChiTietLichTrong();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietLichTrong/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietLichTrong = JsonConvert.DeserializeObject<ChiTietLichTrong>(result);
                return new OkObjectResult(chiTietLichTrong);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<ChiTietLichTrong>> AddChiTietLichTrong(ChiTietLichTrong chiTietLichTrong)
        {
            HttpClient client = rccd_api.Initial();

            if (ModelState.IsValid)
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChiTietLichTrong", chiTietLichTrong);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                var Response = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(Response);
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/ChiTietLichTrong/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<ChiTietLichTrong>> Update(ChiTietLichTrong chiTietLichTrong)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChiTietLichTrong/" + chiTietLichTrong.Id, chiTietLichTrong);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                chiTietLichTrong = await res.Content.ReadAsAsync<ChiTietLichTrong>();
                return new OkObjectResult(chiTietLichTrong);
            }
            return BadRequest(ModelState);
        }

        public async Task<IActionResult> GetByGiangVienId(int id)
        {
            List<ChiTietLichTrong> chiTietLichTrong = new List<ChiTietLichTrong>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietLichTrong/GetByGiangVienId/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietLichTrong = JsonConvert.DeserializeObject<List<ChiTietLichTrong>>(result);
                return new OkObjectResult(chiTietLichTrong);
            }
            return BadRequest();
        }
    }
}