﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChiTietNhanVienPhieuThuController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
       
        public async Task<IActionResult> GetAll()
        {
            List<ChiTietNhanVienPhieuThu> chiTietNhanVienPhieuThu = new List<ChiTietNhanVienPhieuThu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietNhanVienPhieuThu");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietNhanVienPhieuThu = JsonConvert.DeserializeObject<List<ChiTietNhanVienPhieuThu>>(result);
                return new OkObjectResult(chiTietNhanVienPhieuThu);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            ChiTietNhanVienPhieuThu chiTietNhanVienPhieuThu = new ChiTietNhanVienPhieuThu();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietNhanVienPhieuThu/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietNhanVienPhieuThu = JsonConvert.DeserializeObject<ChiTietNhanVienPhieuThu>(result);
                return new OkObjectResult(chiTietNhanVienPhieuThu);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<ChiTietNhanVienPhieuThu>> AddChiTietNhanVienPhieuThu(ChiTietNhanVienPhieuThu chiTietNhanVienPhieuThu)
        {
            chiTietNhanVienPhieuThu.NhanVienId = 1;
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (chiTietNhanVienPhieuThu.Id==0)
            {
                

                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChiTietNhanVienPhieuThu", chiTietNhanVienPhieuThu);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietNhanVienPhieuThu = JsonConvert.DeserializeObject<ChiTietNhanVienPhieuThu>(result);
                return new OkObjectResult(chiTietNhanVienPhieuThu);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChiTietNhanVienPhieuThu/" + chiTietNhanVienPhieuThu.Id, chiTietNhanVienPhieuThu);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    chiTietNhanVienPhieuThu = await res.Content.ReadAsAsync<ChiTietNhanVienPhieuThu>();
                    return new OkObjectResult(chiTietNhanVienPhieuThu);
                }
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/ChiTietNhanVienPhieuThu/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<ChiTietNhanVienPhieuThu>> Update(ChiTietNhanVienPhieuThu chiTietNhanVienPhieuThu)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChiTietNhanVienPhieuThu/" + chiTietNhanVienPhieuThu.Id, chiTietNhanVienPhieuThu);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                chiTietNhanVienPhieuThu = await res.Content.ReadAsAsync<ChiTietNhanVienPhieuThu>();
                return new OkObjectResult(chiTietNhanVienPhieuThu);
            }
            return BadRequest(ModelState);
        }
    }
}