﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class PhieuThuController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
       
        public async Task<IActionResult> GetAll()
        {
            List<PhieuThu> phieuThu = new List<PhieuThu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/PhieuThu");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                phieuThu = JsonConvert.DeserializeObject<List<PhieuThu>>(result);
                return new OkObjectResult(phieuThu);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            PhieuThu phieuThu = new PhieuThu();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/PhieuThu/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                phieuThu = JsonConvert.DeserializeObject<PhieuThu>(result);
                return new OkObjectResult(phieuThu);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<PhieuThu>> AddPhieuThu(PhieuThu phieuThu)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (phieuThu.Id==0)
            {
                HttpResponseMessage res = await client.PostAsJsonAsync($"api/PhieuThu", phieuThu);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                var phieuThuRes = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(phieuThuRes);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/PhieuThu/" + phieuThu.Id, phieuThu);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    phieuThu = await res.Content.ReadAsAsync<PhieuThu>();
                    return new OkObjectResult(phieuThu);
                }
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/PhieuThu/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<PhieuThu>> Update(PhieuThu phieuThu)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/PhieuThu/" + phieuThu.Id, phieuThu);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                phieuThu = await res.Content.ReadAsAsync<PhieuThu>();
                return new OkObjectResult(phieuThu);
            }
            return BadRequest(ModelState);
        }

    }
}