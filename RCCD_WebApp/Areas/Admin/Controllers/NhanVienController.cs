﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class NhanVienController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public async Task<IActionResult> Index()
        {
            List<ChucVu> listChucVu = await GetAllChucVu();
            ViewBag.ChucVu = listChucVu;
            return View();
        }
        public async Task<List<ChucVu>> GetAllChucVu()
        {
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChucVu");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chucVu = JsonConvert.DeserializeObject<List<ChucVu>>(result);
                return chucVu;
            }
            return chucVu;
        }
        public async Task<IActionResult> GetChucVuById(int id)
        {
            ChucVu chucVu = new ChucVu();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChucVu/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chucVu = JsonConvert.DeserializeObject<ChucVu>(result);
                return new OkObjectResult(chucVu);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetAll()
        {
            List<NhanVien> nhanVien = new List<NhanVien>();
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NhanVien");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nhanVien = JsonConvert.DeserializeObject<List<NhanVien>>(result);
                return new OkObjectResult(nhanVien);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetAllPaging(Paging paging)
        {

            List<NhanVien> giangVien = new List<NhanVien>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NhanVien/GetAllPaging/?Page=" + paging.Page + "&PageSize=" + paging.PageSize + "");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var model = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(model);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            NhanVien nhanVien = new NhanVien();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NhanVien/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nhanVien = JsonConvert.DeserializeObject<NhanVien>(result);
                return new OkObjectResult(nhanVien);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<NhanVien>> Add(NhanVien nhanVien)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (nhanVien.Id==0)
            {
                HttpResponseMessage res = await client.PostAsJsonAsync("api/NhanVien", nhanVien);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(nhanVien);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/NhanVien/" + nhanVien.Id, nhanVien);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    nhanVien = await res.Content.ReadAsAsync<NhanVien>();
                    return new OkObjectResult(nhanVien);
                }
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/NhanVien/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<NhanVien>> Update(NhanVien nhanVien)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/NhanVien/" + nhanVien.Id, nhanVien);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                nhanVien = await res.Content.ReadAsAsync<NhanVien>();
                return new OkObjectResult(nhanVien);
            }
            return BadRequest(ModelState);
        }
    }
}