﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class HopDongController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<HopDong> hopDong = new List<HopDong>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/HopDong");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                hopDong = JsonConvert.DeserializeObject<List<HopDong>>(result);
                return new OkObjectResult(hopDong);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<HopDong>> Add(HopDong hopDong)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (hopDong.Id==0)
            {
                
                HttpResponseMessage res = await client.PostAsJsonAsync("api/HopDong", hopDong);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(hopDong);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/HopDong/" + hopDong.Id, hopDong);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    hopDong = await res.Content.ReadAsAsync<HopDong>();
                    return new OkObjectResult(hopDong);
                }
               
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/HopDong/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<HopDong>> GetById(int id)
        {
            HopDong hopDong = new HopDong();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/HopDong/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                hopDong = JsonConvert.DeserializeObject<HopDong>(result);
                return new OkObjectResult(hopDong);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<HopDong>> Update(HopDong hopDong)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/HopDong/"+hopDong.Id,hopDong);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                hopDong = await res.Content.ReadAsAsync<HopDong>();
                return new OkObjectResult(hopDong);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetByGiangVienId(int Id)
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<HopDong> hopDong = new List<HopDong>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/HopDong/GetByGiangVienId/" + Id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                hopDong = JsonConvert.DeserializeObject<List<HopDong>>(result);
                return new OkObjectResult(hopDong);
            }
            return BadRequest(ModelState);
        }
    }
}