﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class LoaiChuongTrinhHocController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<LoaiChuongTrinhHoc> loaiChuongTrinhHoc = new List<LoaiChuongTrinhHoc>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/LoaiChuongTrinhHoc");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                loaiChuongTrinhHoc = JsonConvert.DeserializeObject<List<LoaiChuongTrinhHoc>>(result);
                return new OkObjectResult(loaiChuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<LoaiChuongTrinhHoc>> Add(LoaiChuongTrinhHoc loaiChuongTrinhHoc)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (loaiChuongTrinhHoc.Id==0)
            {
                
                HttpResponseMessage res = await client.PostAsJsonAsync("api/LoaiChuongTrinhHoc", loaiChuongTrinhHoc);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(loaiChuongTrinhHoc);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/LoaiChuongTrinhHoc/" + loaiChuongTrinhHoc.Id, loaiChuongTrinhHoc);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    loaiChuongTrinhHoc = await res.Content.ReadAsAsync<LoaiChuongTrinhHoc>();
                    return new OkObjectResult(loaiChuongTrinhHoc);
                }
               
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/LoaiChuongTrinhHoc/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<LoaiChuongTrinhHoc>> GetById(int id)
        {
            LoaiChuongTrinhHoc loaiChuongTrinhHoc = new LoaiChuongTrinhHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/LoaiChuongTrinhHoc/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                loaiChuongTrinhHoc = JsonConvert.DeserializeObject<LoaiChuongTrinhHoc>(result);
                return new OkObjectResult(loaiChuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<LoaiChuongTrinhHoc>> Update(LoaiChuongTrinhHoc loaiChuongTrinhHoc)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/LoaiChuongTrinhHoc/"+loaiChuongTrinhHoc.Id,loaiChuongTrinhHoc);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                loaiChuongTrinhHoc = await res.Content.ReadAsAsync<LoaiChuongTrinhHoc>();
                return new OkObjectResult(loaiChuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetByLoaiGiangDayId(int Id)
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<LoaiChuongTrinhHoc> loaiChuongTrinhHoc = new List<LoaiChuongTrinhHoc>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/LoaiChuongTrinhHoc/GetByLoaiGiangDayId/" + Id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                loaiChuongTrinhHoc = JsonConvert.DeserializeObject<List<LoaiChuongTrinhHoc>>(result);
                return new OkObjectResult(loaiChuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
    }
}