﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class HocHamHocViController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<HocHamHocVi> hocHamHocVi = new List<HocHamHocVi>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/HocHamHocVi");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                hocHamHocVi = JsonConvert.DeserializeObject<List<HocHamHocVi>>(result);
                return new OkObjectResult(hocHamHocVi);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<HocHamHocVi>> Add(HocHamHocVi hocHamHocVi)
        {
            if(ModelState.IsValid)
            {
                HttpClient client = rccd_api.Initial();

                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                //hocHamHocVi = JsonConvert.DeserializeObject<HocHamHocVi>();
                HttpResponseMessage res = await client.PostAsJsonAsync("api/HocHamHocVi", hocHamHocVi);
                res.EnsureSuccessStatusCode();

                // return URI of the created resource.
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/HocHamHocVi/{id}");
            return RedirectToAction("Index");
        }

    }
}