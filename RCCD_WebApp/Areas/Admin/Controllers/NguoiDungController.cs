﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class NguoiDungController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        const string Id = "_Id";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<NguoiDung> nguoiDung = new List<NguoiDung>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/NguoiDung");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nguoiDung = JsonConvert.DeserializeObject<List<NguoiDung>>(result);
                return new OkObjectResult(nguoiDung);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<NguoiDung>> Add(AddNguoiDung addNguoiDung)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PostAsJsonAsync("api/NguoiDung",addNguoiDung);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var Response = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(Response);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/NguoiDung/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<NguoiDung>> GetById(int id)
        {
            NguoiDung nguoiDung = new NguoiDung();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nguoiDung = JsonConvert.DeserializeObject<NguoiDung>(result);
                return new OkObjectResult(nguoiDung);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<NguoiDung>> Update(NguoiDung nguoiDung)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/NguoiDung/"+nguoiDung.Id,nguoiDung);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                nguoiDung = await res.Content.ReadAsAsync<NguoiDung>();
                return new OkObjectResult(nguoiDung);
            }
            return BadRequest(ModelState);
        }

        public IActionResult NhanVienIndex()
        {
            return View();
        }

        public IActionResult GiangVienIndex()
        {
            return View();
        }

        public IActionResult HocVIenIndex()
        {
            return View();
        }

        public async Task<IActionResult> GetByPositionNhanVien()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<NguoiDung> nguoiDung = new List<NguoiDung>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/GetByPositionNhanVien");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nguoiDung = JsonConvert.DeserializeObject<List<NguoiDung>>(result);
                return new OkObjectResult(nguoiDung);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetByPositionGiangVien(int Page)
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            //List<NguoiDung> nguoiDung = new List<NguoiDung>();
            HttpClient client = rccd_api.Initial();
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/GetByPositionGiangVien/"+Page);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var  nguoiDung = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(nguoiDung);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetByPositionHocVien(int Page)
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            HttpClient client = rccd_api.Initial();
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/GetByPositionHocVien/"+Page);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var nguoiDung = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(nguoiDung);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetNhanVienChuaCoTaiKhoan(int Page)
        {
            List<NhanVien> nhanVien = new List<NhanVien>();
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/GetListNhanVienChuaCoTaiKhoan/"+Page);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var Response = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(Response);
            }
            return BadRequest();
        }
        //Người dùng xem thông tin tài khoản
        public async Task<ActionResult<NguoiDung>> DetailNguoiDung(int id)
        {
            //Kiểm tra người dùng nhập tự nhập Id
            var nguoiDungId = HttpContext.Session.GetString(Id);
            if (id.ToString()!= nguoiDungId.ToString())
            {
                return BadRequest(ModelState);
            }

            NguoiDung nguoiDung = new NguoiDung();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/DetailNguoiDung/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nguoiDung = JsonConvert.DeserializeObject<NguoiDung>(result);
                return View(nguoiDung);
            }
            return BadRequest(ModelState);
        }

        //Đổi mật khẩu
        public async Task<ActionResult<NguoiDung>> DoiMatKhau(int id)
        {
            //Kiểm tra người dùng nhập tự nhập Id
            var nguoiDungId = HttpContext.Session.GetString(Id);
            if (id.ToString() != nguoiDungId.ToString())
            {
                return BadRequest(ModelState);
            }

            NguoiDung nguoiDung = new NguoiDung();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NguoiDung/DetailNguoiDung/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                nguoiDung = JsonConvert.DeserializeObject<NguoiDung>(result);
                return new OkObjectResult(nguoiDung);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<DoiMatKhau>> SaveChangeDoiMatKhau(DoiMatKhau doiMatKhau)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PostAsJsonAsync($"api/NguoiDung/DoiMatKhau/",doiMatKhau);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var response = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(response);
            }
            return BadRequest(ModelState);
        }
    }
}