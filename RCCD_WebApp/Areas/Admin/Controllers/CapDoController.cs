﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class CapDoController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<CapDo> capDo = new List<CapDo>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/CapDo");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                capDo = JsonConvert.DeserializeObject<List<CapDo>>(result);
                return new OkObjectResult(capDo);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<CapDo>> Add(CapDo capDo)
        {
            HttpClient client = rccd_api.Initial();
           if(capDo.Id==0)
            {

                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PostAsJsonAsync("api/CapDo", capDo);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(capDo);
            }
            else
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PutAsJsonAsync($"api/CapDo/" + capDo.Id, capDo);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    capDo = await res.Content.ReadAsAsync<CapDo>();
                    return new OkObjectResult(capDo);
                }
               
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/CapDo/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<CapDo>> GetById(int id)
        {
            CapDo capDo = new CapDo();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/CapDo/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                capDo = JsonConvert.DeserializeObject<CapDo>(result);
                return new OkObjectResult(capDo);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<CapDo>> Update(CapDo capDo)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/CapDo/"+capDo.Id,capDo);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                capDo = await res.Content.ReadAsAsync<CapDo>();
                return new OkObjectResult(capDo);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetByChuongTrinhHocId(int Id)
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<CapDo> chuongTrinhHoc = new List<CapDo>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/CapDo/GetByChuongTrinhHocId/" + Id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chuongTrinhHoc = JsonConvert.DeserializeObject<List<CapDo>>(result);
                return new OkObjectResult(chuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
    }
}