﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class BoMonController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            
            List<BoMon> boMon = new List<BoMon>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/BoMon");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                boMon = JsonConvert.DeserializeObject<List<BoMon>>(result);
                return new OkObjectResult(boMon);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<BoMon>> Add(BoMon boMon)
        {
            HttpClient client = rccd_api.Initial();
           if(boMon.Id==0)
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PostAsJsonAsync("api/BoMon", boMon);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(boMon);
            }
            else
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PutAsJsonAsync($"api/BoMon/" + boMon.Id, boMon);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    boMon = await res.Content.ReadAsAsync<BoMon>();
                    return new OkObjectResult(boMon);
                }
               
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/BoMon/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<BoMon>> GetById(int id)
        {
            BoMon boMon = new BoMon();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/BoMon/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                boMon = JsonConvert.DeserializeObject<BoMon>(result);
                return new OkObjectResult(boMon);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<BoMon>> Update(BoMon boMon)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/BoMon/"+boMon.Id,boMon);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                boMon = await res.Content.ReadAsAsync<BoMon>();
                return new OkObjectResult(boMon);
            }
            return BadRequest(ModelState);
        }
    }
}