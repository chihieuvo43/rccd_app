﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BaseController : Controller
    {
        const string SessionToken = "_Token";
      
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            if (demo == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { Controller= "Account", Action="Login" }));
            }
            base.OnActionExecuting(filterContext);
        }
    }
}