﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class LoaiGiangDayController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<LoaiGiangDay> loaiGiangDay = new List<LoaiGiangDay>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/LoaiGiangDay");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                loaiGiangDay = JsonConvert.DeserializeObject<List<LoaiGiangDay>>(result);
                return new OkObjectResult(loaiGiangDay);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<LoaiGiangDay>> Add(LoaiGiangDay loaiGiangDay)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (loaiGiangDay.Id==0)
            {
                
                HttpResponseMessage res = await client.PostAsJsonAsync("api/LoaiGiangDay", loaiGiangDay);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(loaiGiangDay);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/LoaiGiangDay/" + loaiGiangDay.Id, loaiGiangDay);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    loaiGiangDay = await res.Content.ReadAsAsync<LoaiGiangDay>();
                    return new OkObjectResult(loaiGiangDay);
                }
               
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/LoaiGiangDay/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<LoaiGiangDay>> GetById(int id)
        {
            LoaiGiangDay loaiGiangDay = new LoaiGiangDay();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/LoaiGiangDay/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                loaiGiangDay = JsonConvert.DeserializeObject<LoaiGiangDay>(result);
                return new OkObjectResult(loaiGiangDay);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<LoaiGiangDay>> Update(LoaiGiangDay loaiGiangDay)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/LoaiGiangDay/"+loaiGiangDay.Id,loaiGiangDay);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                loaiGiangDay = await res.Content.ReadAsAsync<LoaiGiangDay>();
                return new OkObjectResult(loaiGiangDay);
            }
            return BadRequest(ModelState);
        }
    }
}