﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChiTietChuyenMonGiangVienController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
       
        public async Task<IActionResult> GetAll()
        {
            List<ChiTietChuyenMonGiangVien> chiTietChuyenMonGiangVien = new List<ChiTietChuyenMonGiangVien>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietChuyenMonGiangVien");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietChuyenMonGiangVien = JsonConvert.DeserializeObject<List<ChiTietChuyenMonGiangVien>>(result);
                return new OkObjectResult(chiTietChuyenMonGiangVien);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            ChiTietChuyenMonGiangVien chiTietChuyenMonGiangVien = new ChiTietChuyenMonGiangVien();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietChuyenMonGiangVien/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietChuyenMonGiangVien = JsonConvert.DeserializeObject<ChiTietChuyenMonGiangVien>(result);
                return new OkObjectResult(chiTietChuyenMonGiangVien);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<ChiTietChuyenMonGiangVien>> AddChiTietChuyenMonGiangVien(ChiTietChuyenMonGiangVien chiTietChuyenMonGiangVien)
        {
            HttpClient client = rccd_api.Initial();
            if(ModelState.IsValid)
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChiTietChuyenMonGiangVien", chiTietChuyenMonGiangVien);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietChuyenMonGiangVien = JsonConvert.DeserializeObject<ChiTietChuyenMonGiangVien>(result);
                return new OkObjectResult(chiTietChuyenMonGiangVien);
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/ChiTietChuyenMonGiangVien/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<ChiTietChuyenMonGiangVien>> Update(ChiTietChuyenMonGiangVien chiTietChuyenMonGiangVien)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChiTietChuyenMonGiangVien/" + chiTietChuyenMonGiangVien.Id, chiTietChuyenMonGiangVien);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                chiTietChuyenMonGiangVien = await res.Content.ReadAsAsync<ChiTietChuyenMonGiangVien>();
                return new OkObjectResult(chiTietChuyenMonGiangVien);
            }
            return BadRequest(ModelState);
        }

        public async Task<IActionResult> GetByGiangVienId(int id)
        {
            List<ChiTietChuyenMonGiangVien> chiTietChuyenMonGiangVien = new List<ChiTietChuyenMonGiangVien>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietChuyenMonGiangVien/GetByGiangVienId/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietChuyenMonGiangVien = JsonConvert.DeserializeObject<List<ChiTietChuyenMonGiangVien>>(result);
                return new OkObjectResult(chiTietChuyenMonGiangVien);
            }
            return BadRequest();
        }
        public async Task<IActionResult> DeleteByGiangVienId(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietChuyenMonGiangVien/DeleteByGiangVienId/" + id);
            if (res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetByLoaiGiangDayId(int id)
        {
            List<GiangVien> listGiangVienByLoaiGiangDayId = new List<GiangVien>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietChuyenMonGiangVien/GetByLoaiGiangDayId/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                listGiangVienByLoaiGiangDayId = JsonConvert.DeserializeObject<List<GiangVien>>(result);
                return new OkObjectResult(listGiangVienByLoaiGiangDayId);
            }
            return BadRequest();
        }
    }
}