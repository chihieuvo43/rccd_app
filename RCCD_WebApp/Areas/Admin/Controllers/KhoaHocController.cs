﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class KhoaHocController : BaseController
    {
        const string SessionToken = "_Token";
         RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
                return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<KhoaHoc> khoaHoc = new List<KhoaHoc>();
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<List<KhoaHoc>>(result);
                return new OkObjectResult(khoaHoc);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetAllPaging(Paging paging)
        {

            List<KhoaHoc> giangVien = new List<KhoaHoc>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            paging.PageSize = 10;
            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/GetAllPaging/?Page=" + paging.Page + "&PageSize=" + paging.PageSize + "");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var model = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(model);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetHocPhi(int id)
        {
            KhoaHoc khoaHoc = new KhoaHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);
                return new OkObjectResult(khoaHoc);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            KhoaHoc khoaHoc = new KhoaHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);
                ViewBag.KhoaHoc = khoaHoc;
                return View();
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetByIdToValue(int id)
        {
            KhoaHoc khoaHoc = new KhoaHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);
                return new OkObjectResult(khoaHoc);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetThoiGianHoc(int khoaHocId)
        {
            List<ChiTietGiangDay> chiTietGiangDay = new List<ChiTietGiangDay>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetMultiple/" + khoaHocId);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietGiangDay = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(result);
                ViewBag.KhoaHoc = chiTietGiangDay;
                return PartialView(chiTietGiangDay);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetThoiGianHocValue(int Id)//Id is khóa học Id
        {
            List<ChiTietGiangDay> chiTietGiangDay = new List<ChiTietGiangDay>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetMultiple/" + Id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietGiangDay = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(result);
                return new OkObjectResult(chiTietGiangDay);
            }
            return BadRequest();
        }
        public ActionResult GetAdd()
        {
            return View();
        }
        
        public async Task<ActionResult> Add(KhoaHoc khoaHoc)
        {
            khoaHoc.Flag = 0;
           
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                            = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PostAsJsonAsync("api/KhoaHoc", khoaHoc);
            res.EnsureSuccessStatusCode();

            var result = res.Content.ReadAsStringAsync().Result;

            var result1 = JsonConvert.DeserializeObject(result);
                
            return Ok(result1);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/KhoaHoc/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<KhoaHoc>> Edit(KhoaHoc khoaHoc)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync(
                $"api/KhoaHoc/"+khoaHoc.Id,khoaHoc);
            if (res.IsSuccessStatusCode)
            {
                KhoaHoc khoaHocUpdated = new KhoaHoc();
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHocUpdated = JsonConvert.DeserializeObject<KhoaHoc>(result);
                return khoaHocUpdated;
            }
            return BadRequest();
        }
        public async Task<ActionResult<KhoaHoc>> EditTrangThai(int khoaHocId,int trangThai)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            KhoaHoc khoaHoc = new KhoaHoc();
            HttpResponseMessage reskhoaHoc = await client.GetAsync("api/KhoaHoc/" + khoaHocId);
            if (reskhoaHoc.IsSuccessStatusCode)
            {
                var resultkhoaHoc = reskhoaHoc.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(resultkhoaHoc);
            }
            //nếu trạng thái kết thúc thì cũng đánh dấu trạng thái chi tiết lịch dạy =0
            if (trangThai == 4)
            {
                List<ChiTietGiangDay> listChiTietGiangDayByKhoaHocId = new List<ChiTietGiangDay>();
                HttpResponseMessage resListChiTietGiangDayByKhoaHocId = await client.GetAsync("api/ChiTietGiangDay/GetListByKhoaHocId/" + khoaHocId);
                if (resListChiTietGiangDayByKhoaHocId.IsSuccessStatusCode)
                {
                    var resultListChiTietGiangDayByKhoaHocId = resListChiTietGiangDayByKhoaHocId.Content.ReadAsStringAsync().Result;
                    listChiTietGiangDayByKhoaHocId = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(resultListChiTietGiangDayByKhoaHocId);
                    //update trạng thái chi tiết giảng dạy
                    foreach (var item in listChiTietGiangDayByKhoaHocId)
                    {
                        item.TrangThai = 0;
                        HttpResponseMessage updateChiTietGiangDay = await client.PutAsJsonAsync(
                        $"api/ChiTietGiangDay/" + item.Id, item);
                    }
                }
            }

            khoaHoc.TrangThai = trangThai;

            HttpResponseMessage res = await client.PutAsJsonAsync(
                $"api/KhoaHoc/" + khoaHocId, khoaHoc);

            

            //---------------------------------------------------------------------------
            if (res.IsSuccessStatusCode)
            {
               // KhoaHoc khoaHocUpdated = new KhoaHoc();
                var result = res.Content.ReadAsStringAsync().Result;
                var khoaHocUpdated = JsonConvert.DeserializeObject(result);
                return Ok(khoaHocUpdated);
            }
            return BadRequest();
        }
        [HttpGet]
        public async Task<ActionResult<KhoaHoc>> EditKhoaHoc(string id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/GetByMaLopHoc/" + id);
            if (res.IsSuccessStatusCode)
            {
                KhoaHoc khoaHoc = new KhoaHoc();
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);
                return View(khoaHoc);
            }
            return BadRequest();
        }
        //Khi nhấn nút tay đổi trạng thái flag nếu là 1 thì chuyển thành 0 và ngược lại
        public async Task<ActionResult<KhoaHoc>> Flag(int id)
        {

            KhoaHoc khoaHoc = new KhoaHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);
                if(khoaHoc!=null)
                {
                    if (khoaHoc.Flag == 1)
                    {
                        khoaHoc.Flag = 0;
                    }
                    else
                    {
                        khoaHoc.Flag = 1;
                    }
                    HttpResponseMessage response = await client.PutAsJsonAsync(
                    $"api/KhoaHoc/" + khoaHoc.Id, khoaHoc);
                    response.EnsureSuccessStatusCode();
                    khoaHoc = await response.Content.ReadAsAsync<KhoaHoc>();
                    return new OkObjectResult(khoaHoc);
                }
            }
            return BadRequest();
            
        }
        //Lấy những khóa học có Flag là 1 để hiển thị ở trang chủ
        public async Task<IActionResult> GetByFlag()
        {
            List<KhoaHoc> khoaHoc = new List<KhoaHoc>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/GetKhoaHocByFlag");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<List<KhoaHoc>>(result);
                return View(khoaHoc);
            }
            return View();
        }
        //Load khóa học theo trạng thái
        public async Task<IActionResult> GetByTrangThai(int id)//id là trạng thái
        {
          
            List<KhoaHoc> khoaHoc = new List<KhoaHoc>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/GetKhoaHocByTrangThai/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<List<KhoaHoc>>(result);
                return new OkObjectResult(khoaHoc);
            }
            return BadRequest();
        }
        //Hiển thị chi tiết lớp học
        public async Task<IActionResult> DetailLopHoc(string id)
        {
            KhoaHoc khoaHoc = new KhoaHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/GetByMaLopHoc/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);
                //List Loại giảng dạy
                List<LoaiGiangDay> loaiGiangDay = new List<LoaiGiangDay>();
                HttpResponseMessage resLoaiGiangDay = await client.GetAsync("api/LoaiGiangDay");
                if (resLoaiGiangDay.IsSuccessStatusCode)
                {
                    var resLoaiGiangDayResult = resLoaiGiangDay.Content.ReadAsStringAsync().Result;
                    loaiGiangDay = JsonConvert.DeserializeObject<List<LoaiGiangDay>>(resLoaiGiangDayResult);
                    ViewBag.ListLoaiGiangDay = loaiGiangDay;
                }
                //List Loại chương trình học
                List<LoaiChuongTrinhHoc> loaiChuongTrinhHoc = new List<LoaiChuongTrinhHoc>();
                HttpResponseMessage resLoaiChuongTrinhHoc = await client.GetAsync("api/LoaiChuongTrinhHoc");
                if (resLoaiChuongTrinhHoc.IsSuccessStatusCode)
                {
                    var resLoaiChuongTrinhHocResult = resLoaiChuongTrinhHoc.Content.ReadAsStringAsync().Result;
                    loaiChuongTrinhHoc = JsonConvert.DeserializeObject<List<LoaiChuongTrinhHoc>>(resLoaiChuongTrinhHocResult);
                    ViewBag.ListLoaiChuongTrinhHoc = loaiChuongTrinhHoc;
                }
                //List Chương trình học
                List<ChuongTrinhHoc> chuongTrinhHoc = new List<ChuongTrinhHoc>();
                HttpResponseMessage resChuongTrinhHoc = await client.GetAsync("api/ChuongTrinhHoc");
                if (resChuongTrinhHoc.IsSuccessStatusCode)
                {
                    var resChuongTrinhHocResult = resChuongTrinhHoc.Content.ReadAsStringAsync().Result;
                    chuongTrinhHoc = JsonConvert.DeserializeObject<List<ChuongTrinhHoc>>(resChuongTrinhHocResult);
                    ViewBag.ListChuongTrinhHoc = chuongTrinhHoc;
                }
                //List Cấp độ
                List<CapDo> capDo = new List<CapDo>();
                HttpResponseMessage resCapDo = await client.GetAsync("api/CapDo");
                if (resCapDo.IsSuccessStatusCode)
                {
                    var resCapDoResult = resCapDo.Content.ReadAsStringAsync().Result;
                    capDo = JsonConvert.DeserializeObject<List<CapDo>>(resCapDoResult);
                    ViewBag.ListCapDo = capDo;
                }
                //List nhân viên (người phụ trách)
                List<NhanVien> nhanVien = new List<NhanVien>();
                HttpResponseMessage resNhanVien = await client.GetAsync("api/NhanVien");
                if (resNhanVien.IsSuccessStatusCode)
                {
                    var resNhanVienResult = resNhanVien.Content.ReadAsStringAsync().Result;
                    nhanVien = JsonConvert.DeserializeObject<List<NhanVien>>(resNhanVienResult);
                    ViewBag.ListNhanVien = nhanVien;
                }
                //Số lượng học viên hiện tại
                List<ChiTietDangKyKhoaHoc> chiTietDangKyKhoaHoc = new List<ChiTietDangKyKhoaHoc>();
                HttpResponseMessage reschiTietDangKyKhoaHoc = await client.GetAsync("api/ChiTietDangKyKhoaHoc/GetByKhoaHocId/"+ khoaHoc.Id);
                if (reschiTietDangKyKhoaHoc.IsSuccessStatusCode)
                {
                    var reschiTietDangKyKhoaHocResult = reschiTietDangKyKhoaHoc.Content.ReadAsStringAsync().Result;
                    chiTietDangKyKhoaHoc = JsonConvert.DeserializeObject<List<ChiTietDangKyKhoaHoc>>(reschiTietDangKyKhoaHocResult);
                    ViewBag.CountChiTietDangKyKhoaHoc = chiTietDangKyKhoaHoc.Count;
                }


                return View(khoaHoc);
            }
            return View();
        }

        public async Task<IActionResult> GiangVienByCaId(int Id,int LoaiGiangDayId, int Page)
        {

            //List<GiangVien> giangVien = new List<GiangVien>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/GiangVienByCaId/?id="+Id+ "&loaiGiangDayId=" + LoaiGiangDayId+"&page="+Page);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var giangVien = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(giangVien);
            }
            return BadRequest();
        }

        public async Task<IActionResult> PhongByCaId(int Id, int HocVienToiDa)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/PhongByCaId/?id=" + Id + "&loaiGiangDayId=" + HocVienToiDa);
            if (res.IsSuccessStatusCode)
            {
                List<Phong> phong = new List<Phong>();
                var result = res.Content.ReadAsStringAsync().Result;
                phong = JsonConvert.DeserializeObject<List<Phong>>(result);
                return new OkObjectResult(phong);
            }
            return BadRequest();
        }

        //Tính thời gian kết thúc
        public async Task<ActionResult> TinhThoiGianKetThuc(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            //Post khóa học
            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/TinhThoiGianKetThuc/"+ id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                KhoaHoc khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);
                //Update khóa học
                HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/KhoaHoc/" + khoaHoc.Id, khoaHoc);
                //response.EnsureSuccessStatusCode();
                //khoaHoc = await response.Content.ReadAsAsync<KhoaHoc>();
                //return new OkObjectResult(khoaHoc);
                return Ok();
            }


            return Ok();
        }

    }
}