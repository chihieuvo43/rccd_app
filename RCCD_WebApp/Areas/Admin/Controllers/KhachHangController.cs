﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class KhachHangController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
       
        public async Task<IActionResult> GetAll()
        {
            List<KhachHang> khachHang = new List<KhachHang>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhachHang");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khachHang = JsonConvert.DeserializeObject<List<KhachHang>>(result);
                return new OkObjectResult(khachHang);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            KhachHang khachHang = new KhachHang();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhachHang/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khachHang = JsonConvert.DeserializeObject<KhachHang>(result);
                return new OkObjectResult(khachHang);
            }
            return BadRequest();
        }
        //Kiểm tra tồn tại của số điện thoại
        public async Task<IActionResult> GetByNumberPhone(int numberPhone)
        {
            KhachHang khachHang = new KhachHang();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhachHang/GetByNumberPhone/" + numberPhone);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khachHang = JsonConvert.DeserializeObject<KhachHang>(result);
                return new OkObjectResult(khachHang);
            }
            return BadRequest();
        }
        //Kiểm tra tồn tại của email
        public async Task<IActionResult> GetByEmail(int email)
        {
            KhachHang khachHang = new KhachHang();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/KhachHang/GetByEmail/" + email);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khachHang = JsonConvert.DeserializeObject<KhachHang>(result);
                return new OkObjectResult(khachHang);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<KhachHang>> Add(KhachHang khachHang)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (khachHang.Id==0)
            {
                HttpResponseMessage res = await client.PostAsJsonAsync("api/KhachHang", khachHang);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                var model = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(model);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/KhachHang/" + khachHang.Id, khachHang);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    khachHang = await res.Content.ReadAsAsync<KhachHang>();
                    return new OkObjectResult(khachHang);
                }
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/KhachHang/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<KhachHang>> Update(KhachHang khachHang)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/KhachHang/" + khachHang.Id, khachHang);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                khachHang = await res.Content.ReadAsAsync<KhachHang>();
                return new OkObjectResult(khachHang);
            }
            return BadRequest(ModelState);
        }
        public ActionResult  DangKyMoi()
        {
            return View();
        }
        public ActionResult AddPhieuThu()
        {
            return View();
        }

    }
}