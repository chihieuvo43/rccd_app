﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChuongTrinhHocController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<ChuongTrinhHoc> chuongTrinhHoc = new List<ChuongTrinhHoc>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/ChuongTrinhHoc");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chuongTrinhHoc = JsonConvert.DeserializeObject<List<ChuongTrinhHoc>>(result);
                return new OkObjectResult(chuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<ChuongTrinhHoc>> Add(ChuongTrinhHoc chuongTrinhHoc)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (chuongTrinhHoc.Id==0)
            {
                
                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChuongTrinhHoc", chuongTrinhHoc);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(chuongTrinhHoc);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChuongTrinhHoc/" + chuongTrinhHoc.Id, chuongTrinhHoc);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    chuongTrinhHoc = await res.Content.ReadAsAsync<ChuongTrinhHoc>();
                    return new OkObjectResult(chuongTrinhHoc);
                }
               
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/ChuongTrinhHoc/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<ChuongTrinhHoc>> GetById(int id)
        {
            ChuongTrinhHoc chuongTrinhHoc = new ChuongTrinhHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChuongTrinhHoc/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chuongTrinhHoc = JsonConvert.DeserializeObject<ChuongTrinhHoc>(result);
                return new OkObjectResult(chuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<ChuongTrinhHoc>> Update(ChuongTrinhHoc chuongTrinhHoc)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChuongTrinhHoc/"+chuongTrinhHoc.Id,chuongTrinhHoc);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                chuongTrinhHoc = await res.Content.ReadAsAsync<ChuongTrinhHoc>();
                return new OkObjectResult(chuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetByLoaiChuongTrinhHocId(int Id)
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<ChuongTrinhHoc> chuongTrinhHoc = new List<ChuongTrinhHoc>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/ChuongTrinhHoc/GetByLoaiChuongTrinhHocId/"+Id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chuongTrinhHoc = JsonConvert.DeserializeObject<List<ChuongTrinhHoc>>(result);
                return new OkObjectResult(chuongTrinhHoc);
            }
            return BadRequest(ModelState);
        }
    }
}