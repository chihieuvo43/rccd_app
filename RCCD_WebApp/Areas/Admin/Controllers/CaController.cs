﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class CaController : BaseController
    {
        //const string Token = "AccessToken";
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            var demo = HttpContext.Session.GetString(SessionToken);
            var token = HttpContext.Session.GetString("AccessToken");
            List<Ca> ca = new List<Ca>();
            HttpClient client = rccd_api.Initial();
            //Response.Headers.Add("Bearer", demo);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);
            HttpResponseMessage res = await client.GetAsync("api/Ca");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                ca = JsonConvert.DeserializeObject<List<Ca>>(result);
                return new OkObjectResult(ca);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<Ca>> Add(Ca ca)
        {
            HttpClient client = rccd_api.Initial();
           if(ca.Id==0)
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PostAsJsonAsync("api/Ca", ca);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(ca);
            }
            else
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PutAsJsonAsync($"api/Ca/" + ca.Id, ca);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    ca = await res.Content.ReadAsAsync<Ca>();
                    return new OkObjectResult(ca);
                }
               
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/Ca/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<Ca>> GetById(int id)
        {
            Ca ca = new Ca();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/Ca/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                ca = JsonConvert.DeserializeObject<Ca>(result);
                return new OkObjectResult(ca);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<Ca>> Update(Ca ca)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/Ca/"+ca.Id,ca);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                ca = await res.Content.ReadAsAsync<Ca>();
                return new OkObjectResult(ca);
            }
            return BadRequest(ModelState);
        }
    }
}