﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class GiangVienController : BaseController
    {
        const string SessionToken = "_Token";
        const string NguoiDungId = "_nguoiDungId";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<GiangVien> giangVien = new List<GiangVien>();
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/GiangVien");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                giangVien = JsonConvert.DeserializeObject<List<GiangVien>>(result);
                ViewBag.ChucVu = chucVu;
                return new OkObjectResult(giangVien);
            }
            return BadRequest();
        }
        //public async Task<IActionResult> GetAllPaging(Paging paging)
        //{

        //    List<GiangVien> giangVien = new List<GiangVien>();
        //    List<ChucVu> chucVu = new List<ChucVu>();
        //    HttpClient client = rccd_api.Initial();
        //    HttpResponseMessage res = await client.GetAsync("api/GiangVien/GetAllPaging/?Page=" + paging.Page+"&PageSize="+paging.PageSize+"");
        //    if (res.IsSuccessStatusCode)
        //    {
        //        var result = res.Content.ReadAsStringAsync().Result;
        //        var model = JsonConvert.DeserializeObject(result);
        //        return new OkObjectResult(model);
        //    }
        //    return BadRequest();
        //}
        public async Task<IActionResult> GetAllPaging(Paging paging)
        {

            List<GiangVienLichDay> giangVien = new List<GiangVienLichDay>();
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/GiangVien/GetAllPaging/?Page=" + paging.Page + "&PageSize=" + paging.PageSize + "");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                var model = JsonConvert.DeserializeObject(result);
                return new OkObjectResult(model);
            }
            return BadRequest();
        }
        [HttpGet]
        public async Task<IActionResult> GetById(int id)
        {
            GiangVien giangVien = new GiangVien();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/GiangVien/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                giangVien = JsonConvert.DeserializeObject<GiangVien>(result);

                //List Đơn vị

                List<DonVi> donVi = new List<DonVi>();
                HttpResponseMessage resDonVi = await client.GetAsync("api/DonVi");
                if (resDonVi.IsSuccessStatusCode)
                {
                    var resDonViResult = resDonVi.Content.ReadAsStringAsync().Result;
                    donVi = JsonConvert.DeserializeObject<List<DonVi>>(resDonViResult);
                    ViewBag.ListDonVi = donVi;
                }
                //List Chuyên môn
                List<ChiTietChuyenMonGiangVien> chuyenMon = new List<ChiTietChuyenMonGiangVien>();
                HttpResponseMessage resChuyenMon = await client.GetAsync("api/ChiTietChuyenMonGiangVien");
                if (resChuyenMon.IsSuccessStatusCode)
                {
                    var resChuyenMonResult = resChuyenMon.Content.ReadAsStringAsync().Result;
                    chuyenMon = JsonConvert.DeserializeObject<List<ChiTietChuyenMonGiangVien>>(resChuyenMonResult);
                    ViewBag.ListChuyenMon = chuyenMon;
                }

                //List quốc tịch
                List<QuocTich> quocTich = new List<QuocTich>();
                HttpResponseMessage resQuocTich = await client.GetAsync("api/QuocTich");
                if (resQuocTich.IsSuccessStatusCode)
                {
                    var resQuocTichResult = resQuocTich.Content.ReadAsStringAsync().Result;
                    quocTich = JsonConvert.DeserializeObject<List<QuocTich>>(resQuocTichResult);
                    ViewBag.ListQuocTich = quocTich;
                }
                //List loại giảng dạy
                List<LoaiGiangDay> loaiGiangDay = new List<LoaiGiangDay>();
                HttpResponseMessage resLoaiGiangDay = await client.GetAsync("api/LoaiGiangDay");
                if (resLoaiGiangDay.IsSuccessStatusCode)
                {
                    var resLoaiGiangDayResult = resLoaiGiangDay.Content.ReadAsStringAsync().Result;
                    loaiGiangDay = JsonConvert.DeserializeObject<List<LoaiGiangDay>>(resLoaiGiangDayResult);
                    ViewBag.ListLoaiGiangDay = loaiGiangDay;
                }

                return View(giangVien);
            }
            return BadRequest();
        }
  
        public async Task<IActionResult> GetValues(int id)
        {
            GiangVien giangVien = new GiangVien();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/GiangVien/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                giangVien = JsonConvert.DeserializeObject<GiangVien>(result);
                
                return new OkObjectResult(giangVien);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<GiangVien>> Add(GiangVien giangVien)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PostAsJsonAsync("api/GiangVien", giangVien);
            res.EnsureSuccessStatusCode();
            var result = res.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject(result);
            return new OkObjectResult(model);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/GiangVien/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<GiangVien>> Edit(GiangVien giangVien)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/GiangVien/{giangVien.Id}",giangVien);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
            var GiangVien = await response.Content.ReadAsAsync<GiangVien>();
            return Ok();
        }
        [HttpGet]
        public IActionResult AddGiangVien()
        {
            return View();
        }
        public async Task<IActionResult> ListGiangDay(int id)
        {

            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            var giangVienId = HttpContext.Session.GetString(NguoiDungId);

            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            List<LichDayHomNay> listLichDayHomNay = new List<LichDayHomNay>();

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetAllByGiangVienId/?giangVienId="+ giangVienId);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                listLichDayHomNay = JsonConvert.DeserializeObject<List<LichDayHomNay>>(result);
                return View(listLichDayHomNay);
            }
         

            return View();
        }
    }
}