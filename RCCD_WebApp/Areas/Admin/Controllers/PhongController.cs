﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Helper;
using RCCD_WebApp.Areas.Admin.Models;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class PhongController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<Phong> phong = new List<Phong>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/Phong");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                phong = JsonConvert.DeserializeObject<List<Phong>>(result);
                return new OkObjectResult(phong);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<Phong>> Add(Phong phong)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (phong.Id==0)
            {
                HttpResponseMessage res = await client.PostAsJsonAsync("api/Phong", phong);
                res.EnsureSuccessStatusCode();
                return new OkObjectResult(phong);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/Phong/" + phong.Id, phong);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    phong = await res.Content.ReadAsAsync<Phong>();
                    return new OkObjectResult(phong);
                }
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/Phong/{id}");
            if(res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<Phong>> GetById(int id)
        {
            Phong phong = new Phong();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/Phong/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                phong = JsonConvert.DeserializeObject<Phong>(result);
                return new OkObjectResult(phong);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<Phong>> Update(Phong phong)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/Phong/"+phong.Id,phong);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                phong = await res.Content.ReadAsAsync<Phong>();
                return new OkObjectResult(phong);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetBySoLuongHocVienToiDa(int id)
        {
            List<Phong> phong = new List<Phong>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/Phong/GetBySoLuongHocVienToiDa/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                phong = JsonConvert.DeserializeObject<List<Phong>>(result);
                return new OkObjectResult(phong);
            }
            return BadRequest(ModelState);
        }
    }
}