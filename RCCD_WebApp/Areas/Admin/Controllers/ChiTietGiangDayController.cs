﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChiTietGiangDayController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
                return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<ChiTietGiangDay> chiTietGiangDay = new List<ChiTietGiangDay>();
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietGiangDay = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(result);
                return new OkObjectResult(chiTietGiangDay);
            }
            return BadRequest();
        }
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            ChiTietGiangDay chiTietGiangDay = new ChiTietGiangDay();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietGiangDay = JsonConvert.DeserializeObject<ChiTietGiangDay>(result);
                return new OkObjectResult(chiTietGiangDay);
            }
            return BadRequest();
        }
        public ActionResult GetAdd()
        {
            return View();
        }
        public async Task<ActionResult<ChiTietGiangDay>> Add(ChiTietGiangDay chiTietGiangDay)
        {
            if (ModelState.IsValid)
            {
                HttpClient client = rccd_api.Initial();

                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChiTietGiangDay", chiTietGiangDay);
                res.EnsureSuccessStatusCode();
                var Result = res.Content.ReadAsStringAsync().Result;
                var model = JsonConvert.DeserializeObject(Result);
                return new OkObjectResult(model);
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/ChiTietGiangDay/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<ChiTietGiangDay>> Edit(ChiTietGiangDay chiTietGiangDay)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/ChiTietGiangDay/{chiTietGiangDay.Id}",chiTietGiangDay);
            response.EnsureSuccessStatusCode();
            chiTietGiangDay = await response.Content.ReadAsAsync<ChiTietGiangDay>();
            return new OkObjectResult(chiTietGiangDay);
        }
        //Get khóa học by day
        public async Task<IActionResult> GetByDay(int id)
        {
            List<ChiTietGiangDay> chiTietGiangDay = new List<ChiTietGiangDay>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetByNow/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietGiangDay = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(result);
                //List khóa học
                List<KhoaHoc> khoaHoc = new List<KhoaHoc>();
                HttpResponseMessage listKhoaHoc = await client.GetAsync("api/KhoaHoc");
                var listKhoaHocResult = listKhoaHoc.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<List<KhoaHoc>>(listKhoaHocResult);
                //List phòng
                List<Phong> phong = new List<Phong>();
                HttpResponseMessage listPhong = await client.GetAsync("api/Phong");
                var listPhongResult = listPhong.Content.ReadAsStringAsync().Result;
                phong = JsonConvert.DeserializeObject<List<Phong>>(listPhongResult);
                //List giảng viên
                List<GiangVien> giangVien = new List<GiangVien>();
                HttpResponseMessage listGiangVien = await client.GetAsync("api/GiangVien");
                var listGiangVienResult = listGiangVien.Content.ReadAsStringAsync().Result;
                giangVien = JsonConvert.DeserializeObject<List<GiangVien>>(listGiangVienResult);

                ViewBag.ListKhoaHoc = khoaHoc;
                ViewBag.ListChiTietGiangDay = chiTietGiangDay;
                ViewBag.ListPhong = phong;
                ViewBag.ListGiangVien = giangVien;

                return View();
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetByKhoaHocId(int id)
        {
            List<ThoiKhoaBieu> thoiKhoaBieu = new List<ThoiKhoaBieu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetByKhoaHocId/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                thoiKhoaBieu = JsonConvert.DeserializeObject<List<ThoiKhoaBieu>>(result);
                return new OkObjectResult(thoiKhoaBieu);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetByGiangVienId(int id)
        {
            List<ThoiKhoaBieu> thoiKhoaBieu = new List<ThoiKhoaBieu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietGiangDay/GetByGiangVienId/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                thoiKhoaBieu = JsonConvert.DeserializeObject<List<ThoiKhoaBieu>>(result);
                return new OkObjectResult(thoiKhoaBieu);
            }
            return BadRequest();
        }
    }
}