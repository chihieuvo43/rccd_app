﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class DonViController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<DonVi> donVi = new List<DonVi>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/DonVi");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                donVi = JsonConvert.DeserializeObject<List<DonVi>>(result);
                return new OkObjectResult(donVi);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<DonVi>> Add(DonVi donVi)
        {
            if(ModelState.IsValid)
            {
                HttpClient client = rccd_api.Initial();

                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                //donVi = JsonConvert.DeserializeObject<DonVi>();
                HttpResponseMessage res = await client.PostAsJsonAsync("api/DonVi", donVi);
                res.EnsureSuccessStatusCode();

                // return URI of the created resource.
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/DonVi/{id}");
            return RedirectToAction("Index");
        }

    }
}