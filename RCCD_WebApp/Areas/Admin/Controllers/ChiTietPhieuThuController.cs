﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChiTietPhieuThuController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
       
        public async Task<IActionResult> GetAll()
        {
            List<ChiTietPhieuThu> chiTietPhieuThu = new List<ChiTietPhieuThu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietPhieuThu");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietPhieuThu = JsonConvert.DeserializeObject<List<ChiTietPhieuThu>>(result);
                return new OkObjectResult(chiTietPhieuThu);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            ChiTietPhieuThu chiTietPhieuThu = new ChiTietPhieuThu();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietPhieuThu/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietPhieuThu = JsonConvert.DeserializeObject<ChiTietPhieuThu>(result);
                return new OkObjectResult(chiTietPhieuThu);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<ChiTietPhieuThu>> AddChiTietPhieuThu(ChiTietPhieuThu chiTietPhieuThu)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (chiTietPhieuThu.Id==0)
            {
                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChiTietPhieuThu", chiTietPhieuThu);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietPhieuThu = JsonConvert.DeserializeObject<ChiTietPhieuThu>(result);
                return new OkObjectResult(chiTietPhieuThu);
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChiTietPhieuThu/" + chiTietPhieuThu.Id, chiTietPhieuThu);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    chiTietPhieuThu = await res.Content.ReadAsAsync<ChiTietPhieuThu>();
                    return new OkObjectResult(chiTietPhieuThu);
                }
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/ChiTietPhieuThu/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<ChiTietPhieuThu>> Update(ChiTietPhieuThu chiTietPhieuThu)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChiTietPhieuThu/" + chiTietPhieuThu.Id, chiTietPhieuThu);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                chiTietPhieuThu = await res.Content.ReadAsAsync<ChiTietPhieuThu>();
                return new OkObjectResult(chiTietPhieuThu);
            }
            return BadRequest(ModelState);
        }

    }
}