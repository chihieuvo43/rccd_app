﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Helper;
using RCCD_WebApp.Areas.Admin.Models;
using System;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class NgayNghiController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<NgayNghi> ngayNghi = new List<NgayNghi>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NgayNghi");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                ngayNghi = JsonConvert.DeserializeObject<List<NgayNghi>>(result);
                return new OkObjectResult(ngayNghi);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetChildrenView(int Id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            NgayNghi NgayNghi = new NgayNghi();
            HttpResponseMessage GetById = await client.GetAsync("api/NgayNghi/" + Id);
            if (GetById.IsSuccessStatusCode)
            {
                var result = GetById.Content.ReadAsStringAsync().Result;
                NgayNghi = JsonConvert.DeserializeObject<NgayNghi>(result);
            }

            if (NgayNghi != null)
            {
                int id = NgayNghi.Id;
                int nam =NgayNghi.Nam;
                List<NgayNghi> ngayNghi = new List<NgayNghi>();
               
                HttpResponseMessage res = await client.GetAsync("api/NgayNghi/GetChildren/" + Id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ngayNghi = JsonConvert.DeserializeObject<List<NgayNghi>>(result);
                    ViewBag.Nam = nam;
                    ViewBag.NamId = id;
                    return View(ngayNghi);
                }
            }
           
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetChildren(int Id)
        {
            List<NgayNghi> ngayNghi = new List<NgayNghi>();
            
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NgayNghi/GetChildren/"+Id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                ngayNghi = JsonConvert.DeserializeObject<List<NgayNghi>>(result);
                return new OkObjectResult(ngayNghi);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<NgayNghi>> Add(NgayNghi ngayNghi)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            if (ngayNghi.DenNgay != null)
            {
                if (DateTime.Compare(ngayNghi.TuNgay, ngayNghi.DenNgay.Value) >0)
                {
                    return BadRequest();
                }
            }
          
            if (ngayNghi.Id == 0)
            {
                HttpResponseMessage res = await client.PostAsJsonAsync("api/NgayNghi", ngayNghi);
                res.EnsureSuccessStatusCode();
                return Ok();
            }
            else
            {
                HttpResponseMessage res = await client.PutAsJsonAsync($"api/NgayNghi/" + ngayNghi.Id, ngayNghi);
                if (res.IsSuccessStatusCode)
                {
                    res.EnsureSuccessStatusCode();
                    ngayNghi = await res.Content.ReadAsAsync<NgayNghi>();
                    return new OkObjectResult(ngayNghi);
                }
            }

            return BadRequest();
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.DeleteAsync(
                $"api/NgayNghi/{id}");
            if (res.IsSuccessStatusCode)
            {
                return Ok();
            }
            return BadRequest(ModelState);
        }
        [HttpGet]
        public async Task<ActionResult<NgayNghi>> GetById(int id)
        {
            NgayNghi ngayNghi = new NgayNghi();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/NgayNghi/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                ngayNghi = JsonConvert.DeserializeObject<NgayNghi>(result);
                return new OkObjectResult(ngayNghi);
            }
            return BadRequest(ModelState);
        }
        public async Task<ActionResult<NgayNghi>> Update(NgayNghi ngayNghi)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/NgayNghi/"+ngayNghi.Id,ngayNghi);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                ngayNghi = await res.Content.ReadAsAsync<NgayNghi>();
                return new OkObjectResult(ngayNghi);
            }
            return BadRequest(ModelState);
        }
    }
}