﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;

namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChucVuController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
    public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetAll()
        {
            List<ChucVu> chucVu = new List<ChucVu>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChucVu");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chucVu = JsonConvert.DeserializeObject<List<ChucVu>>(result);
                return new OkObjectResult(chucVu);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<ChucVu>> Add(ChucVu chucVu)
        {
            if(ModelState.IsValid)
            {
                HttpClient client = rccd_api.Initial();

                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                //chucVu = JsonConvert.DeserializeObject<ChucVu>();
                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChucVu", chucVu);
                res.EnsureSuccessStatusCode();

                // return URI of the created resource.
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/ChucVu/{id}");
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> GetById(int id)
        {
            ChucVu chucVu = new ChucVu();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChucVu/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chucVu = JsonConvert.DeserializeObject<ChucVu>(result);
                return new OkObjectResult(chucVu);
            }
            return BadRequest();
        }

    }
}