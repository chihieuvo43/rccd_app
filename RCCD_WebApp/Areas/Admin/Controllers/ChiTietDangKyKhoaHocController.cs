﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
namespace RCCD_WebApp.Areas.Admin.Controllers
{
    public class ChiTietDangKyKhoaHocController : BaseController
    {
        const string SessionToken = "_Token";
        RCCD_API rccd_api = new RCCD_API();
        //private readonly IHostingEnvironment _hostingEnvironment;

        public IActionResult Index()
        {
            return View();
        }
       
        public async Task<IActionResult> GetAll()
        {
            List<ChiTietDangKyKhoaHoc> chiTietDangKyKhoaHoc = new List<ChiTietDangKyKhoaHoc>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietDangKyKhoaHoc");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietDangKyKhoaHoc = JsonConvert.DeserializeObject<List<ChiTietDangKyKhoaHoc>>(result);
                return new OkObjectResult(chiTietDangKyKhoaHoc);
            }
            return BadRequest();
        }
        public async Task<IActionResult> GetById(int id)
        {
            ChiTietDangKyKhoaHoc chiTietDangKyKhoaHoc = new ChiTietDangKyKhoaHoc();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietDangKyKhoaHoc/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietDangKyKhoaHoc = JsonConvert.DeserializeObject<ChiTietDangKyKhoaHoc>(result);
                return new OkObjectResult(chiTietDangKyKhoaHoc);
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<ActionResult<ChiTietDangKyKhoaHoc>> AddChiTietDangKyKhoaHoc(ChiTietDangKyKhoaHoc chiTietDangKyKhoaHoc)
        {
            HttpClient client = rccd_api.Initial();
            if(ModelState.IsValid)
            {
                var demo = HttpContext.Session.GetString(SessionToken);
                client.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", demo);

                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChiTietDangKyKhoaHoc", chiTietDangKyKhoaHoc);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietDangKyKhoaHoc = JsonConvert.DeserializeObject<ChiTietDangKyKhoaHoc>(result);
                return new OkObjectResult(chiTietDangKyKhoaHoc);
            }
            return new BadRequestObjectResult(ModelState);
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage response = await client.DeleteAsync(
                $"api/ChiTietDangKyKhoaHoc/{id}");
            return new OkObjectResult(id);
        }
        public async Task<ActionResult<ChiTietDangKyKhoaHoc>> Update(ChiTietDangKyKhoaHoc chiTietDangKyKhoaHoc)
        {
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.PutAsJsonAsync($"api/ChiTietDangKyKhoaHoc/" + chiTietDangKyKhoaHoc.Id, chiTietDangKyKhoaHoc);
            if (res.IsSuccessStatusCode)
            {
                res.EnsureSuccessStatusCode();
                chiTietDangKyKhoaHoc = await res.Content.ReadAsAsync<ChiTietDangKyKhoaHoc>();
                return new OkObjectResult(chiTietDangKyKhoaHoc);
            }
            return BadRequest(ModelState);
        }
        public async Task<IActionResult> GetByKhoaHocId(int id)//id là id của khóa học
        {
            List<ChiTietDangKyKhoaHoc> chiTietDangKyKhoaHoc = new List<ChiTietDangKyKhoaHoc>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

            HttpResponseMessage res = await client.GetAsync("api/ChiTietDangKyKhoaHoc/GetByKhoaHocId/"+id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietDangKyKhoaHoc = JsonConvert.DeserializeObject<List<ChiTietDangKyKhoaHoc>>(result);
                return new OkObjectResult(chiTietDangKyKhoaHoc);
            }
            return BadRequest();
        }
        public async Task<IActionResult> AddFileHocVien(IList<IFormFile> files, int khoaHocId)
        {
            List<ChiTietDangKyKhoaHoc> chiTietDangKyKhoaHoc = new List<ChiTietDangKyKhoaHoc>();
            HttpClient client = rccd_api.Initial();

            var demo = HttpContext.Session.GetString(SessionToken);
            client.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", demo);

         

            HttpResponseMessage res = await client.GetAsync("api/ChiTietDangKyKhoaHoc/ImportExcel/?files=" + files + "&khoaHocId=" + khoaHocId);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietDangKyKhoaHoc = JsonConvert.DeserializeObject<List<ChiTietDangKyKhoaHoc>>(result);
                return new OkObjectResult(chiTietDangKyKhoaHoc);
            }
            return BadRequest();

        }

    }
}