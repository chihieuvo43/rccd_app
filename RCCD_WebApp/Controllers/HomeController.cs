﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Areas.Admin.Models;
using RCCD_WebApp.Helper;
using RCCD_WebApp.Models;

namespace RCCD_WebApp.Controllers
{
    public class HomeController : Controller
    {
        RCCD_API rccd_api = new RCCD_API();
        public ActionResult Index()
       {
            return View();
       }
        public async Task<ActionResult<KhoaHoc>> Register(int id)
        {
            KhoaHoc khoaHoc = new KhoaHoc();
            HttpClient client = rccd_api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/KhoaHoc/" + id);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhoaHoc>(result);

                //Lấy thông tin chi tiết
                //List<ChiTietGiangDay> listChiTietGiangDay = new List<ChiTietGiangDay>();
                //HttpResponseMessage ChiTietGiangDay = await client.GetAsync("api/ChiTietGiangDay/GetMultiple/" + khoaHoc.KhoaHocId);
                //var resultChiTietGiangDay = ChiTietGiangDay.Content.ReadAsStringAsync().Result;
                //listChiTietGiangDay = JsonConvert.DeserializeObject<List<ChiTietGiangDay>>(resultChiTietGiangDay);

                ////List giảng viên
                //List<GiangVien> listGiangVien = new List<GiangVien>();
                //HttpResponseMessage giangVien = await client.GetAsync("api/GiangVien");
                //var resultGiangVien = giangVien.Content.ReadAsStringAsync().Result;
                //listGiangVien = JsonConvert.DeserializeObject<List<GiangVien>>(resultGiangVien);

                //ViewBag.ListChiTietGiangDay = listChiTietGiangDay;
                //ViewBag.ListGiangVien = giangVien;

                return View(khoaHoc);
            }
            return BadRequest();
        }
        //Kiểm tra tồn tại email
        public async Task<ActionResult<KhachHang>> CheckEmail(string email)
        {
            KhachHang khoaHoc = new KhachHang();
            HttpClient client = rccd_api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/KhachHang/GetByEmail/" + email);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhachHang>(result);
                return new OkObjectResult(khoaHoc);
            }
            return BadRequest();
        }
        //Kiểm tra tồn tại số điện thoại
        public async Task<ActionResult<KhachHang>> CheckNumberPhone(string numberPhone)
        {
            KhachHang khoaHoc = new KhachHang();
            HttpClient client = rccd_api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/KhachHang/GetByNumberPhone/" + numberPhone);
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                khoaHoc = JsonConvert.DeserializeObject<KhachHang>(result);
                return new OkObjectResult(khoaHoc);
            }
            return BadRequest();
        }
        public async Task<ActionResult<KhachHang>> DangKyHoc(DangKyKhoaHoc dangKyKhoaHoc)
        {
            HttpClient client = rccd_api.Initial();
            //kiếm tra số điện thoại để biết khách hàng là cũ hay mới
            //nếu khách hàng là mới thì lưu thông tin vào csdl
            //nếu khách hàng là cũ thì lấy mã khách hàng lưu vào bảng chi tiết đăng ký khóa học
            string soDienThoai = "";
                soDienThoai = dangKyKhoaHoc.SoDienThoai;
            string email = "";
                email = dangKyKhoaHoc.Email;
            //khách hàng mới
            if(CheckNumberPhone(soDienThoai)==null)
            {
                KhachHang khachHang = new KhachHang();
                khachHang.Email = email;
                khachHang.SoDienThoai = soDienThoai;
                //Lưu thông tin khách hàng vào bảng khách hàng
                HttpResponseMessage res = await client.PostAsJsonAsync("api/KhachHang", khachHang);
               
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                khachHang = JsonConvert.DeserializeObject<KhachHang>(result);
                return new OkObjectResult(khachHang);
            }
            return BadRequest();
        }
        public async Task<ActionResult<ChiTietDangKyKhoaHoc>> ChiTietDangKyKhoaHoc(ChiTietDangKyKhoaHoc chiTietDangKyKhoaHoc)
        {
           if(ModelState.IsValid)
           {
                HttpClient client = rccd_api.Initial();
                HttpResponseMessage res = await client.PostAsJsonAsync("api/ChiTietDangKyKhoaHoc", chiTietDangKyKhoaHoc);
                res.EnsureSuccessStatusCode();
                var result = res.Content.ReadAsStringAsync().Result;
                chiTietDangKyKhoaHoc = JsonConvert.DeserializeObject<ChiTietDangKyKhoaHoc>(result);
                return new OkObjectResult(chiTietDangKyKhoaHoc);
            }
            return BadRequest();
        }
    }
}
