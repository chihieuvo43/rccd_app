﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RCCD_WebApp.Helper;
using RCCD_WebApp.Models;

namespace RCCD_WebApp.Controllers
{
    public class AccountController : Controller
    {
        RCCD_API rccd_api = new RCCD_API();
        const string Token = "";
        const string SessionToken = "_Token";
        const string FullName = "_FullName";
        const string Email = "_Email";
        const string Id = "_Id";
        const string NguoiDungId = "_nguoiDungId";
        const string Position = "_position";
        const string Avatar = "_avatar";
        const string Roles = "_roles";

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult LoginClient()
        {
            return View();
        }

       
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                AccessToken token = new AccessToken();
                HttpClient client = rccd_api.Initial();
                HttpResponseMessage res = await client.PostAsJsonAsync("api/account/login", model);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    token = JsonConvert.DeserializeObject<AccessToken>(result);

                    //encode token
                    var stream = token.Token;
                    var handler = new JwtSecurityTokenHandler();
                    var jsonToken = handler.ReadToken(stream);
                    var tokenS = handler.ReadToken(token.Token) as JwtSecurityToken;

                    //Get claims
                    var fullName = tokenS.Claims.First(claim => claim.Type == "FullName").Value;
                    var email = tokenS.Claims.First(claim => claim.Type == "Email").Value;
                    var id = tokenS.Claims.First(claim => claim.Type == "Id").Value;
                    var nguoiDungId = tokenS.Claims.First(claim => claim.Type == "NguoiDungId").Value;
                    var position = tokenS.Claims.First(claim => claim.Type == "Position").Value;
                    var avatar = tokenS.Claims.First(claim => claim.Type == "Avatar").Value;
                    //var role = tokenS.Claims.First(claim => claim.Type == "Roles").Value;

                    HttpContext.Session.SetString(SessionToken, token.Token);
                    HttpContext.Session.SetString(FullName, fullName);
                    HttpContext.Session.SetString(Email, email);
                    HttpContext.Session.SetString(Id, id);
                    HttpContext.Session.SetString(NguoiDungId, nguoiDungId);
                    HttpContext.Session.SetString(Position, position);
                    HttpContext.Session.SetString(Avatar, avatar);
                    //HttpContext.Session.SetString(Roles, role);

                    return Ok();
                }
                return BadRequest();
            }
            return BadRequest();
        }
        [Route("Account/Logout")]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToRoute(new { Controller = "Account", Action = "Login" });
        }
        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();

            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);

            Response.Cookies.Append(key, value, option);
        }
    }
}