﻿using System.ComponentModel.DataAnnotations;

namespace RCCD_WebApp.Models
{
    public class DangKyKhoaHoc
    {
        public int KhoaHocId { get; set; }
        [Required(ErrorMessage ="Nhập họ và tên")]
        public string HoVaTen { get; set; }
        [Required(ErrorMessage = "Nhập số điện thoại")]
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
    }
}
