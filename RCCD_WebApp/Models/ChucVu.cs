﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Models
{
    public class ChucVu
    {
        public int Id { get; set; }
        public string TenChucVu { get; set; }
    }
}
