﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RCCD_WebApp.Models
{
    public class LoaiChuongTrinhHoc
    {
        public int Id { get; set; }
        public string TenLoaiChuongTrinhHoc { get; set; }
        public int? LoaiGiangDayId { get; set; }
        public string MoTa { get; set; }
    }
}
